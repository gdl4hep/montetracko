.. MonteTracko documentation master file, created by
   sphinx-quickstart on Mon Mar 27 03:05:36 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MonteTracko
===========

MonteTracko is a Python library for evaluating the performance of track reconstruction
algorithms in particle physics experiments.
It provides tools to match simulated particle trajectories with reconstructed tracks,
compute performance metrics, and visualize the results in various ways.

The Montetracko library has been tested and validated to produce identical
results to the Allen Velo validation sequence.
Currently, the library only supports one detector.

.. toctree::
   :glob:
   :hidden:

   guide
   changelog

.. toctree::
   :caption: APIDOC
   :glob:
   :hidden:

   apidoc/*


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
