CHANGELOG
=========

Version 0.2
-----------

* Fix typehints
* Fix minor bugs
* Use ``tqdm.auto``
* Delete ``core_legacy``

Version 0.3
-----------

* Fix documentation
* Add SciFi categories
* Update test data
* Add test for SciFi tracks
* Update to sphinx-book-theme 1.0.1
