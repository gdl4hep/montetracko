defs module
===========

.. automodule:: montetracko.defs
   :members:
   :undoc-members:
   :show-inheritance:
   :noindex: