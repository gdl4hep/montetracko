evaluation package
==================

.. automodule:: montetracko.evaluation
   :members:
   :undoc-members:
   :show-inheritance:
   :noindex:

evaluation.collections module
-----------------------------

.. automodule:: montetracko.evaluation.collections
   :members:
   :undoc-members:
   :show-inheritance:

evaluation.matching module
--------------------------

.. automodule:: montetracko.evaluation.matching
   :members:
   :undoc-members:
   :show-inheritance:

evaluation.trackevaluator module
--------------------------------

.. automodule:: montetracko.evaluation.trackevaluator
   :members:
   :undoc-members:
   :show-inheritance:

