# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

sys.path.insert(0, os.path.abspath("../.."))


# -- Project information -----------------------------------------------------

project = "MonteTracko"
copyright = "2023, Anthony Correia"
author = "Anthony Correia"

# The full version, including alpha/beta/rc tags
release = "0.1"

master_doc = "index"
language = "en"

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",  # aut-documentation of python code
    "sphinx.ext.viewcode",  # be able to see code directly in the documentation website
    "sphinx.ext.napoleon",  # numpy doc style
    # "sphinx.ext.coverage",
    "sphinx_autodoc_typehints",  # to read the python typehints
    "sphinx.ext.mathjax",  # for math support
    "sphinx-mathjax-offline",  # for using mathjax in offline mode
    # "myst_parser",  # to parse markdown files
    "sphinx_multiversion",
    "nbsphinx",
]

autodoc_default_options = {
    "members": True,
    "undoc-members": True,
    "private-members": True,
}

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

source_suffix = [".rst", ".md"]

myst_enable_extensions = [
    "dollarmath",
    "amsmath",
    "tasklist",
]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_book_theme"
html_static_path = ["_static"]
html_theme_options = {
    "repository_url": "https://gitlab.cern.ch/gdl4hep/montetracko",
    "use_repository_button": True,
    "path_to_docs": "docs/source",
    "use_issues_button": False,
    "use_edit_page_button": False,
    "home_page_in_toc": True,
    "repository_branch": True,
}

html_title = ""
html_logo = "_static/png/logo.png"
html_favicon = "_static/png/logo_square.png"

if (
    "SPHINX_MULTIVERSION_ACTIVATED" in os.environ
    and os.environ["SPHINX_MULTIVERSION_ACTIVATED"] == "true"
):
    templates_path = [
        "_templates",
    ]

html_css_files = [
    "css/readthedocs-doc-embed.css",
    "css/local.css",
    "css/badge_only.css",
]

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]
