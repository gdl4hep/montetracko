"""Module that implements a the base classes for categories.

A category is a requirement that is based on particle truth information.
"""
from __future__ import annotations
import typing
import pandas as pd
from . import base
from ..config import ConfigParams
from .. import utils


class MaskFunction(typing.Protocol):
    def __call__(self, df: pd.DataFrame) -> pd.Series:
        ...


class Category(base.RequirementBase):
    """Base class for particle truth-based categories.

    Args:
        name: The name of the category
        label: the label of the category
        config: configuration parameters

    Attributes:
        name: The name of the category
        label: The label of the category
    """

    def __init__(
        self,
        name: str,
        label: typing.Optional[str] = None,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        super().__init__(config=config)
        utils.formatting._assert_type(name, str, "name")
        utils.formatting._assert_type(label, str, "label", can_be_none=True)
        self.name = name
        self.label = label

    def __repr__(self):
        """Representation of the object."""
        return f"{self.__class__.__name__}[{self.name}]"

    def __str__(self):
        """String representation of the object."""
        return self.name

    def __and__(self, other: "Category"):
        """Define the ``&`` (and) operator for combining categories."""
        return CombinedCategory(
            category1=self,
            category2=other,
            operator="and",
        )

    def __or__(self, other: "Category"):
        """Define the ``|`` (or) operator for combining categories."""
        return CombinedCategory(
            category1=self,
            category2=other,
            operator="or",
        )

    def __invert__(self):
        """Define the ``~`` (not) operator for inverting a category."""
        return NotCategory(category=self)

    @classmethod
    def from_callable(
        cls,
        callable: MaskFunction,
        name: str,
        label: typing.Optional[str] = None,
        config: typing.Optional[ConfigParams | dict] = None,
    ) -> "Category":
        """Define a category from a callable (e.g., a lambda function)

        Args:
            name: The name of the category.
            callable: A callable object taking a DataFrame as input
                and returning a boolean Series (the mask)
            label: The label of the category
            config: Optional configuration parameters.

        Returns:
            A :py:class`Category` instance.
        """
        self = cls(name=name, label=label, config=config)
        self._mask = callable
        return self

    @classmethod
    def from_expr(
        cls,
        expr: str,
        name: str,
        label: typing.Optional[str] = None,
        config: typing.Optional[ConfigParams] = None,
    ) -> "Category":
        """Define a category from a query expression.

        Args:
            name: The name of the category.
            expr: The query expression.
            label: The label of the category
            config: Optional configuration parameters.

        Returns:
            A Category instance.
        """
        self = cls(name=name, label=label, config=config)
        self._mask = lambda df: df.eval(expr)
        return self

    @classmethod
    def from_category(
        cls,
        category: "Category",
        name: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        config: typing.Optional[ConfigParams] = None,
    ) -> "Category":
        """Define a new category by copying an existing category.

        Args:
            category: the :py:class:`Category` instance to copy
            name: the name of the new category. If not specified,
                the name of the original category is used.
            label: the label of the new category. If not specified,
                the label of the original category is used.
            config: the configuration parameters of the new category.
                If not specified, the configuration parameters of the original category
                are used.

        Returns:
            A new instance of :py:class:`Category` with the same mask
            and configuration parameters as the original category.
        """
        self = cls(
            name=category.name if name is None else name,
            label=label,
            config=category.config if config is None else config,
        )
        self._mask = category._mask
        return self

    def __eq__(self, category: "Category") -> bool:
        if type(self) == type(category):
            return (
                self.name == category.name
                and self.name == category.name
                and self._mask == category._mask
            )
        else:
            return False


class CombinedCategory(base.CombinedRequirementBase, Category):
    """Class that implements the combination of 2 :py:class:`Category`
    instances with a boolean operator (``and`` or ``or``).

    Attributes:
        requirement1: first :py:class:`Category` instance
        requirement2: second :py:class:`Category` instance
        operator: The boolean operator to combine the two categories ("and" or "or").
    """

    def __init__(
        self,
        category1: Category,
        category2: Category,
        operator: typing.Literal["and", "or"],
    ):
        base.CombinedRequirementBase.__init__(
            self,
            requirement1=category1,
            requirement2=category2,
            operator=operator,
        )
        Category.__init__(
            self,
            name=(
                f"{category1.name}_{category2.name}"
                if self.operator == "and"
                else f"{category1.name}_{self.operator}_{category2.name}"
            ),
            config=self.config,
        )


class NotCategory(base.NotRequirementBase, Category):
    """Class that implements the "not" (or inversion) operator of
    a :py:class:`Category` instance.

    Attributes:
        requirement: the :py:class:`Category` instance to invert
        config: configuration parameters
    """

    def __init__(self, category: Category):
        base.NotRequirementBase.__init__(self, requirement=category)
        Category.__init__(self, name=f"not_{category.name}", config=self.config)
