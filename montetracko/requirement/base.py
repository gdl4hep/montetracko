"""Module that implements the base classes to represent and combine requirements.
"""
from __future__ import annotations
import typing
import pandas as pd
from ..config import ConfigurableClass, ConfigParams
from .. import utils


class RequirementBase(ConfigurableClass):
    """Base class for requirements, that is, a Matching Condition or a Category.

    Attributes:
        config: configuration parameters
    """

    def __init__(
        self,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        """ """
        super().__init__(config=config)

    def __and__(self, other: "RequirementBase"):
        """Define the ``&`` (and) operator between 2 :py:class:`RequirementBase`
        instances.
        """
        return CombinedRequirementBase(
            requirement1=self,
            requirement2=other,
            operator="and",
        )

    def __or__(self, other: "RequirementBase"):
        """Define the ``|`` (or) operator between 2 :py:class:`RequirementBase`
        instances.
        """
        return CombinedRequirementBase(
            requirement1=self,
            requirement2=other,
            operator="or",
        )

    def __invert__(self):
        """Define the ``~`` (not) operator of a :py:class:`RequirementBase`
        instance.
        """
        return NotRequirementBase(requirement=self)

    def _mask(self, df: pd.DataFrame) -> pd.Series:
        """Defines the logic of the requirement.

        This method should be overridden by subclasses to implement specific
        requirements.

        Args:
            dataframe: the input data

        Returns:
            A boolean mask indicating which rows of the dataframe meet the requirement.
        """
        raise NotImplementedError

    def mask(self, dataframe: pd.DataFrame) -> pd.Series:
        """Checks the type of ``dataframe`` and applies :py:func:`RequirementBase._mask`
        method.

        Args:
            dataframe: DataFrame

        Returns:
            Mask according to the selection implemented in
            :py:func:`RequirementBase._mask`
        """
        utils.formatting._assert_type(dataframe, pd.DataFrame, "dataframe")
        return self._mask(dataframe)

    def filter(self, dataframe: pd.DataFrame) -> pd.DataFrame:
        """Filters the input dataframe based on the requirement.

        Args:
            dataframe: the input data to filder

        Returns:
            A filtered dataframe that contains only the rows that meet the requirement.
        """
        return dataframe[self.mask(dataframe)]

    def __repr__(self):
        """Representation of a requirement."""
        return utils.formatting._get_parameterised_repr(self)

    def __str__(self):
        """String representation of the requirement"""
        return repr(self)

    def _not_repr(self):
        """Representation of the inverse of the requirement"""
        return f"not ({repr(self)})"

    def _not_str(self):
        """String representation of the inverse of the requirement"""
        return f"not ({str(self)})"


class CombinedRequirementBase(RequirementBase):
    """Class that implements the combination of 2 :py:class:`RequirementBase` instances
    with a boolean operator (``and`` or ``or``).

    Attributes:
        requirement1: the first :py:class:`RequirementBase` instance
        requirement2: the second :py:class:`RequirementBase` instance
        operator: the boolean operator to combine the requirements (``and`` or ``or``)
    """

    def __init__(
        self,
        requirement1: RequirementBase,
        requirement2: RequirementBase,
        operator: typing.Literal["and", "or"],
    ):
        """Initializes a :py:class:``CombinedRequirementBase` instance.

        Args:
            requirement1: The first RequirementBase instance.
            requirement2: The second RequirementBase instance.
            operator: The boolean operator to combine the requirements ("and" or "or").

         Raises:
            ValueError: If the requirements have different configurations.
            ValueError: If the operator is neither ``and`` not ``or``
        """
        if not requirement1.config == requirement2.config:
            raise ValueError("The categories have different configurations")

        utils.formatting._assert_var_in_list(operator, ["and", "or"], "operator")

        RequirementBase.__init__(self, config=requirement1.config)

        self.requirement1 = requirement1
        self.requirement2 = requirement2
        self.operator = operator

    def _mask(self, *args, **kwargs) -> pd.Series:
        """Combines the masks of the ``requirement1`` and ``requirement2`` attributes
        with the ``operator`` (``and`` or ``or``).

        Args:
            args: passed to ``requirement1.mask`` and ``requirement.mask2``
            kwargs: passed to ``requirement1.mask`` and ``requirement.mask2``

        Returns:
            A boolean mask indicating which rows of the input data meet the
            combined requirements.
        """
        mask1 = self.requirement1.mask(*args, **kwargs)
        mask2 = self.requirement2.mask(*args, **kwargs)
        if self.operator == "and":
            return mask1 & mask2
        elif self.operator == "or":
            return mask1 | mask2
        else:
            raise ValueError("The operator is not supported.")

    def __repr__(self):
        """Representation of two combined requirements"""
        return (
            f"({repr(self.requirement1)}) {self.operator} ({repr(self.requirement2)})"
        )

    def __str__(self):
        """String representation of two combined requirements"""
        return f"({str(self.requirement1)}) {self.operator} ({str(self.requirement2)})"


class NotRequirementBase(RequirementBase):
    """Class that implements the "not" (or inversion) operator of
    a :py:class:`RequirementBase` instance.

    Attributes:
        requirement: the :py:class:`RequirementBase` instance to invert
        config: configuration parameters
    """

    def __init__(self, requirement: RequirementBase):
        """Initialises a :py:class:`NotRequirementBase` instance.

        Args:
            requirement (RequirementBase): The RequirementBase instance to invert.
        """
        RequirementBase.__init__(self, config=requirement.config)

        self.requirement = requirement

    def _mask(self, *args, **kwargs) -> pd.Series:
        """Return the inverse of the ``requirement``'s mask

        Arguments:
            args: passed to ``requirement.mask``
            kwargs: passed to ``requirement.mask``

        Returns:
            Inverse of the mask returned for the ``requirement`` attribute
        """
        mask = self.requirement.mask(*args, **kwargs)
        return ~mask

    def __repr__(self):
        """Representation of the inverse of a requirement"""
        return self.requirement._not_repr()

    def __str__(self):
        """String representation of the inverse of a requirement"""
        return self.requirement._not_str()
