"""A library to match tracks to MC particles, and compute efficiencies.
"""
# Packages
from . import (
    array_utils,
    tracks,
    requirement,
    matchcond,
    metrics,
)

# Objects
from .config.globals import NUMBA_CONFIG
from .config import ConfigParams
from .tracks.trackhandler import TrackHandler
from .metrics.library import metricsLibrary 
# from .metrics import metricsLibrary
from .report import AllenReporter, TabReporter
from .evaluation import TrackMatcher, TrackEvaluator

__all__ = [
    "array_utils",
    "tracks",
    "requirement",
    "matchcond",
    "metrics",
]

__all__ += [
    # config
    "NUMBA_CONFIG",
    "ConfigParams",
    # Tracks
    "TrackHandler",
    # metrics
    "metricsLibrary",
    # report
    "AllenReporter",
    "TabReporter",
    # evaluation
    "TrackMatcher",
    "TrackEvaluator",
]
