"""Module that defines :py:class:`TrackHandler` to load and save tracks.
"""
from __future__ import annotations
import typing
import warnings
import os.path as op
import glob

from tqdm.auto import tqdm
from joblib import Parallel, delayed
import pandas as pd
import numpy as np
from . import conversion, io
from ..array_utils import records, groupby
from ..config import ConfigParams, ConfigurableClass
from .. import utils


def _read_csv_pool(path, skip_header):
    """Reads a CSV file from disk and returns its contents as a NumPy array along with
    any other additional arguments.

    Args:
        path: A string representing the path to the CSV file to read.
        skip_header: An integer representing the number of header rows to skip.

    Returns:
        The NumPy array representing the contents of the CSV file.
    """
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", UserWarning)
        # Much faster than using pandas!

        return np.genfromtxt(
            path,
            delimiter=",",
            skip_header=skip_header,
            dtype=np.int64,
        )


def _get_paths(paths: str | typing.List[str]) -> typing.List[str]:
    """Returns a list of file paths from a given string path or a list of string paths.

    Args:
        paths: A string or list of strings representing file paths or path patterns.
            If a path pattern is provided (i.e. contains "*"), it is expanded using
            ``glob``.

    Returns:
        A list of string paths.

    Examples:
        >>> _get_paths("path/to/file.csv")
        ["path/to/file.csv"]

        >>> _get_paths(["path/to/file1.csv", "path/to/file2.csv"])
        ["path/to/file1.csv", "path/to/file2.csv"]

        >>> _get_paths("path/to/*.csv")
        ["path/to/file1.csv", "path/to/file2.csv"]
    """
    if isinstance(paths, str):
        if "*" in paths:
            paths = glob.glob(paths)
        else:
            paths = [paths]

    return list(paths)


def _read_numbers_in_filenames(paths) -> np.ndarray:
    """Returns an array of integers representing the numbers in the filenames
    of the input paths.

    Args:
        paths: A sequence of strings representing the paths to the input files.

    Returns:
        A NumPy array of integers representing the numbers in the filenames of
        the input paths.
    """
    return np.array([int(op.splitext(op.basename(path))[0]) for path in paths])


class TrackHandler(ConfigurableClass):
    """A class to handle a set of tracks, represented as a pandas DataFrame."""

    def __init__(
        self,
        dataframe: typing.Optional[pd.DataFrame] = None,
        config: typing.Optional[ConfigParams] = None,
    ):
        """Create a new TrackHandler instance.

        Args:
            dataframe: A pandas DataFrame containing track information.
            config: A :py:class:`..config.ConfigParams` object containing configuration
                parameters for the TrackHandler.
                If not provided, the default configuration will be used.
        """
        super().__init__(config=config)
        if dataframe is None:
            self._dataframe = None
        else:
            self.dataframe = dataframe

    # ############################## Dataframe property ##############################
    @property
    def dataframe(self) -> pd.DataFrame:
        """Current DataFrame of tracks.

        The DataFrame must contain the following columns:
        * the event IDs
        * The track IDs for the given events
        * The hit IDs for this tracks
        """
        if self._dataframe is None:
            raise Exception("The DataFrame of tracks was not defined.")
        else:
            assert isinstance(self._dataframe, pd.DataFrame)
            return self._dataframe

    @dataframe.setter
    def dataframe(self, dataframe: pd.DataFrame):
        """Set the current DataFrame of tracks.

        Args:
            dataframe: The new DataFrame of tracks.

        Raises:
            ValueError: If the input DataFrame does not contain the required columns.
        """
        self._dataframe = self._check_dataframe(dataframe)

    def add_dataframe(self, dataframe: pd.DataFrame):
        """Add a new DataFrame of tracks to the current DataFrame.

        Args:
            dataframe: The new DataFrame of tracks to add.

        Raises:
            ValueError: If the input DataFrame does not contain the required columns.
        """
        if self._dataframe is None:
            self.dataframe = dataframe
        else:
            self.dataframe = pd.concat(
                (self.dataframe, self._check_dataframe(dataframe))
            )

    def _check_dataframe(self, dataframe):
        """Check if the input DataFrame contains the required columns.

        Args:
            dataframe: The DataFrame to check.

        Returns:
            The DataFrame containing only the required columns.

        Raises:
            ValueError: If the input DataFrame does not contain the required columns.
        """
        required_columns = [
            self.to_alias(column) for column in ["event_id", "track_id", "hit_id"]
        ]

        for column in required_columns:
            if column not in dataframe:
                raise ValueError("Column " + str(column) + " is not in the dataframe.")
        return dataframe[required_columns]

    # ############################# from padded array(s) #############################
    def from_padded_array_to_dataframe(
        self,
        padded_tracks: np.ndarray,
        event_id: int,
        padding_value: int = 0,
    ):
        """Converts an array of padded tracks to a Pandas DataFrame.

        Args:
            padded_tracks: A NumPy array of shape ``(n_tracks, n_hits_max)``
                representing the padded tracks to convert to a DataFrame.
            event_id: An integer representing the ID of the event to which the track
                belongs.
            padding_value: An integer representing the value used to identify padded
                hits. Defaults to 0.

        Returns:
            A Pandas DataFrame with columns for event ID, track ID, and hit IDs.
        """
        # Get track IDs and hit IDs from `padded_tracks`
        track_ids, hit_ids = conversion.padded_array_to_trackid_hitid_couples(
            padded_tracks, padding_value=padding_value
        )

        # Form the event ID column
        event_ids = np.full(fill_value=event_id, shape=track_ids.shape)

        # Build the dataframe
        dataframe = pd.DataFrame(
            {
                self.to_alias("event_id"): event_ids,
                self.to_alias("track_id"): track_ids,
                self.to_alias("hit_id"): hit_ids,
            }
        )

        return dataframe

    def add_from_padded_tracks(
        self,
        padded_tracks: np.ndarray,
        event_id: int,
        padding_value: int = 0,
    ):
        """Adds tracks to the existing DataFrame.

        Args:
            padded_tracks: A NumPy array of shape ``(n_tracks, n_hits_max)``
                representing the padded tracks to add to the DataFrame.
            event_id: An integer representing the ID of the event to which the track
                belongs.
            padding_value: An integer representing the value used to identify padded
                hits. Defaults to 0.
        """
        dataframe = self.from_padded_array_to_dataframe(
            padded_tracks,
            event_id=event_id,
            padding_value=padding_value,
        )
        # Add the dataframe
        self.add_dataframe(dataframe)

    def add_from_padded_csv(
        self,
        paths: typing.List[str],
        padding_value: int = 0,
        skip_header: bool = True,
        ncores: typing.Optional[int] = None,
    ):
        """Load tracks stored in a padded CSV file and add them to the existing
        DataFrame.

        The name of the files must be the event IDs.

        Args:
            paths: A list of paths to the CSV files.
            padding_value: An integer representing the value used to identify padded
                hits. Defaults to 0.
            skip_header: A boolean indicating whether to skip the first line of the
                CSV file. By convention, a header is included in the CSV file.
                Defaults to True.
            ncores: An integer representing the number of cores to use for reading
                the files.
                If not specified, it is read in the configuration.
        """
        # `paths` can be a unique paths, a list of paths, or a unique path with
        # `*` so that `glob` is applied.
        # We uniform this and get a list of paths
        paths = _get_paths(paths)

        # event IDs are always read in the name of the file
        event_ids = _read_numbers_in_filenames(paths)

        # Get number of cores
        if ncores is None:
            ncores = min(self.config["ncores"], len(paths))

        dataframes = []

        def read_csv_to_dataframe(path: str, event_id: int) -> pd.DataFrame | None:
            padded_tracks = _read_csv_pool(path=path, skip_header=skip_header)
            if padded_tracks.size:  # if not empty
                dataframe = self.from_padded_array_to_dataframe(
                    padded_tracks=padded_tracks,
                    event_id=event_id,
                    padding_value=padding_value,
                )
                return dataframe
            else:
                return None

        dataframes: typing.List[pd.DataFrame | None] = Parallel(  # type: ignore
            n_jobs=ncores
        )(
            delayed(read_csv_to_dataframe)(path=path, event_id=event_id)
            for path, event_id in tqdm(zip(paths, event_ids), total=len(paths))
        )

        # Concatenate the dataframes and add the overall dataframe
        overall_dataframe = pd.concat(
            [dataframe for dataframe in dataframes if dataframe is not None],
            axis=0,
        )
        self.add_dataframe(overall_dataframe)

    # ############################## to padded array(s) ##############################
    def from_dataframe_to_padded_arrays(
        self,
        dataframe: pd.DataFrame,
        group_by_events: bool = False,
        padding_value: int = 0,
        nhits_max: typing.Optional[int] = None,
    ) -> typing.Tuple[typing.List[np.ndarray], np.ndarray] | np.ndarray:
        """Converts a Pandas DataFrame of tracks to an array of padded tracks.

        Args:
            dataframe: A Pandas DataFrame of tracks.
            group_by_events: A boolean that determines if the padded arrays are grouped
                by event. Default to False.
            padding_value: An integer that represents the value used to fill in
                the padded cells. Defaults to 0.
            nhits_max: An integer that specifies the maximum number of hits per track.
                Default to None.

        Returns:
            An array of padded tracks.
        """
        # Get columns of interest
        columns = [
            "event_id",
            "track_id",
            "hit_id",
        ]
        event_id_track_id_column_aliases = self.to_aliases(["event_id", "track_id"])

        # To record so that Numba can read id
        record = records.to_record(
            dataframe, columns=columns, alias_to_column_names=self.aliases
        )

        # If `nhits_max` is not given, determine it from the dataframe
        if nhits_max is None:
            nhits_max_ = int(
                dataframe.groupby(event_id_track_id_column_aliases).count().max()  # type: ignore
            )
        else:
            nhits_max_ = int(nhits_max)

        # Counter the number of tracks to build the padded array
        ntracks = dataframe.groupby(event_id_track_id_column_aliases).ngroups

        # Build the padded tracks (Numba JIT-compiled function)
        padded_array = conversion.dataframe_to_padded_array_numba_impl(
            record,
            ntracks=ntracks,
            with_event_id_column=True,
            padding_value=padding_value,
            nhits_max=nhits_max_,
        )
        # Return the padded array(s)
        if group_by_events:
            event_lengths, event_ids = groupby.group_lengths(
                padded_array[:, 0],
            )
            padded_array = np.delete(padded_array, 0, axis=1)
            padded_arrays = groupby.groupby_sorted_array_from_group_lengths(
                padded_array, group_lengths=event_lengths
            )
            return padded_arrays, event_ids
        else:
            return padded_array

    @typing.overload
    def to_padded_arrays(
        self,
        group_by_events: typing.Literal[True],
        padding_value: int = 0,
        nhits_max: typing.Optional[int] = None,
    ) -> typing.Tuple[typing.List[np.ndarray], np.ndarray]:
        ...

    @typing.overload
    def to_padded_arrays(
        self,
        group_by_events: typing.Literal[False],
        padding_value: int = 0,
        nhits_max: typing.Optional[int] = None,
    ) -> np.ndarray:
        ...

    def to_padded_arrays(
        self,
        group_by_events: bool = False,
        padding_value: int = 0,
        nhits_max: typing.Optional[int] = None,
    ) -> typing.Tuple[typing.List[np.ndarray], np.ndarray] | np.ndarray:
        """Convert the current DataFrame of tracks to an array of padded tracks.

        Args:
            group_by_events: A boolean that determines if the padded arrays are
                grouped by event.
                If ``True``, returns a tuple of padded arrays and event IDs.
                If ``False``, returns a single padded array. Defaults to False.
            padding_value: An integer that represents the value used to fill in
                the padded cells. Defaults to 0.
            nhits_max: An integer that specifies the maximum number of hits per
                track. If None, determines the maximum number of hits from the
                DataFrame. Defaults to None.

        Returns:
            If group_by_events is True, returns a tuple containing a list of padded
            arrays and a NumPy array of event IDs. Otherwise, returns a single
            NumPy array of padded tracks.
        """
        return self.from_dataframe_to_padded_arrays(
            self.dataframe,
            group_by_events=group_by_events,
            padding_value=padding_value,
            nhits_max=nhits_max,
        )

    def to_padded_csv(
        self,
        outdir: str,
        padding_value: int = 0,
        nhits_max: typing.Optional[int] = None,
        engine: typing.Optional[str] = None,
        verbose: bool = True,
    ):
        """Saves padded tracks stored in this instance to a directory in
        the form of CSV files. One file per event is created.

        Args:
            outdir: A string representing the directory to save the CSV files.
            padding_value: An integer that represents the value used to fill in
                the padded cells. Defaults to 0.
            nhits_max: An integer that specifies the maximum number of hits per track.
                Default to None.
            engine: A string that specifies the method for saving CSV files, either
                'pyarrow' or 'pandas'.
                Default to None, which means 'pyarrow' will be used.
            verbose: A boolean that determines if the function should print out status
                updates. Default to ``True``.
        """
        # Obtain the padded arrays
        padded_arrays, event_ids = self.to_padded_arrays(
            group_by_events=True,
            padding_value=padding_value,
            nhits_max=nhits_max,
        )

        # Save them
        io.dump_padded_tracks_into_csv(
            outdir=outdir,
            padded_arrays=padded_arrays,
            filenames=event_ids.tolist(),
            engine=engine,
            verbose=verbose,
        )

    # ##################################### Write ####################################
    def to_csv(self, path: str, engine: typing.Optional[str] = None, *args, **kwargs):
        """Write the DataFrame of tracks to a CSV file.

        Args:
            path: The path to the output CSV file.
            engine: The engine to use to write the CSV file. If None and PyArrow is
                installed, will default to ``pyarrow``. Otherwise, will use ``pandas``.
                Defaults to None.
            *args: Additional arguments to pass to Pandas' :py:func:`to_csv` method.
            **kwargs: Additional keyword arguments to pass to Pandas py:func:`to_csv`
                method.
        """
        if utils.libs.pyarrow_installed():
            engine = "pyarrow"

        if engine == "pyarrow":
            import pyarrow as pa
            import pyarrow.csv as pac

            pac.write_csv(pa.Table.from_pandas(self.dataframe), path, *args, **kwargs)
        elif engine == "pandas":
            self.dataframe.to_csv(path, *args, **kwargs)
        else:
            raise ValueError("Engine " + str(engine) + " is not recognised")

    def to_parquet(self, path: str, *args, **kwargs):
        """Write the DataFrame of tracks to a Parquet file.

        Args:
            path: The path to the output Parquet file.
            *args: Additional arguments to pass to Pandas :py:func:`to_parquet` method.
            **kwargs: Additional keyword arguments to pass to Pandas
                :py:func:`to_parquet` method.
        """
        self.dataframe.to_parquet(path, *args, **kwargs)

    # ################################# Constructors #################################
    @classmethod
    def from_dataframe(
        cls,
        dataframe: pd.DataFrame,
        config: typing.Optional[ConfigParams] = None,
    ) -> "TrackHandler":
        """Create a TrackHandler from a Pandas DataFrame.

        Args:
            dataframe: A Pandas DataFrame representing the tracks.
            config: A dictionary of configuration parameters. Defaults to None.

        Returns:
            A new instance of TrackHandler.
        """
        return cls(dataframe=dataframe, config=config)

    @classmethod
    def from_csv(
        cls,
        path: str,
        config: typing.Optional[ConfigParams] = None,
        use_pyarrow: typing.Optional[bool] = None,
        **kwargs,
    ) -> "TrackHandler":
        """Create a TrackHandler from a CSV file.

        Args:
            path: A string representing the path to the CSV file.
            config: A dictionary of configuration parameters. Defaults to None.
            use_pyarrow: A boolean indicating whether to use PyArrow for reading the
                CSV file. If None, PyArrow will be used if it is installed.
            **kwargs: Additional keyword arguments to be passed to Pandas read_csv.

        Returns:
            A new instance of TrackHandler.
        """
        if use_pyarrow is None:
            use_pyarrow = utils.libs.pyarrow_installed()
        if use_pyarrow:
            import pyarrow.csv as pac

            dataframe = pac.read_csv(path, **kwargs).to_pandas()
        else:
            dataframe = pd.read_csv(path, **kwargs)

        return cls(dataframe=dataframe, config=config)

    @classmethod
    def from_parquet(
        cls,
        path: str,
        config: typing.Optional[ConfigParams] = None,
        **kwargs,
    ) -> "TrackHandler":
        """Create a TrackHandler from a Parquet file.

        Args:
            path: A string representing the path to the Parquet file.
            config: A dictionary of configuration parameters. Defaults to None.
            **kwargs: Additional keyword arguments to be passed to Pandas read_parquet.

        Returns:
            A new instance of TrackHandler.
        """
        return cls(
            dataframe=pd.read_parquet(path, **kwargs),
            config=config,
        )

    @classmethod
    def from_padded_csv(
        cls,
        *args,
        config: typing.Optional[ConfigParams] = None,
        **kwargs,
    ) -> "TrackHandler":
        """Load tracks stored in a padded CSV file and return a TrackHandler object.

        The name of the CSV files must be the event ID.

        Args:
            paths: A list of paths to the CSV files.
            padding_value: An integer representing the value used to identify padded
                hits. Defaults to 0.
            skip_header: A boolean indicating whether to skip the first line of the
                CSV file. By convention, a header is included in the CSV file.
                Defaults to True.
            ncores: An integer representing the number of cores to use for reading
                the files.
                If not specified, it is read in the configuration.
            config: A ConfigParams object that contains the configuration parameters to
                initialize the TrackHandler object. Defaults to None.

        Returns:
            A TrackHandler object containing the loaded tracks.
        """
        self = cls(config=config)
        self.add_from_padded_csv(*args, **kwargs)
        return self
