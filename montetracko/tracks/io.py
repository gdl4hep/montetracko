"""Python module to import and export tracks.
"""
from __future__ import annotations
import typing
import os
import os.path as op
from tqdm.auto import tqdm
import numpy as np
from .. import utils


def _get_function_save_array_into_csv(engine: str):
    """Return a function that saves an array of tracks to a CSV file.

    Args:
        engine: String specifying the engine to use for saving the CSV file. Must be
            either ``"pyarrow"`` or ``"pandas"``.

    Returns:
        Function that takes an array of tracks, a list of column names, and a file path,
        and saves the array to a CSV file using the specified engine.

    Raises:
        Exception: If the engine argument is not ``"pyarrow"`` or ``"pandas"``.

    Notes:
        The returned function takes three arguments: an array (``array``), a list of
        column names (``columns``), and the path to the output CSV file (``path``).
        It then saves the array to the CSV file using the specified engine.
    """
    if engine == "pyarrow":
        import pyarrow as pa
        import pyarrow.csv as pac

        def transform_and_save_array(
            array: np.ndarray, columns: typing.List[str], path: str
        ):
            """Save the array of tracks using the ``pyarrow`` engine."""
            table_tracks = pa.Table.from_arrays(array.T, names=columns)
            pac.write_csv(table_tracks, path)

    elif engine == "pandas":
        import pandas as pd

        def transform_and_save_array(
            array: np.ndarray, columns: typing.List[str], path: str
        ):
            """Save the array of tracks using the ``pandas`` engine."""
            dataframe_tracks = pd.DataFrame(array, columns=columns)
            dataframe_tracks.to_csv(path, index=False)

    else:
        raise Exception(
            "engine "
            + str(engine)
            + " is not recognised as a valid engine to write a CSV file"
        )

    return transform_and_save_array


def dump_padded_tracks_into_csv(
    outdir: str,
    padded_arrays: typing.List[np.ndarray],
    filenames: typing.Iterable[str] | None = None,
    engine: typing.Optional[str] = None,
    verbose: bool = True,
):
    """Save a list of arrays of tracks to CSV files.

    Args:
        list_arrays_tracks: List of arrays of tracks to save. Each element in the list
            corresponds to one event, and each array should have the same number of
            columns: the event number and the hit IDs of the tracks.
        outdir: Path to the output directory where the CSV files will be saved.
        filenames: Optional list of filenames for the CSV files. If not provided, the
            CSV files will be named after the index of the corresponding element in
            ``list_arrays_tracks``.
        engine: Optional string specifying the engine to use for saving the CSV
            files. Must be either ``pyarrow`` or ``pandas``.
            If not provided, the functionwill use the "pyarrow" engine by default.
        verbose: Whether to print the ouput directory
    """
    # Get saving function according to the engine that is used
    if engine is None:
        if utils.libs.pyarrow_installed():
            engine = "pyarrow"
        else:
            engine = "pandas"
    save_array_into_csv = _get_function_save_array_into_csv(engine=engine)

    # Check the `padded_arrays` has the correct formatting
    assert isinstance(padded_arrays, list)
    assert len(padded_arrays) > 0, "List `list_arrays_tracks` is empty"

    # Name of the CSV files (from 0 to number of events - 1, by default)
    if filenames is None:
        filenames = [
            str(idx) for idx in range(len(padded_arrays))
        ]

    # Use first array to figure out maximal number of hits,
    example_array_tracks = padded_arrays[0]
    nhits_max = example_array_tracks.shape[1]
    csv_columns = [f"plane{i}" for i in range(nhits_max)]

    os.makedirs(outdir, exist_ok=True)

    # Loop over events (or padded arrays)
    for filename, padded_array in tqdm(
        zip(filenames, padded_arrays), total=len(padded_arrays)
    ):
        save_array_into_csv(
            array=padded_array,
            columns=csv_columns,
            path=op.join(outdir, f"{filename}.csv"),
        )

    if verbose:
        print("Tracks were saved in CSV files in", outdir)
