"""Package that handle tracks.
"""
from . import (
    conversion,
    generator,
    trackhandler,
    io,
)

from .trackhandler import TrackHandler

__all__ = [
    "conversion",
    "generator",
    "trackhandler",
    "io",
]

__all__ += ["TrackHandler"]
