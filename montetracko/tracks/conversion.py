"""Module that defines function to convert from and to various track representations.
"""
from __future__ import annotations
import numpy as np
import numba as nb
from ..array_utils import groupby, transform
from ..config import NUMBA_CONFIG


def padded_array_to_trackid_hitid_couples(
    padded_tracks: np.ndarray,
    padding_value: int = 0,
):
    """Converts an array of padded tracks to an alternate representation with
    an array of hit IDs and an array of the corresponding track IDs.

    The array of padded tracks is as follows:

    .. code-block:: python

        [
            [hit0, hit1, ..., hit10, padding_value, ..., padding_value] # Track 1
            [hit0, hit1, ..., hit19, padding_value, ..., padding_value] # Track 2
            [hit0, hit1, ..., ...,                       hit25        ] # Track 3
            # ...
        ]

    Args:
        padded_tracks: A NumPy array of shape ``(n_tracks, n_hits_max)`` representing
            the padded tracks to convert to a DataFrame.
        padding_value: An integer representing the value used to identify padded hits.
            Defaults to 0.

    Returns:
        A tuple of two NumPy arrays. The first array contains the track IDs and the
        second array contains the hit IDs. Only non-padded hits are included in the
        hit IDs array.

    Notes:
        This function could be compiled with Numba.
    """
    padded_tracks = np.asarray(padded_tracks)

    # In the case where there is only one track, 1D -> 2D
    if padded_tracks.ndim == 1:
        padded_tracks = padded_tracks.reshape((1, -1))

    ntracks, nhits_max = padded_tracks.shape

    # flatten (2D -> 1D)  => Define `hit_id` column
    hit_ids_column = padded_tracks.flatten()

    # Define `track_id` column
    track_ids_column = np.repeat(np.arange(ntracks), nhits_max)

    # Mask to remove the padding hits
    padding_hits_mask = hit_ids_column != padding_value

    # Return tuple (track IDs, hit IDs)
    return (
        track_ids_column[padding_hits_mask],
        hit_ids_column[padding_hits_mask],
    )


@nb.jit(**NUMBA_CONFIG)
def dataframe_to_padded_array_numba_impl(
    record_events_tracks_hits,
    ntracks,
    nhits_max: int,
    padding_value: int = 0,
    with_event_id_column: bool = False,
):
    """Convert a structured NumPy array (representing track hits in events) to a 2D
    NumPy array representing padded tracks.

    Args:
        record_events_tracks_hits: A structured NumPy array with columns ``event_id``,
            ``track_id``, and ``hit_id`` representing the hits for each track
            in each event.
        ntracks: the number of tracks in total. It is used to create the
            initial outputted array
        nhits_max: maximum number of hits in a track, used to determine the shape
            of the output ``padded_tracks`` NumPy array.
            If a track has fewer than ``nhits_max`` hits, the missing hits are filled
            with a ``padding_value``
        padding_value: An integer representing the value used to fill the padded tracks.
            Default to 0.
        with_event_id_column: A boolean indicating whether the first column of the
            padded tracks should represent the event ID. Default to False.

    Returns:
        A 2D NumPy array representing the padded tracks with shape
        ``(ntracks, nhits_max + 1)``
        if ``with_event_id_column`` is True or ``(ntracks, nhits_max)`` otherwise.
    """
    #: List of arrays of tracks. This is the object returned
    padded_tracks = np.full(
        shape=(ntracks, int(with_event_id_column) + nhits_max),
        dtype=np.int_,
        fill_value=padding_value,
    )

    # Group by events
    event_lengths, event_ids = groupby.group_lengths(
        record_events_tracks_hits["event_id"]
    )
    track_idx = 0
    for record_tracks_hits, event_id in zip( # type: ignore
        groupby.groupby_sorted_array_from_group_lengths(
            record_events_tracks_hits, event_lengths
        ),
        event_ids,
    ):
        # Group by track IDs
        # Use merge sort to avoid to change sorting if already sorted.
        record_tracks_hits = record_tracks_hits[
            record_tracks_hits["track_id"].argsort(kind="mergesort")
        ]
        track_lengths, _ = groupby.group_lengths(record_tracks_hits["track_id"])

        # Loop over the tracks
        for record_hits in groupby.groupby_sorted_array_from_group_lengths(
            record_tracks_hits,
            track_lengths,
        ):
            #: Array of `hit_id` for the given `track_id`
            track = np.asarray(record_hits["hit_id"], dtype=np.int_)
            # print(track)
            padded_track = transform.get_padded_array(
                array=track, length_max=nhits_max, fill_value=padding_value
            )
            if with_event_id_column:
                full_track = np.concatenate((np.asarray([event_id]), padded_track))
            else:
                full_track = padded_track
            # Store the track in the big `array_tracks` for the given event
            padded_tracks[track_idx, :] = full_track
            track_idx += 1

    return padded_tracks[:track_idx, :]
