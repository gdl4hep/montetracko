"""Module to generate tracks.
"""
import typing
import numpy as np
import numba as nb
import pandas as pd

from ..array_utils import transform, groupby, records
from ..config import NUMBA_CONFIG


@nb.jit(**NUMBA_CONFIG)
def should_save_track_passthrough(nhits_track: int) -> bool:
    """A function that always returns ``True``."""
    return True


@nb.jit(**NUMBA_CONFIG)
def should_save_track_ge_3_hits(nhits_track: int) -> bool:
    """Return ``True`` if number of hits in track is higher or equal than 3.

    Args:
        nhits_track: Number of hits in the track.

    Returns:
        ``True`` if the number of hits in the track is greater than or equal to 3,
        ``False`` otherwise.

    Notes:
        used as default by :py:func:`get_list_perfect_tracks_events`
    """
    return nhits_track >= 3


@nb.jit(**NUMBA_CONFIG)
def get_list_perfect_tracks_events_numba(
    record_hits: np.ndarray,
    nhits_max: int,
    condition: typing.Callable[[int], bool] = should_save_track_passthrough,
    event_ids: typing.Optional[np.ndarray] = None,
    fill_value: int = 0,
    with_event_col: bool = False,
) -> typing.List[np.ndarray]:
    """Extract tracks from hits array, where a track is a MC particle.

    Args:
        record_hits: Array containing the hits. It is assumed that the hits
            are grouped by events. The columns of the array must include
            the event (``event_id``), MC particle (``particle_id``), and hit
            (``hit_id``) identifiers.
        nhits_max: Maximum number of hits per track. Tracks with more than
            ``nhits_max`` hits will be truncated.
        condition: Function that takes the number of hits in a track as input,
            and returns a boolean indicating whether the track
            should be saved or not. Defaults to ``should_save_track_passthrough``, which
            always returns ``True``.
        event_ids: Array of event IDs to use when constructing the output.
            If not ``None``, events with no tracks will still be included in the output,
            as empty arrays with the correct event ID. Defaults to None.
        fill_value: padding value to use for the tracks with more less that
            ``nhits_max`` hits. Defualt to 0.
        with_event_col: whether each track should contain the event number as
            the first column.

    Returns:
        List of track arrays. Each track array contains one row per
        track, and ``nhits_max + 1`` columns. The first column is the event number, and
        the remaining columns are the hits in the track. The list is indexed by event
        number, with one array per event.
    """
    fill_value = int(fill_value)

    #: List of arrays of tracks. This is the object returned
    list_arrays_tracks = []

    # Loop over events
    event_idx = 0

    # Group by events
    event_lengths, event_ids = groupby.group_lengths(record_hits["event_id"])

    for record_hits_event, event_id in zip( # type: ignore
        groupby.groupby_sorted_array_from_group_lengths(record_hits, event_lengths),
        event_ids,
    ):
        # Group by MC particles to form the tracks
        record_sorted = record_hits_event[record_hits_event["particle_id"].argsort()]
        mcp_lengths, mcpids = groupby.group_lengths(record_sorted["particle_id"])
        # Use the number of `mcid`s to find out the maximal number of expected tracks
        # Build the array that will contain all the track
        array_tracks = np.full(
            shape=(len(mcpids), int(with_event_col) + nhits_max),
            dtype=np.int_,
            fill_value=fill_value,
        )

        # If `event_ids` is specified, this allows to append events without tracks
        # in `list_arrays_tracks`, as array of size 0.
        # The latter is used to export tracks with the right `event_idx`, as
        # this kind of events will be missed which mess up `event_idx`.
        if event_ids is not None:
            while (event_ids[event_idx] != event_id) and (event_idx < len(event_ids)):
                list_arrays_tracks.append(
                    np.full(
                        shape=(0, int(with_event_col) + nhits_max),
                        dtype=np.int_,
                        fill_value=fill_value,
                    )
                )
                event_idx += 1
            # check that the end of the list was not reached
            assert event_idx != len(event_ids)

        # Loop over the tracks (or MC particles)
        ntracks_reco = 0
        for record_hits_event_mcid in groupby.groupby_sorted_array_from_group_lengths(
            record_sorted,
            mcp_lengths,
        ):
            #: Array of `lhcbid` for the given `mcid`
            track = np.asarray(record_hits_event_mcid["hit_id"], dtype=np.int_)
            nhits_track = len(track)
            if condition(nhits_track):  # whether to save the track
                padded_track = transform.get_padded_array(
                    array=track, length_max=nhits_max, fill_value=fill_value
                )
                if with_event_col:
                    full_track = np.concatenate((np.asarray([event_id]), padded_track))
                else:
                    full_track = padded_track
                # Store the track in the big `array_tracks` for the given event
                array_tracks[ntracks_reco, :] = full_track
                ntracks_reco += 1

        # Append the `array_track` to the list of `array_track`s
        list_arrays_tracks.append(array_tracks[:ntracks_reco, :])
        event_idx += 1

    return list_arrays_tracks


def get_list_perfect_tracks_events(
    hits: pd.DataFrame,
    requirement: typing.Optional[str] = None,
    nhits_max: int = 26,
    condition: typing.Callable[[int], bool] = should_save_track_ge_3_hits,
    with_event_col: bool = False,
    mcparticle_fake_hit: int = 0,
    event_id_column_name: str = "event_id",
    particle_id_column_name: str = "particle_id",
    hit_id_column_name: str = "hit_id",
) -> typing.List[np.ndarray]:
    """Extract tracks from VELO hits, where a track is a MC particle.

    Args:
        hits: DataFrame containing the VELO hits. It is assumed that the hits are
            grouped by event number,
            The DataFrame must include columns for the event, MC particle and hit
            IDs.
        requirement: Optional string specifying an additional requirement that the
            hits must meet. If specified, the requirement will be applied to the hits
            DataFrame using the ``query`` method. Defaults to None.
        nhits_max: Maximum number of hits per track. Tracks with more than
            ``nhits_max`` hits will be truncated. Default to 26.
        condition: Function that takes the number of hits in a track as input,
            and returns a boolean indicating whether the track
            should be saved or not. Defaults to ``should_save_track_ge_3_hits``, which
            returns ``True`` if the track has at least 3 hits.
        with_event_col: whether each track should contain the event number as
            the first column.
        mcparticle_fake_hit: number representing a fake hit in the
            ``hit_id_column_name`` column of the ``hits`` DataFrame.

    Returns:
        List of track arrays. Each track array contains one row per track, and
        ``nhits_max + 1`` columns. The first column is the event number, and the
        remaining columns are the hits in the track. The list is indexed by event
        number, with one array per event. Only tracks is at least 3 hits
        are saved.
    """
    hits = pd.DataFrame(hits)

    # Get the non-ordered list of events before filtering
    event_ids = hits[event_id_column_name].unique()
    # Remove fake hits
    hits_true = hits[hits[particle_id_column_name] != mcparticle_fake_hit]
    # Apply requirements
    if requirement is not None:
        assert isinstance(requirement, str)
        hits_true = hits_true.query(requirement)

    # Turn into a NumPy structured array
    columns = [event_id_column_name, particle_id_column_name, hit_id_column_name]
    record_hits = records.to_record(
        hits_true,
        columns,
        alias_to_column_names={
            "event_id": event_id_column_name,
            "particle_id": particle_id_column_name,
            "hit_id": hit_id_column_name,
        },
    )
    # Get the list of perfect tracks
    return get_list_perfect_tracks_events_numba(
        record_hits=record_hits,
        nhits_max=nhits_max,
        condition=condition,
        event_ids=event_ids,
        with_event_col=with_event_col,
    )
