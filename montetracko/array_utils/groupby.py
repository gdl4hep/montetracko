"""Module that implements groupby operations.
"""
import typing
import numpy as np
import numba as nb
from ..config import NUMBA_CONFIG


@nb.jit(**NUMBA_CONFIG)
def group_lengths(
    array: np.ndarray,
) -> typing.Tuple[np.ndarray, np.ndarray]:
    """Computes the run lengths of of a one-dimensional numpy array.

    Args:
        array: A sorted 1D array
        return_group_values: A boolean flag indicating whether to return the list of
            values that each run corresponds to. If True, returns a tuple of two arrays:
            the run lengths, and the corresponding run values. If False (default),
            returns only the run lengths.

    Returns:
        A tuple of two 1D numpy arrays:
        the first array contains the run lengths, and the second array contains
        the list of values that each run corresponds to.
    """
    group_lengths = []
    group_idx = -1
    group_value = np.nan
    group_values = []

    for column_value in array:
        if group_value != column_value:
            group_value = column_value
            group_idx += 1
            group_lengths.append(1)
            group_values.append(group_value)
        else:
            group_lengths[group_idx] += 1

    return np.asarray(group_lengths), np.asarray(group_values)


@nb.jit(**NUMBA_CONFIG)
def groupby_sorted_array_from_group_lengths(
    array: np.ndarray,
    group_lengths: np.ndarray,
) -> typing.List[np.ndarray]:
    """Group an array of sorted values by the lengths of each group.

    This function takes an array of sorted values and a 1D array specifying the
    length of each group, and returns a list of arrays, where each array contains
    the values in a group. The input array and group lengths must be sorted in
    ascending order.

    Args:
        array: A 2D numpy array containing the values to group.
        group_lengths: A 1D numpy array containing the length of each group.

    Returns:
        A list of numpy arrays, where each array contains the values in a group.
    """
    array = np.asarray(array)
    groups = []
    run_index = 0

    for run_length in group_lengths:
        groups.append(array[run_index : run_index + run_length])
        run_index += run_length

    return groups


@nb.jit(**NUMBA_CONFIG)
def groupby_sorted_array(
    array: np.ndarray,
    column_idx,
    sort: bool = False,
) -> typing.Tuple[typing.List[np.ndarray], np.ndarray]:
    """
    Groups the rows of a sorted 2D numpy array by the unique values
    in a specified column.

    Args:
        array: A 2D numpy array where the specified column is sorted.
        column_idx: The index of the column to group by.
        sort: whether to sort the ``array``. Only use if the ``array`` is not already
            sorted.

    Returns:
        A tuple of two 1D numpy arrays:
        the first array contains the grouped arrays, and the second array contains
        the list of values that the grouping column takes for each group.
    """
    if sort:
        array_sorted = array[array[:, column_idx].argsort(), :]
    else:
        array_sorted = array

    run_lengths, group_values = group_lengths(
        array_sorted[:, column_idx],
    )
    groups = groupby_sorted_array_from_group_lengths(array_sorted, run_lengths)

    return groups, group_values
