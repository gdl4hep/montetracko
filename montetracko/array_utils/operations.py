"""Define operations using numba.
"""
from __future__ import annotations
import numpy as np
import numba as nb
from ..config import NUMBA_CONFIG


def is_sorted(array: np.ndarray) -> bool:
    """Check if an array is sorted."""
    return bool(np.all(array[:-1] <= array[1:]))


@nb.jit(**NUMBA_CONFIG)
def where_first(array, value: int | float = 0) -> int | None:
    """Return the index of the first occurrence of a value in the input array.

    Args:
        array: The input array to search.
        value: The value to search for in the input array. The default value is 0.

    Returns:
        The index of the first occurrence of the specified value in the
        input array, or ``None`` if the value is not found.
    """
    value = array.dtype.type(value)
    for idx, el in enumerate(array):
        if el == value:
            return idx
    return None


@nb.jit(**NUMBA_CONFIG)
def _check_equality_list_arrays_numba(arrays1, arrays2):
    """Check if two lists of arrays are equal element-wise.

    Args:
        arrays1: First list of arrays to compare.
        arrays2: Second list of arrays to compare.

    Returns:
        Boolean indicating whether the two lists of arrays are equal element-wise.

    Notes:
        Does not support empty list and lists of different sizes.
        Use :py:func:`check_equality_list_arrays` instead;
    """
    for array1, array2 in zip(arrays1, arrays2): # type: ignore
        array1 = np.asarray(array1)
        array2 = np.asarray(array2)
        if not len(array1) == len(array2) or not np.all(array1 == array2):
            return False
    return True


def check_equality_list_arrays(arrays1, arrays2):
    """Check if two lists of arrays are equal element-wise.

    Args:
        arrays1: First list of arrays to compare.
        arrays2: Second list of arrays to compare.

    Returns:
        Boolean indicating whether the two lists of arrays are equal element-wise.
    """
    len1 = len(arrays1)
    len2 = len(arrays2)
    if len1 != len2:
        return False
    elif len1 == 0 or len2 == 0:
        return len1 == len2
    else:
        return _check_equality_list_arrays_numba(arrays1, arrays2)


@nb.jit(**NUMBA_CONFIG)
def find_sorted_array(array: np.ndarray, value) -> int:
    """Find the index of a value in a sorted NumPy array.

    Args:
        array: The NumPy array to search in.
        value: The value to look for in the array.

    Returns:
        The index of the value in the array if it exists, otherwise -1.
    """
    found_idx = np.searchsorted(array, value)
    if found_idx < len(array) and array[found_idx] == value:
        return found_idx
    else:
        return -1
