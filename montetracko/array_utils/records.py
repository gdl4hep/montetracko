"""Module to handle NumPy records.
"""
import typing
import numpy as np
from numpy.core import records
import pandas as pd


def to_record(
    dataframe: pd.DataFrame,
    columns: typing.List[str],
    alias_to_column_names: typing.Optional[typing.Dict[str, str]] = None,
):
    """Convert a subset of a pandas DataFrame to a numpy record array.

    This function takes a pandas DataFrame, a list of column names, and an optional
    dictionary that maps column aliases to their corresponding column names in the
    DataFrame. It returns a numpy record array containing the values in the specified
    columns.

    If an ``alias_to_column_names`` dictionary is provided, it is used to map column
    aliases to their corresponding column names in the DataFrame. Otherwise, the
    default mapping provided in :py:mod:`montetracko.config` is used.

    Args:
        dataframe: A pandas DataFrame containing the data to convert to a record array.
        columns: A list of column names to include in the record array.
        alias_to_column_names: A dictionary that maps column aliases to their
            corresponding column names in the DataFrame, if any.

    Returns:
        A numpy record array containing the values in the specified columns.

    Raises:
        KeyError: If any of the specified column names are not present in the DataFrame.
    """
    if alias_to_column_names is None:
        alias_to_column_names = {}
    dtype = np.dtype(
        [
            (column, dataframe[alias_to_column_names.get(column, column)].dtype)
            for column in columns
        ]
    )

    return np.asarray(
        records.fromarrays(
            [
                dataframe[alias_to_column_names.get(column, column)]
                for column in columns
            ],
            dtype=dtype,
        )
    )
