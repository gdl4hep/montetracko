"""Module to transform nested structures using numba.
"""
from __future__ import annotations
import typing
import numpy as np
import numba as nb
from . import operations
from ..config import NUMBA_CONFIG

try:
    import awkward as ak  # type: ignore
except ModuleNotFoundError:
    raise ModuleNotFoundError(
        "The `awkward` library could not be imported. "
        "Install it with `pip install awkward` or "
        "`conda install -c conda-forge awkward`"
    )


@nb.jit(**NUMBA_CONFIG)
def _nested_list_to_padded_array_numba(
    arrays: ak.Array,
    ncols: typing.Optional[int] = None,
    fill_value: int | float = 0,
) -> np.ndarray:
    """See the documentation of :py:func:`nested_list_to_padded_array`.
    This function is a Numba-optimized implementation of
    :py:func:`nested_list_to_padded_array`, which wraps `arrays` into an Awkward Array
    to support list of lists as input even though Numba is used.
    """
    # Find the length of each array
    lengths = np.zeros(shape=len(arrays), dtype=np.int64)
    for row_idx, array in enumerate(arrays):
        lengths[row_idx] = len(array)

    # Number of rows and columns in the new big array
    nrows = len(arrays)
    ncols_ = lengths.max() if ncols is None else ncols

    # Create the big array by filling it with `fill_value` (`np.nan` does not work)
    big_array = np.full(
        shape=(nrows, ncols_),
        dtype=np.asarray(arrays[0]).dtype,
        fill_value=fill_value,
    )
    for row_idx, array in enumerate(arrays):
        array = np.asarray(arrays[row_idx])
        big_array[row_idx, : min(lengths[row_idx], ncols_)] = array[:ncols_]
    return big_array


def nested_list_to_padded_array(
    arrays: (ak.Array | typing.List[typing.List[int] | typing.List[float]]),
    ncols: typing.Optional[int] = None,
    fill_value: int | float = 0,
) -> np.ndarray:
    """Transform a nested list of different lengths into a 2D NumPy array.

    This function takes a nested list of arrays and returns a 2D NumPy array
    where each subarray is aligned to the left and padded with a fill value
    (0 by default) to match the length of the longest subarray.
    If a maximum number of columns is specified, shorter subarrays are truncated
    and longer subarrays are padded.

    Args:
        arrays: A nested list of arrays to be transformed into a 2D NumPy array.
        ncols: Optional integer specifying the maximum number of columns in the
            resulting array.
            If not specified, the number of columns will be the length of the
            longest subarray.
        fill_value: The value to use for padding shorter subarrays.

    Returns:
        A 2D NumPy array with the same data as the input list,
        aligned and padded as needed.

    Examples:
        >>> arrays = [[1, 2, 3], [4, 5], [6]]
        >>> nested_list_to_padded_array(arrays)
        array([[1, 2, 3],
            [4, 5, 0],
            [6, 0, 0]])

        >>> nested_list_to_padded_array(arrays, ncols=2)
        array([[1, 2],
            [4, 5],
            [6, 0]])
    """
    arrays = ak.Array(arrays)
    return _nested_list_to_padded_array_numba(
        arrays=arrays, ncols=ncols, fill_value=fill_value
    )


@nb.jit(**NUMBA_CONFIG)
def padded_array_to_nested_list(
    array: np.ndarray,
    fill_value: int | float = 0,
) -> typing.List[np.ndarray]:
    """Converts a padded 2D numpy array back to a list of numpy arrays.

    Args:
        array: The padded 2D numpy array to be converted.
        fill_value: The fill value used to pad the array. Defaults to 0.

    Returns:
        A list of numpy arrays where each element of the list corresponds to a
        row of the original padded array. The resulting numpy arrays have the
        same data type as the original padded array.

    Example:
        >>> array = np.array([[1, 2, 3], [4, 5, 0], [6, 0, 0]])
        >>> nested_list = padded_array_to_nested_list(array)
        >>> print(nested_list)
        [array([1, 2, 3]), array([4, 5]), array([6])]
    """
    arrays = []

    for row in array:
        idx_stop = operations.where_first(row, fill_value)
        if idx_stop is None:
            arrays.append(row)
        else:
            arrays.append(row[:idx_stop])
    return arrays
