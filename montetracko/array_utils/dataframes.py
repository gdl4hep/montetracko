"""Module that implements helper functions for Pandas DataFrames.
"""
from __future__ import annotations
import typing
import pandas as pd

#: List of the possible ways of printing a Pandas DataFrame
POSSIBLE_TABLE_MODES = ["string", "markdown", "latex", "html", "csv", "pandas"]

#: Type of the `mode` argument that indicates the way of printing a pandas DataFrame
TableMode = typing.Literal["string", "markdown", "latex", "html", "csv", "pandas"]


def count_distinct_groups(
    dataframe: pd.DataFrame,
    columns: typing.List[str],
    groupby: typing.Optional[typing.List[str]] = None,
) -> pd.Series | int:
    """Compute the number of unique groups in a Pandas DataFrame.

    Given a Pandas DataFrame, compute the number of unique groups in the specified
    columns, optionally grouped by the specified groupby columns.

    Args:
        dataframe: A Pandas DataFrame to compute unique groups on.
        columns: A list of column names in the DataFrame to compute unique groups on.
        groupby: A list of column names to group the unique groups by, if any.
            If not specified, the unique groups will be computed overall.

    Returns:
        The number of unique groups in the specified columns, optionally grouped by the
        specified groupby columns.

    Raises:
        ValueError: If any of the specified column or groupby names do not exist
            in the DataFrame.

    Examples:
        Consider the following Pandas DataFrame:

            >>> import pandas as pd
            >>> df = pd.DataFrame({
            ...     'A': ['a', 'b', 'c', 'd', 'a', 'b', 'c', 'd'],
            ...     'B': [1, 2, 1, 2, 1, 2, 1, 2],
            ...     'C': ['x', 'y', 'x', 'y', 'x', 'y', 'x', 'y'],
            ... })

        To count the unique groups for columns `A` and `B` without grouping, call:

            >>> count_distinct_groups(df, ['A', 'B'])
            4

        To count the unique groups for columns `A` and `B`, grouped by column `C`, call:

            >>> count_distinct_groups(df, ['A', 'B'], ['C'])
            C
            x    2
            y    2
            dtype: int6
    """
    if groupby is None:
        groupby = []
    # Overall columns with respect to groupby, without duplicates
    overall_groupby = columns + [el for el in groupby if el not in columns]

    # Use `groupby` to get unique values
    dataframe_grouped_by = dataframe.groupby(overall_groupby).size()

    if groupby:
        # Corresponds to the number of groups in `columns`, grouped by `groupby`
        return dataframe_grouped_by.groupby(groupby).size()
    else:
        # corresponds to the number of groups in `columns`
        return dataframe_grouped_by.shape[0]


def sum_column_count(
    dataframe: pd.DataFrame,
    count_column: str,  # existing column name
    groupby: typing.Optional[typing.List[str]] = None,  # groupby operation
):
    """Compute the sum of values in a column, grouped by one or more columns.

    Args:
        dataframe: A Pandas DataFrame containing the data.
        count_column: The name of the column to compute the sum of.
        groupby: A list of column names to group by. If not specified, the sum
            is computed over the entire column.

    Returns:
        A Pandas Series containing the sum of values in `count_column`,
        grouped by the specified columns.

    Examples:
        Consider the following Pandas DataFrame:

            >>> import pandas as pd
            >>> df = pd.DataFrame({
            ...     'A': ['a', 'b', 'c', 'a', 'b', 'c'],
            ...     'B': [1, 2, 1, 1, 2, 1],
            ...     'C': [10, 20, 30, 40, 50, 60],
            ... })

        We can compute the sum of values in column 'C', grouped by column 'A':

            >>> compute_column_count(df, 'C', ['A'])
            A
            a    50
            b    70
            c    90
            Name: C, dtype: int64

        We can also compute the sum of values in column 'C' over the entire DataFrame:

            >>> compute_column_count(df, 'C')
            210
    """
    if groupby:
        return dataframe.groupby(groupby)[count_column].sum()
    else:
        return dataframe[count_column].sum()


def get_table_representation(
    table: pd.DataFrame,
    mode: TableMode = "string",
    **kwargs,
) -> str | pd.DataFrame:
    """Returns a string representation of a pandas DataFrame in the specified format.

    Args:
        table: A pandas DataFrame to represent.
        mode: The format to return the DataFrame in. Valid options are
            "markdown", "string", "latex", "html", "csv", and "pandas".
            Default is "string".
        **kwargs: Optional keyword arguments to pass to the pandas formatting functions.

    Returns:
        A string representation of the DataFrame in the specified format.

    Raises:
        ValueError: If  ``mode`` is not a valid option.
    """
    if mode == "string":
        return table.to_string(**kwargs)
    elif mode == "markdown":
        return table.to_markdown(**kwargs)
    elif mode == "latex":
        return table.style.to_latex(**kwargs)
    elif mode == "html":
        return table.style.to_html(**kwargs)
    elif mode == "csv":
        return table.to_csv(**kwargs)
    elif mode == "pandas":
        return table
    else:
        raise ValueError(
            f"`mode` is {mode} but should be one of these values: "
            + ", ".join(POSSIBLE_TABLE_MODES)
        )
