"""Module that implements helper functions to handle pandas series.
"""
from __future__ import annotations
import typing
import numpy as np
import pandas as pd


def broadcast_index_series(subseries: pd.Series, series: pd.Series) -> pd.Series:
    """
    Reindexes a Pandas Series with a single-level or multi-level index to match
    the index of another Series with a multi-level index.

    Args:
        subseries: The Pandas Series to broadcast with a matching index.
        series: The Pandas Series with the target index.

    Returns:
        The reindexed Pandas Series with the same index as ``series`` and
        values from ``subseries``.

    Raises:
        ValueError: If the index names of ``subseries`` are not a subset of the
        index names of ``series``.

    Examples:
        >>> series1 = pd.Series(
                [879, 1237, 185, 3317, 1477],
                index=pd.Index([148345, 148346, 148347, 148348, 148349], name="event"),
                name="n_hits_particle",
            )
        >>> series2 = pd.Series(
                [2, 2, 2, 4, 4, 0, 6, 0, 0, 0],
                index=pd.MultiIndex.from_tuples(
                    [
                        (148345, 240),
                        (148345, 241),
                        (148345, 246),
                        (148345, 251),
                        (148345, 253),
                        (148349, 3321),
                        (148349, 3328),
                        (148349, 3329),
                        (148349, 3361),
                        (148349, 3364),
                    ],
                    names=["event", "mcid"],
                ),
                name="n_hits_particle",
            )
        >>> broadcast_index_series(series1, series2)
                event   mcid
        148345  240      879
                241      879
                246      879
                251      879
                253      879
        148349  3321    1477
                3328    1477
                3329    1477
                3361    1477
                3364    1477
        Name: n_hits_particle, dtype: int64
    """
    sub_index_names = subseries.index.names if isinstance(subseries, pd.Series) else []
    index_names = series.index.names if isinstance(series, pd.Series) else []
    if not set(sub_index_names).issubset(index_names):
        raise ValueError(
            "Index names of the sub-series are "
            + ", ".join(index_names)
            + " with is not included in the big series, whose index names are: "
            + ", ".join(index_names)
        )
    elif len(sub_index_names) == len(index_names):
        # In the case where the indices are the same, just the subseries
        # with the index names in the correct order
        if len(index_names) == 1 or len(sub_index_names) == 0:
            return subseries
        else:
            return subseries.reorder_levels(index_names)
    elif isinstance(subseries, pd.Series):
        # Re-order `series` index names so that the beginning of the index names
        # are the same between the two series
        reordered_index_names = sub_index_names + [
            index_name
            for index_name in index_names
            if index_name not in sub_index_names
        ]
        series = series.reorder_levels(reordered_index_names)
        # Return the broadcasted Series
        if len(sub_index_names) == 1:
            sub_index_name = sub_index_names[0]
            return subseries.reindex(
                series.index,
                level=sub_index_name,
            ).reorder_levels(index_names)
        else:
            return subseries.reindex(series.index).reorder_levels(index_names)
    else:
        return pd.Series(
            np.full(fill_value=subseries, shape=len(series)),
            index=series.index,
        )


def merge_series_indices(
    collection_series: typing.Iterable[pd.Series],
    merging: typing.Optional[typing.Literal["union", "intersection"]] = None,
) -> typing.List[pd.Series]:
    """Merge the indices of a list of Pandas Series.

    Given a collection of Pandas Series, this function merges their indices and
    reindexes them, optionally using a union or intersection operation.

    Args:
        list_series: A list of Pandas Series to merge and reindex.
        merging: Whether to perform a union or intersection operation on the indices
            of the series.

            * If ``None``, the indices of the series must match exactly.
            * If ``"union"``, the merged index will contain all unique values from
                the individual indices. 0 is used for missing values.
            * If ``"intersection"``, only the common values between all indices
                will be used. Defaults to None.

    Returns:
        A list of reindexed Pandas Series with merged indices.

    Raises:
        ValueError: If the merging argument is not "union", "intersection", or None,
            or if the indices of the input series do not match exactly when merging
            is None.

    Example:
        >>> s1 = pd.Series([1, 2, 3], index=['a', 'b', 'c'])
        >>> s2 = pd.Series([4, 5, 6], index=['a', 'c', 'd'])
        >>> merge_series_indices([s1, s2], merging='union')
        [a    1
        b    2
        c    3
        d    0
        dtype: int64,
        a    4
        b    0
        c    5
        d    6
        dtype: int64]
    """
    indices = [metric_value.index for metric_value in collection_series]
    # Merge indices
    overall_index = indices[0]
    if merging is None:
        for index in indices[1:]:
            if not index.equals(overall_index):
                raise ValueError(
                    "As `merging` is set to `None`, the indices should be equal"
                )
    elif merging == "union":
        for index in indices[1:]:
            assert overall_index.names == index.names
            overall_index = overall_index.union(index)
    elif merging == "intersection":
        for index in indices[1:]:
            assert overall_index.names == index.names
            overall_index = overall_index.intersection(index)
    else:
        raise ValueError(
            '`merging` should either be `None`, `"union"` or `"intersection"` '
            "but is " + str(merging)
        )

    list_reindexed_series = []
    for series in collection_series:
        list_reindexed_series.append(
            series.reindex(
                overall_index,
                fill_value=0 if merging == "union" else np.nan,
            )
        )
        # There should not be any NaN values after reindexing
        assert not list_reindexed_series[-1].isna().any()

    return list_reindexed_series
