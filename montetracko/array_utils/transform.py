"""Module to transform arrays.
"""
from __future__ import annotations
import numpy as np
import numpy.typing as npt
import numba as nb

from . import operations
from ..config import NUMBA_CONFIG


@nb.jit(**NUMBA_CONFIG)
def repeated_arange(stops: np.ndarray):
    """Return an array of integers with a sequence of consecutive integers
    repeated multiple times based on the values in the `stops` array.

    Args:
        stops (numpy.ndarray): A 1D array of integers specifying the number of
            consecutive integers to generate for each repetition.

    Returns:
        numpy.ndarray: A 1D array of integers containing the repeated sequences.

    Notes:
        This function is compiled using Numba for faster execution.
    """
    stops = np.asarray(stops)
    total_length = stops.sum()
    array = np.empty(shape=total_length, dtype=np.int_)
    running_idx = 0
    for stop in stops:
        end_idx = running_idx + stop
        array[running_idx:end_idx] = np.arange(stop)
        running_idx = end_idx
    return array


@nb.jit(**NUMBA_CONFIG)
def get_padded_array(
    array: npt.ArrayLike,
    length_max: int,
    fill_value: int | float = 0,
) -> np.ndarray:
    """Pad an array with a fill value or zeros to a desired length.

    Args:
        array: Input array-like object to be padded. It will be casted to a NumPy
            array.
        length_max: Desired length of the padded array.
        fill_value: Value to be used to fill the padded region. Default to 0.

    Returns:
        NumPy ndarray with the same dtype as the input array.
        If the input array has length less than ``length_max``,
        it is padded with ``fill_value`` or zeros to reach the desired length.
        If the input array has length greater
        than or equal to ``length_max``, it is truncated to the desired length.

    Examples:
        >>> a = np.array([1, 2, 3])
        >>> get_padded_array(a, 5)
        array([1, 2, 3, 0, 0])
        >>> b = [0.5, 1.5, 2.5, 3.5]
        >>> get_padded_array(b, 3, fill_value=-1)
        array([0.5, 1.5, 2.5])
    """
    array = np.asarray(array)
    fill_value = array.dtype.type(fill_value)
    length_array = len(array)
    if length_array < length_max:
        return np.concatenate(
            (
                array[:length_max],
                np.full(
                    shape=(length_max - length_array),
                    dtype=array.dtype,
                    fill_value=fill_value,
                ),
            )
        )
    else:
        return array[:length_max]


@nb.jit(**NUMBA_CONFIG)
def replace(
    array: np.ndarray,
    values_to_replace: np.ndarray,
    replacement_values: np.ndarray,
    inplace: bool = False,
) -> np.ndarray:
    """Replace elements in a 1D NumPy array with specified values.

    Args:
        array: The 1D NumPy array to be modified.
        values_to_replace: The values to be replaced.
        replacement_values: The values that will replace the specified values.
        inplace: A boolean value that determines whether the original array is modified
            or a new one is created. Defaults to False.

    Returns:
        The modified array.
    """
    array = np.asarray(array)
    assert array.ndim == 1, "Only 1D arrays are supported"
    assert len(values_to_replace) == len(replacement_values)

    # Sort for faster search
    sorting_indices = values_to_replace.argsort()
    values_to_replace = values_to_replace[sorting_indices]
    replacement_values = replacement_values[sorting_indices]

    if inplace:
        output = array
    else:
        output = np.zeros_like(array)

    for idx, element in enumerate(array):
        found_idx = operations.find_sorted_array(values_to_replace, element)
        new_element = element if found_idx == -1 else replacement_values[found_idx]
        output[idx] = new_element

    return output
