"""This module defines global variables used for evaluation.
"""
import typing

#: Names of the dataframes used in the evaluation
POSSIBLE_DFNAMES = [
    "particles",  # dataframe of MC particles
    "tracks",  # dataframe of tracks
    "candidates",  # dataframe resulting from matching
]

#: Type for a dataframe name
DFName = typing.Literal["particles", "tracks", "candidates"]

#: Index columns for ``tracks`` dataframe
TRACK_INDICES = ["event_id", "track_id"]
#: Index columns for ``particles`` dataframe
PARTICLE_INDICES = ["event_id", "particle_id"]
#: Index columns for ``candidates`` dataframe
CANDIDATE_INDICES = ["event_id", "track_id", "particle_id"]

#: Associates a dataframe with the list of columns that identify a row
#: in this dataframe
DFNAME_TO_INDICES = {
    "particles": PARTICLE_INDICES,
    "tracks": TRACK_INDICES,
    "candidates": CANDIDATE_INDICES,
}

#: List of the dataframes that can be filtered by a truth-based category
FILTERED_DFNAMES = ["particles", "candidates"]
