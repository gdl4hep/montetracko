"""This package defines specific categories and matching conditions used at LHCb.
"""
from . import category, matchcond
from .matchcond import veloMatchingCondition

__all__ = [
    "category",
    "matchcond",
    "veloMatchingCondition",
]
