"""Define categogies used at LHCb.
"""
from math import pi
from ..requirement import Category

in_acceptance = Category.from_callable(
    name="in_acceptance",
    callable=lambda df: (df["eta"] > 2) & (df["eta"] < 5),
)
no_electrons = Category.from_callable(
    name="no_electrons",
    callable=lambda df: df["pid"].abs() != 11,
)
reconstructible_velo = Category.from_callable(
    name="reconstructible_velo",
    callable=lambda df: df["has_velo"] == True,  # noqa: E712
)
reconstructible_long = Category.from_callable(
    name="reconstructible_long",
    callable=lambda df: (df["has_velo"] == True)  # noqa: E712
    & (df["has_scifi"] == True),  # noqa: E712
)
momentum_greater_5GeV = Category.from_callable(
    name="P>5GeV",
    callable=lambda df: df["p"] > 5e3,
)

from_strange = Category.from_callable(
    name="from_strange",
    callable=lambda df: df["from_sdecay"] == True,  # noqa: E712
)

from_beauty = Category.from_callable(
    name="from_beauty",
    callable=lambda df: df["from_bdecay"] == True,  # noqa: E712
)
long_noelectrons = Category.from_category(
    name="long_no_electrons",
    label="Long, no electrons",
    category=in_acceptance & reconstructible_long & no_electrons,
)

category_from_kshort = Category.from_callable(
    name="kshort",
    label="$K^{0}_{S}$",
    callable=lambda df: df["mother_pid"].abs() == 310,
)


#: List of categories used in the velo validation sequence in Allen
velo_categories = [
    Category.from_category(
        name="velo",
        category=in_acceptance & reconstructible_velo & no_electrons,
    ),
    Category.from_category(
        name="long",
        category=long_noelectrons,
    ),
    Category.from_category(
        name="long_P>5GeV",
        category=long_noelectrons & momentum_greater_5GeV,
    ),
    Category.from_category(
        name="long_strange",
        category=long_noelectrons & no_electrons & from_strange,
    ),
    Category.from_category(
        name="long_strange_P>5GeV",
        category=long_noelectrons & from_strange & momentum_greater_5GeV,
    ),
    Category.from_category(
        name="long_fromB",
        category=long_noelectrons & from_beauty,
    ),
    Category.from_category(
        name="long_fromB_P>5GeV",
        category=long_noelectrons & from_beauty & momentum_greater_5GeV,
    ),
    Category.from_category(
        name="long_electrons",
        category=in_acceptance & reconstructible_long & (~no_electrons),
    ),
    Category.from_category(
        name="long_fromB_electrons",
        category=in_acceptance & reconstructible_long & (~no_electrons) & from_beauty,
    ),
    Category.from_category(
        name="long_fromB_electrons_P>5GeV",
        category=in_acceptance
        & reconstructible_long
        & from_beauty
        & (~no_electrons)
        & momentum_greater_5GeV,
    ),
]

extended_velo_categories = [
    Category.from_category(
        name="velo_no_electrons",
        category=in_acceptance & reconstructible_velo & no_electrons,
    ),
    Category.from_category(
        name="long_no_electrons",
        category=long_noelectrons,
    ),
    Category.from_category(
        name="long_P>5GeV_no_electrons",
        category=long_noelectrons & momentum_greater_5GeV,
    ),
    Category.from_category(
        name="long_from_strange",
        category=long_noelectrons & no_electrons & from_strange,
    ),
    Category.from_category(
        name="long_from_strange_P>5GeV",
        category=long_noelectrons & from_strange & momentum_greater_5GeV,
    ),
    Category.from_category(
        name="long_fromB_no_electrons",
        category=long_noelectrons & from_beauty,
    ),
    Category.from_category(
        name="long_fromB_P>5GeV_no_electrons",
        category=long_noelectrons & from_beauty & momentum_greater_5GeV,
    ),
    Category.from_category(
        name="long_electrons",
        category=in_acceptance & reconstructible_long & (~no_electrons),
    ),
    Category.from_category(
        name="long_fromB_electrons",
        category=in_acceptance & reconstructible_long & (~no_electrons) & from_beauty,
    ),
    Category.from_category(
        name="long_fromB_electrons_P>5GeV",
        category=in_acceptance
        & reconstructible_long
        & from_beauty
        & (~no_electrons)
        & momentum_greater_5GeV,
    ),
    Category.from_category(
        name="only_velo_electrons",
        category=in_acceptance
        & reconstructible_velo
        & (~reconstructible_long)
        & (~no_electrons),
    ),
    Category.from_category(
        name="velo_fromK0s",
        category=in_acceptance & reconstructible_velo & category_from_kshort,
    ),
    Category.from_category(
        name="long_fromK0s",
        category=in_acceptance & reconstructible_long & category_from_kshort,
    ),
]


reconstructible_novelo_ut_scifi = Category.from_callable(
    name="UT+SciFi",
    callable=lambda df: (~df["has_velo"]) & df["has_ut"] & df["has_scifi"],
)

p_sup_3gev = Category.from_callable(
    name="P>3GeV",
    callable=lambda df: df["p"] > 3.0e3,
)
p_sup_5gev = Category.from_callable(
    name="P>5GeV",
    callable=lambda df: df["p"] > 5.0e3,
)
pt_sup_0_5 = Category.from_callable(
    name="Pt>0.5",
    callable=lambda df: df["pt"] > 500.0,
)
pt_sup_2gev = Category.from_callable(
    name="Pt>2GeV",
    callable=lambda df: df["pt"] > 2000.0,
)


seeding_categories = [
    Category.from_category(
        name="00_P>3Gev_Pt>0.5",
        category=(
            in_acceptance
            & reconstructible_long
            & no_electrons
            & p_sup_3gev
            & pt_sup_0_5
        ),
    ),
    Category.from_category(
        name="01_long",
        category=long_noelectrons,
    ),
    Category.from_callable(
        name="---1. phi quadrant",
        callable=lambda df: (
            long_noelectrons.mask(df) & (df["phi"] > 0) & (df["phi"] < pi / 2)
        ),
    ),
    Category.from_callable(
        name="---2. phi quadrant",
        callable=lambda df: (
            long_noelectrons.mask(df) & (df["phi"] > pi / 2) & (df["phi"] < pi)
        ),
    ),
    Category.from_callable(
        name="---3. phi quadrant",
        callable=lambda df: (
            long_noelectrons.mask(df) & (df["phi"] > -pi) & (df["phi"] < -pi / 2)
        ),
    ),
    Category.from_callable(
        name="---4. phi quadrant",
        callable=lambda df: (
            long_noelectrons.mask(df) & (df["phi"] > -pi / 2) & (df["phi"] < 0)
        ),
    ),
    Category.from_callable(
        name="---eta < 2.5, small x, large y (long) ",
        label="---eta < 2.5, small x, large y ",
        callable=lambda df: (
            long_noelectrons.mask(df)
            & (df["eta"] < 2.5)
            & (
                ((df["phi"] > pi / 3) & (df["phi"] < 2 * pi / 3))
                | ((df["phi"] > -2 * pi / 3) & (df["phi"] < -pi / 3))
            )
        ),
    ),
    Category.from_callable(
        name="---eta < 2.5, large x, small y (long) ",
        label="---eta < 2.5, large x, small y ",
        callable=lambda df: (
            long_noelectrons.mask(df)
            & (df["eta"] < 2.5)
            & (
                (df["phi"] > 2 * pi / 3)
                | (df["phi"] < -2 * pi / 3)
                | ((df["phi"] > -pi / 3) & (df["phi"] < pi / 3))
            )
        ),
    ),
    Category.from_callable(
        name="---eta > 2.5, small x, large y (long) ",
        label="---eta > 2.5, small x, large y ",
        callable=lambda df: (
            long_noelectrons.mask(df)
            & (df["eta"] > 2.5)
            & (
                ((df["phi"] > pi / 3) & (df["phi"] < 2 * pi / 3))
                | ((df["phi"] > -2 * pi / 3) & (df["phi"] < -pi / 3))
            )
        ),
    ),
    Category.from_callable(
        name="---eta > 2.5, large x, small y (long) ",
        label="---eta > 2.5, large x, small y ",
        callable=lambda df: (
            long_noelectrons.mask(df)
            & (df["eta"] > 2.5)
            & (
                (df["phi"] > 2 * pi / 3)
                | (df["phi"] < -2 * pi / 3)
                | ((df["phi"] > -pi / 3) & (df["phi"] < pi / 3))
            )
        ),
    ),
    Category.from_category(
        name="02_long_P>5GeV",
        category=long_noelectrons & p_sup_5gev,
    ),
    Category.from_callable(
        name="02_long_P>5GeV, eta > 4",
        callable=lambda df: (
            long_noelectrons.mask(df) & p_sup_5gev.mask(df) & (df["eta"] > 4)
        ),
    ),
    Category.from_callable(
        name="---eta < 2.5, small x, large y ",
        callable=lambda df: (
            long_noelectrons.mask(df)
            & p_sup_5gev.mask(df)
            & (df["eta"] < 2.5)
            & (
                ((df["phi"] > pi / 3) & (df["phi"] < 2 * pi / 3))
                | ((df["phi"] > -2 * pi / 3) & (df["phi"] < -pi / 3))
            )
        ),
    ),
    Category.from_callable(
        name="---eta < 2.5, large x, small y ",
        callable=lambda df: (
            long_noelectrons.mask(df)
            & p_sup_5gev.mask(df)
            & (df["eta"] < 2.5)
            & (
                (df["phi"] > 2 * pi / 3)
                | (df["phi"] < -2 * pi / 3)
                | ((df["phi"] > -pi / 3) & (df["phi"] < pi / 3))
            )
        ),
    ),
    Category.from_callable(
        name="---eta > 2.5, small x, large y ",
        callable=lambda df: (
            long_noelectrons.mask(df)
            & p_sup_5gev.mask(df)
            & (df["eta"] > 2.5)
            & (
                ((df["phi"] > pi / 3) & (df["phi"] < 2 * pi / 3))
                | ((df["phi"] > -2 * pi / 3) & (df["phi"] < -pi / 3))
            )
        ),
    ),
    Category.from_callable(
        name="---eta > 2.5, large x, small y ",
        callable=lambda df: (
            long_noelectrons.mask(df)
            & p_sup_5gev.mask(df)
            & (df["eta"] > 2.5)
            & (
                (df["phi"] > 2 * pi / 3)
                | (df["phi"] < -2 * pi / 3)
                | ((df["phi"] > -pi / 3) & (df["phi"] < pi / 3))
            )
        ),
    ),
    Category.from_category(
        name="03_long_P>3GeV",
        category=long_noelectrons & p_sup_3gev,
    ),
    Category.from_callable(
        name="04_long_P>0.5GeV",
        callable=lambda df: (long_noelectrons.mask(df) & (df["p"] > 5e2)),
    ),
    Category.from_category(
        name="05_long_from_B",
        category=long_noelectrons & from_beauty,
    ),
    Category.from_category(
        name="06_long_from_B_P>5GeV",
        category=long_noelectrons & from_beauty & p_sup_5gev,
    ),
    Category.from_category(
        name="07_long_from_B_P>3GeV",
        category=long_noelectrons & from_beauty & p_sup_3gev,
    ),
    Category.from_category(
        name="08_UT+SciFi",
        category=in_acceptance & reconstructible_novelo_ut_scifi & no_electrons,
    ),
    Category.from_category(
        name="09_UT+SciFi_P>5GeV",
        category=(
            in_acceptance & reconstructible_novelo_ut_scifi & no_electrons & p_sup_5gev
        ),
    ),
    Category.from_category(
        name="10_UT+SciFi_P>3GeV",
        category=(
            in_acceptance & reconstructible_novelo_ut_scifi & no_electrons & p_sup_3gev
        ),
    ),
    Category.from_category(
        name="11_UT+SciFi_fromStrange",
        category=(
            in_acceptance
            & reconstructible_novelo_ut_scifi
            & no_electrons
            & from_strange
        ),
    ),
    Category.from_category(
        name="12_UT+SciFi_fromStrange_P>5GeV",
        category=(
            in_acceptance
            & reconstructible_novelo_ut_scifi
            & no_electrons
            & from_strange
            & p_sup_5gev
        ),
    ),
    Category.from_category(
        name="13_UT+SciFi_fromStrange_P>3GeV",
        category=(
            in_acceptance
            & reconstructible_novelo_ut_scifi
            & no_electrons
            & from_strange
            & p_sup_3gev
        ),
    ),
    Category.from_category(
        name="14_long_electrons",
        category=in_acceptance & reconstructible_long & in_acceptance & (~no_electrons),
    ),
    Category.from_category(
        name="15_long_electrons_P>5GeV",
        category=(
            in_acceptance
            & reconstructible_long
            & (~no_electrons)
            & p_sup_3gev
            & (~no_electrons)
            & p_sup_5gev
        ),
    ),
    Category.from_category(
        name="16_long_electrons_P>3GeV",
        category=in_acceptance & reconstructible_long & (~no_electrons) & p_sup_3gev,
    ),
    Category.from_category(
        name="17_long_fromB_electrons",
        category=in_acceptance & reconstructible_long & from_beauty & (~no_electrons),
    ),
    Category.from_category(
        name="18_long_fromB_electrons_P>5GeV",
        category=(
            in_acceptance
            & reconstructible_long
            & from_beauty
            & (~no_electrons)
            & p_sup_5gev
        ),
    ),
    Category.from_category(
        name="19_long_PT>2GeV",
        category=in_acceptance & reconstructible_long & no_electrons & pt_sup_2gev,
    ),
    Category.from_category(
        name="20_long_from_B_PT>2GeV",
        category=(
            in_acceptance
            & reconstructible_long
            & no_electrons
            & from_beauty
            & pt_sup_2gev
        ),
    ),
    Category.from_category(
        name="21_long_strange_P>5GeV",
        category=(
            in_acceptance
            & reconstructible_long
            & no_electrons
            & from_strange
            & p_sup_5gev
        ),
    ),
    Category.from_category(
        name="22_long_strange_P>5GeV_PT>500MeV",
        category=(
            in_acceptance
            & reconstructible_long
            & no_electrons
            & from_strange
            & p_sup_5gev
            & pt_sup_0_5
        ),
    ),
]

detector_to_categories = {
    "velo": velo_categories,
    "scifi": seeding_categories,
    "scifi_xz": seeding_categories,
}
detector_to_extended_categories = {
    "velo": extended_velo_categories,
    "scifi": seeding_categories,
    "scifi_xz": seeding_categories,
}


category_velo = Category.from_category(
    name="velo_reconstructible_acceptance",
    label="Velo",
    category=in_acceptance & reconstructible_velo,
)
category_long = Category.from_category(
    name="long_reconstructible_acceptance",
    label="Long",
    category=in_acceptance & reconstructible_long,
)

category_velo_no_electrons = Category.from_category(
    name="velo_reconstructible_acceptance_no_electrons",
    label="Velo, no electrons",
    category=in_acceptance & reconstructible_velo & no_electrons,
)
category_only_velo_no_electrons = Category.from_category(
    name="velo_reconstructible_acceptance_only_no_electrons",
    label="Velo only, no electrons",
    category=in_acceptance
    & reconstructible_velo
    & (~reconstructible_long)
    & (no_electrons),
)
category_only_velo_only_electrons = Category.from_category(
    name="velo_reconstructible_acceptance_only_electrons",
    label="Velo only, only electrons",
    category=in_acceptance
    & reconstructible_velo
    & (~reconstructible_long)
    & (~no_electrons),
)
category_long_no_electrons = Category.from_category(
    name="long_reconstructible_acceptance_no_electrons",
    label="Long, no electrons",
    category=in_acceptance & reconstructible_long & no_electrons,
)
category_long_only_electrons = Category.from_category(
    name="long_only_electrons",
    label="Long, only electrons",
    category=in_acceptance & reconstructible_long & (~no_electrons),
)

category_only_velo_strange = Category.from_category(
    name="only_velo_from_strange",
    label="Velo only, from strange",
    category=category_velo & (~category_long) & no_electrons & from_strange,
)

category_long_strange = Category.from_category(
    name="long_from_strange",
    label="Long, from strange",
    category=long_noelectrons & no_electrons & from_strange,
)

category_only_velo_kshort = Category.from_category(
    name="velo_from_kshort",
    label="Velo only, from $K^{0}_{S}$",
    category=category_from_kshort & category_velo & (~category_long),
)

category_only_velo_strange = Category.from_category(
    name="velo_only_from_strange",
    label="Velo only, from strange",
    category=from_strange & category_velo & (~category_long),
)

category_long_kshort = Category.from_category(
    name="long_from_kshort",
    label="Long, from $K^{0}_{S}$",
    category=category_from_kshort & category_long,
)


category_scifi = Category.from_callable(
    name="scifi",
    label="SciFi",
    callable=lambda df: df["has_scifi"],
)

category_scifi = Category.from_category(
    name="scifi_only", label="SciFi-only", category=category_scifi & (~category_velo)
)

#: List of relevant categories to test velo tracking algorithms
velo_categories = [
    category_velo,
    category_long,
    category_velo_no_electrons,
    category_only_velo_no_electrons,
    category_only_velo_only_electrons,
    category_long_only_electrons,
    category_only_velo_strange,
    category_long_strange,
    category_only_velo_kshort,
    category_long_kshort,
]

category_long_high_p = Category.from_category(
    name="long_P>3GeV",
    label="Long, $p > 3$ GeV",
    category=long_noelectrons & p_sup_3gev,
)

scifi_categories = [
    category_long,
    category_long_high_p,
    category_long_only_electrons,
    # category_long_fromB,
    category_long_kshort,
    category_scifi,
]

detector_to_table_categories = {
    "velo": velo_categories,
    "scifi": scifi_categories,
    "scifi_xz": scifi_categories,
}
