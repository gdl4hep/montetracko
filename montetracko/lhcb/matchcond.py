"""Define matching conditions at LHCb.
"""
from .. import matchcond

#: Matching condition for the Velo only
veloMatchingCondition = matchcond.MinMatchingFraction(0.7) | (
    ~matchcond.MinLengthTrack(3)
)
