"""Implement basic general reporters.
"""
from __future__ import annotations
import typing
import pandas as pd
from .base import ReporterBase
from ..requirement import Category
from .. import utils
from ..metrics import metricsLibrary
from ..array_utils.dataframes import (
    get_table_representation,
    POSSIBLE_TABLE_MODES,
    TableMode,
)


class AllenReporter(ReporterBase):
    """Reporter that presents the metric values in the same format as the validation
    sequences of Allen.
    """

    def __init__(self, auto_numbering: bool = True, auto_remove: bool = True):
        # Metrics that are needed to make the report
        metric_names = [
            "n_particles",  # number of MC particles
            "n_matched_particles",
            "n_clones",
            "clone_rate",
            "efficiency",
            "efficiency_per_event",
            "hit_purity_per_candidate",
            "hit_efficiency_per_candidate",
        ]
        global_metric_names = [
            "n_ghosts",
            "n_tracks",
            "ghost_rate",
        ]
        super().__init__(
            metric_names=metric_names,
            global_metric_names=global_metric_names,
        )
        self.auto_numbering = bool(auto_numbering)
        self.auto_remove = bool(auto_remove)

    def _report(
        self,
        categories: typing.List[Category],
        nested_metric_values: typing.Dict[str | None, typing.Dict[str, int | float]],
    ):
        """Computes the report string given a list of categories
        and their metric values.

        Args:
            categories: A list of Category objects used to compute the metrics
                in ``nested_metric_values``
            nested_metric_values: A dictionary of nested metrics,
                where the top-level keys are category names,
                and the values are dictionaries of metric names
                and their associated values.

         Returns:
            The report string in the Allen format.
        """
        report = ""
        mv = nested_metric_values[None]
        report += f"{'TrackChecker output':<50s}: "
        report += f"{mv['n_ghosts']:>9d}/{mv['n_tracks']:>9d} "
        report += f"{mv['ghost_rate'] * 100:>6.2f}% ghosts"
        report += "\n"

        num_digits = len(str(len(categories)))
        for cat_idx, cat in enumerate(categories):
            if (
                not self.auto_remove
                or nested_metric_values[cat.name]["n_particles"] > 0
            ):
                mv = nested_metric_values[cat.name]
                category_name = cat.label if cat.label is not None else cat.name

                if self.auto_numbering:
                    cat_header = f"{cat_idx+1:0{num_digits}}_{category_name}"
                else:
                    cat_header = category_name
                # Category name
                report += f"{cat_header:<50}: "
                # Efficiency
                report += f"{mv['n_matched_particles']: >9}/{mv['n_particles']: >9} "
                report += f"{mv['efficiency']*100: >6.2f}% "
                report += f"({mv['efficiency_per_event']*100: >6.2f}%), "
                # Clones
                report += (
                    f"{mv['n_clones']: >9} ({mv['clone_rate']*100: >6.2f}%) clones, "
                )
                # Hit purity and efficiency
                report += f"pur {mv['hit_purity_per_candidate']*100: >6.2f}%, "
                report += f"hit eff {mv['hit_efficiency_per_candidate']*100: >6.2f}%"
                report += "\n"

        return report


class TabReporter(ReporterBase):
    """Reporter that presents the results in a table that can be customised.

    Each row of the table is a category, and each column is a metric value.

    Attributes:
        metric_names: name of the metrics to compute
        mode: The format of the table. Valid options are
            "markdown", "string", "latex", "html", "csv", and "pandas".
            Default is "string".

    Raise:
        ValueError: if ``mode`` is not a valid format mode
    """

    def __init__(
        self,
        metric_names: typing.List[str],
        mode: typing.Optional[TableMode] = None,
        **kwargs,
    ):
        super().__init__(
            metric_names=metric_names,
        )
        self.kwargs = kwargs
        if mode is None:
            mode = "string"
        utils.formatting._assert_var_in_list(mode, POSSIBLE_TABLE_MODES, "mode")
        self.mode: TableMode = mode

    def _report(
        self,
        categories: typing.List[Category | None] | typing.List[Category],
        nested_metric_values: typing.Dict[str | None, typing.Dict[str, int | float]],
    ):
        """Procue the report string.

        Args:
            categories: A list of Category objects used to compute the metrics
                in ``nested_metric_values``
            nested_metric_values: A dictionary of nested metrics,
                where the top-level keys are category names,
                and the values are dictionaries of metric names
                and their associated values.

         Returns:
            The report string in the Allen format.
        """
        category_labels = []
        for category in categories:
            if category is None:
                category_labels.append("Everything")
            else:
                if category.label is None:
                    category_labels.append(category.name)
                else:
                    category_labels.append(category.label)

        metric_values = {}
        category_names = [
            None if category is None else category.name for category in categories
        ]
        for metric_name in self.metric_names:
            metric_label = metricsLibrary.label(metric_name)
            metric_values[metric_label] = [
                metricsLibrary.value_to_str(
                    value=nested_metric_values[category_name][metric_name],
                    metric_name=metric_name,
                )
                for category_name in category_names
            ]

        table = pd.DataFrame(
            metric_values, index=pd.Index(category_labels, name="Categories")
        )
        return get_table_representation(table=table, mode=self.mode, **self.kwargs)
