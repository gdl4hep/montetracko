"""Module that implements the base class of reporters.
"""
from __future__ import annotations
import typing
import numpy as np
import numpy.typing as npt
from .. import utils
from ..requirement import Category


def _check_nested_metric_values(
    nested_metric_values,
    metric_names: typing.List[str],
    category_name: typing.Optional[str] = None,
):
    """Helper function to validate the values of nested metrics.

    Args:
        nested_metric_values: A dictionary of nested metrics,
            where the top-level keys are category names,
            and the values are dictionaries of metric names
            and their associated values.
        metric_names: A list of metric names to check for.
        category_name: string indicating the category to check.

    Raises:
        ValueError: If the specified category cannot be found
            in ``nested_metric_values``.
        TypeError: If a value associated with a metric name is not a number.
    """
    if category_name not in nested_metric_values:
        raise ValueError(
            f"Category {category_name} was provided as one of the categories, "
            "but cannot be found in `nested_metric_values`"
        )
    else:
        metric_values = nested_metric_values[category_name]
        if not isinstance(metric_values, dict):
            raise TypeError(
                f"The value associated with the key `{category_name}` should be "
                "a dictionnary"
            )
        for metric_name in metric_names:
            if metric_name not in metric_values:
                raise ValueError(
                    f"Metric `{metric_name}` not found in `nested_metric_values` "
                    f"for category `{category_name}`"
                )
            elif not isinstance(
                metric_values[metric_name], (int, np.integer, float, np.floating)
            ):
                raise TypeError(f"Metric `{metric_name}` value should be a number")


class ReporterBase:
    """Base class of reporters.

    A reporter allows to present the metric values in a readable format.

    Attributes:
        metric_names: list of metric names to compute in different categories
        global_metric_names: list of metric names to compute one time in the overall
            unfiltered dataframes
    """

    def __init__(
        self,
        metric_names: typing.Optional[typing.List[str]] = None,
        global_metric_names: typing.Optional[typing.List[str]] = None,
    ):
        """Initializes a new instance of the ReporterBase class."""
        if metric_names is None:
            metric_names = []
        if global_metric_names is None:
            global_metric_names = []

        utils.formatting._assert_list(metric_names, str, "metrics")
        utils.formatting._assert_list(global_metric_names, str, "global_metric_names")

        self.metric_names = metric_names
        self.global_metric_names = global_metric_names

    def _report(
        self,
        categories: typing.List[Category | None] | typing.List[Category],
        nested_metric_values: typing.Dict[
            str | None, typing.Dict[str, int | float | npt.ArrayLike]
        ],
    ) -> str:
        """Computes the report string given a list of categories
        and their metric values.

        Args:
            categories: A list of Category objects used to compute the metrics
                in ``nested_metric_values``
            nested_metric_values: A dictionary of nested metrics,
                where the top-level keys are category names,
                and the values are dictionaries of metric names
                and their associated values.

         Returns:
            The report string.

        Raises:
            NotImplementedError: If the method is not implemented in a subclass.
        """
        raise NotImplementedError

    def report(
        self,
        categories: typing.Optional[
            typing.List[Category | None] | typing.List[Category]
        ],
        nested_metric_values: typing.Dict[
            str | None, typing.Dict[str, int | float | npt.ArrayLike]
        ],
    ) -> str:
        """Computes and returns the report string given a list of categories and their
        metric values.

        Args:
            categories: A list of Category objects. If None, the report is computed for
                the overall dataframe. Defaults to None.
            nested_metric_values: A dictionary of nested metrics,
                where the top-level keys are category names,
                and the values are dictionaries of metric names
                and their associated values.

        Returns:
            The report string.

        Raises:
            TypeError: If a category in `categories` is not a Category object or None.
        """
        # Categories
        if categories is None:
            categories = [None]

        elif isinstance(categories, Category):
            categories = [categories]

        # Check list of categories
        for category in categories:
            if category is not None and not isinstance(category, Category):
                raise TypeError(
                    "A category (an element of `categories`) should be "
                    "a `Category` instance, or None."
                )

        # Check nested metric values
        for category in categories:
            _check_nested_metric_values(
                nested_metric_values,
                self.metric_names,
                None if category is None else category.name,
            )

        if self.global_metric_names:
            _check_nested_metric_values(
                nested_metric_values,
                self.global_metric_names,
            )
        return self._report(categories, nested_metric_values=nested_metric_values)

    def print_report(self, *args, **kwargs):
        """Prints the report

        Args:
            *args: passed to :py:func:`ReporterBase.report`.
            **kwargs: to py:func:`ReporterBase.report`.
        """
        return print(self.report(*args, **kwargs))
