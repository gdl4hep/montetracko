"""Module that implements :py:class:`CombinedMetric`, the combination of two metrics.
"""
from __future__ import annotations
import typing
import os.path as op
import numpy as np
import pandas as pd

from .base import MetricBase
from .composite import CompositeMetric, _assert_type_for_composite_metric
from ..config import ConfigParams
from .. import utils

#: Associates a parameter with its name
OPERATOR_TO_NAME = {
    "+": "add",
    "-": "sub",
    "*": "mul",
    "/": "truediv",
    "//": "floordiv",
    "%": "mod",
    "**": "pow",
}

#: Associates an operator with its description
OPERATOR_TO_DESCRIPTION = {
    "+": "plus",
    "-": "minus",
    "*": "multiplied by",
    "/": "divided by",
    "//": "integer-divided by",
    "%": "modulo",
    "**": "to the power of",
}

#: Associates an operator with its label
OPERATOR_TO_LABEL = {
    "+": "+",
    "-": "-",
    "*": "x",
    "/": "/",
    "//": "//",
    "%": "%",
    "**": "^",
}


def _combine_str_if_not_none(
    string: str | None,
    string1: str | None,
    string2: str | None,
    delimiter: str,
) -> str:
    """Combine two strings with a delimiter if neither of them is None.

    Args:
        string: A string or ``None``
        string1: A string or ``None``
        string2: A string or ``None``
        delimiter: A string that serves as a separator between two other strings.

    Returns:
        The combination of ``string1`` and ``string2`` with ``delimiter`` in between
        if neither of them is ``None``. Otherwise,  ``string`` is returned.

    Examples:
        >>> _combine_str_if_not_none(None, "hello", "world", " ")
        'hello world'
        >>> _combine_str_if_not_none("goodbye", "world", "!", " ")
        'goodbye'
        >>> _combine_str_if_not_none(None, "hello", None, " ")
        >>> _combine_str_if_not_none(None, None, None, " ")
        >>> _combine_str_if_not_none("hello", "world", "goodbye", ", ")
        'hello'
    """
    if string is None:
        if string1 is None or string2 is None:
            raise ValueError(
                "Could not create a name for the metric because the input metrics "
                "are None."
            )
        else:
            return f"{string1}{delimiter}{string2}"
    else:
        return string


def _retrieve_input_values_for_combined_metrics(inputs, metric_values):
    """Get the input values of the metrics from the dictionary of metric values."""
    input_values = []
    for input in inputs:
        if isinstance(input, MetricBase):
            if input.name not in metric_values:
                raise ValueError(f"{input.name} was not provided.")
            metric_input = metric_values[input.name]
            input_values.append(metric_input)
        else:
            # `metric` is int or float
            assert isinstance(input, (int, float, np.integer, np.floating))
            input_values.append(input)

    return input_values


class CombinedMetric(CompositeMetric):
    """A metric computed from two input metrics using a mathematical operator.

    Args:
        input1: The first input metric or value.
        input2: The second input metric or value.
        operator: The mathematical operator to use to combine the input metrics.
        name: The name of the combined metric. If not provided,
            a default name is generated.
        merging: A string indicating how to combine pandas series representing
            the metric values.
            Must be either ``union``, ``intersection`` or ``None``. None means
            that the index of the pandas Series of two different metrics must
            be the exact same. Defaults to None.
        description : A description of the combined metric. If not provided,
            a default description is generated.
        label: A label for the combined metric. If not provided,
            a default label is generated.
        format_specifier: A format specifier for the combined metric.
        config: Configuration parameters
    Raises:
        ValueError: If the provided operator is not one of the supported operators.

    Attributes:
        operator: The mathematical operator used to combine the input metrics.
    """

    def __init__(
        self,
        input1: MetricBase | int | float,
        input2: MetricBase | int | float,
        operator: typing.Literal["+", "-", "*", "/", "//", "%", "**"],
        name: typing.Optional[str] = None,
        merging: typing.Optional[typing.Literal["union", "intersection"]] = None,
        description: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = None,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        inputs = [input1, input2]
        utils.formatting._assert_var_in_list(
            operator, ["+", "-", "*", "/", "//", "%", "**"], "operator"
        )
        # Assert that the types are correct
        for input in inputs:
            _assert_type_for_composite_metric(input)

        # `name`, `short`, `description` and `label`
        name = _combine_str_if_not_none(
            name,
            *(
                input.name if isinstance(input, MetricBase) else str(input)
                for input in inputs
            ),
            delimiter=f"_{OPERATOR_TO_NAME[operator]}_",
        )
        description = _combine_str_if_not_none(
            description,
            *(
                input.description if isinstance(input, MetricBase) else str(input)
                for input in inputs
            ),
            delimiter=f" {OPERATOR_TO_DESCRIPTION[operator]} ",
        )
        label = _combine_str_if_not_none(
            label,
            *(
                input.label if isinstance(input, MetricBase) else str(input)
                for input in inputs
            ),
            delimiter=f" {OPERATOR_TO_LABEL[operator]} ",
        )
        super().__init__(
            inputs=inputs,
            name=name,
            merging=merging,
            description=description,
            label=label,
            format_specifier=format_specifier,
            config=config,
        )
        self.operator = operator

    def __repr__(self):
        """String representation of a :py:class:`CombinedMetric`"""
        return f"{repr(self.inputs[0])} {self.operator} {self.inputs[1]}"

    def _compute(self, **metric_values):
        """Computes the value of the combined metric by applying the operator
        on the input values.

        Args:
            **metric_values: A dictionary mapping the name of a metric
                to its computed value.

        Returns:
            The computed value of the combined metric.

        Raises:
            ValueError: If a value for an input metric is missing in ``metric_values``.
            ValueError: If the operator is not recognized.
        """
        input_values = _retrieve_input_values_for_combined_metrics(
            inputs=self.inputs, metric_values=metric_values
        )
        input1 = input_values[0]
        input2 = input_values[1]

        if self.operator == "+":
            return input1 + input2
        elif self.operator == "-":
            return input1 - input2
        elif self.operator == "*":
            return input1 * input2
        elif self.operator == "/":
            if (
                isinstance(input2, (int, np.integer, float, np.floating))
                and input2 == 0
            ):
                return np.nan
            else:
                return input1 / input2
        elif self.operator == "//":
            return input1 // input2
        elif self.operator == "%":
            return input1 % input2
        elif self.operator == "**":
            return input1**input2
        else:
            raise ValueError("Operator " + str(self.operator) + " is not recognised")

    @property
    def formula(self):
        """A string representation of the formula used to combine the two metrics."""
        input_labels = []
        for input in self.inputs:
            if isinstance(input, MetricBase):
                input_labels.append(
                    input.label if input.label is not None else input.name
                )
            else:
                input_labels.append(str(input))

        operator_label = OPERATOR_TO_LABEL[self.operator]

        return f"({input_labels[0]}) {operator_label} ({input_labels[1]})"


class RatioMetric(CombinedMetric):
    """A metric defined as the ratio of 2 other metrics.
    This metric allows the computation of uncertainty using the Bayesian method
    with uniform prior.

    Args:
        input1: The first input metric.
        input2: The second input metric.
        name: The name of the combined metric. If not provided,
            a default name is generated.
        merging: A string indicating how to combine pandas series representing
            the metric values.
            Must be either ``union``, ``intersection`` or ``None``. None means
            that the index of the pandas Series of two different metrics must
            be the exact same. Defaults to None.
        description : A description of the combined metric. If not provided,
            a default description is generated.
        label: A label for the combined metric. If not provided,
            a default label is generated.
        format_specifier: A format specifier for the combined metric.
        config: Configuration parameters
    """

    def __init__(
        self,
        input1: MetricBase,
        input2: MetricBase,
        name: typing.Optional[str] = None,
        merging: typing.Optional[typing.Literal["union", "intersection"]] = None,
        description: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = None,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        super(RatioMetric, self).__init__(
            input1=input1,
            input2=input2,
            operator="/",
            name=name,
            merging=merging,
            description=description,
            label=label,
            format_specifier=format_specifier,
            config=config,
        )

    def compute(
        self,
        dataframes: typing.Dict[str, pd.DataFrame] | None = None,
        groupby: typing.List[str] | str | None = None,
        average: str | typing.List[str] | None = None,
        err: str | None = None,
        **kwargs,
    ):
        """Compute the ratio metric, defined as the ratio of two metrics.

        Args:
            dataframes: A dictionary of pandas  dataframes containing the data
                needed to compute the metric. Defaults to ``None``.
            groupby: Optional. Either a single string or a list of strings. The names
                of the columns to group the results by. Default is None.
            average: Optional. Either a single string or a list of strings. The names
                of the columns to average over. Default is None.
            err: Error to compute. ``sym`` (standard error) or ``bayesian``
                (bayesian with a uniform prior).
                and the standard error is computed. If ``err`` is set to ``auto``,
                the error is set to ``bayesian``.
                If ``None``, no error is computed.
            **kwargs: keyword arguments passed to the underlying ``_compute`` method.

        Returns:
            Metric value.
        """
        if err == "auto":
            err = "bayesian"
        bayesian_err = err == "bayesian"
        err_ = None if bayesian_err else err
        if bayesian_err and average is not None:
            raise ValueError("Cannot average a metric computed with bayesian error.")
        return super().compute(
            dataframes=dataframes,
            groupby=groupby,
            average=average,
            err=err_,
            bayesian_err=bayesian_err,
            **kwargs,
        )

    def _compute(self, bayesian_err: bool = False, **metric_values):
        if bayesian_err:
            import ROOT as pyroot

            pyroot.gInterpreter.ProcessLine(  # type: ignore
                f'#include "{op.join(op.dirname(__file__), "errcomp.h")}"'
            )
            input_values = _retrieve_input_values_for_combined_metrics(
                inputs=self.inputs, metric_values=metric_values
            )
            input1 = input_values[0]
            input2 = input_values[1]

            if isinstance(input1, pd.Series):
                assert isinstance(input2, pd.Series)
                index = input1.index
                assert (index == input2.index).all()
            else:
                index = None

            input1 = np.asarray(input1)
            input2 = np.asarray(input2)

            shape1 = input1.shape
            shape2 = input2.shape
            assert shape1 == shape2

            input1 = np.atleast_1d(input1).astype(np.double)
            input2 = np.atleast_1d(input2).astype(np.double)

            mean = pyroot.std.vector("double")(np.zeros_like(input1))  # type: ignore
            err_low = pyroot.std.vector("double")(np.zeros_like(input1))  # type: ignore
            err_up = pyroot.std.vector("double")(np.zeros_like(input1))  # type: ignore

            pyroot.compute_efficiency_uncertainty(input1, input2, mean, err_low, err_up)  # type: ignore
            mean = np.array(mean)
            err_low = np.array(err_low)
            err_up = np.array(err_up)

            if not shape1:
                return {"mean": mean[0], "err_neg": err_low[0], "err_pos": err_up[0]}
            else:
                assert index is not None
                return pd.DataFrame(
                    data={"mean": mean, "err_neg": err_low, "err_pos": err_up},
                    index=index,
                )
        else:
            return super()._compute(**metric_values)
