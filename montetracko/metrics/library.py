"""This module define the :py:class:`MetricsLibrary`, a collection of metrics
that can be used and called from their names.
"""
from __future__ import annotations
import typing
import numpy as np
import pandas as pd
from .base import MetricBase, MetricOutput
from .custom import CountDistinctMetric, SumCountMetric
from .combine import CombinedMetric, RatioMetric
from .aggregated import AverageMetric
from .. import defs
from ..config import ConfigurableClass, ConfigParams
from .. import utils
from ..array_utils.dataframes import get_table_representation, TableMode


class MetricsLibrary(ConfigurableClass):
    """A collection of metrics that can be used and called from their names.

    Args:
        metrics: An optional list of metrics to be added to the library.
        config: configuration parameters
    """

    def __init__(
        self,
        metrics: typing.Optional[typing.List[MetricBase]] = None,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        super().__init__(config=config)
        self._metrics = {}

        if metrics is not None:
            self.add_metrics(metrics=metrics)

    @property
    def metrics(self) -> typing.Dict[str, MetricBase]:
        """Dictionnary that associates a metric name with the Metric instance."""
        return self._metrics

    def set_config(self, config: ConfigParams | dict | None):
        """Set the configuration of the metric and propagate the configuration to
        all the metrics.
        """
        super(MetricsLibrary, self).set_config(config)
        for metric in self._metrics.values():
            if isinstance(metric, MetricBase):
                metric.config = self.config

    def is_registered_metric(self, name: str) -> bool:
        """Check whether a metric is registered.

        Args:
            name: metric name

        Returns:
            whether the metric is registered
        """
        return name in self._metrics

    def assert_is_registered_metric(self, name: str):
        """Raise an error if the metric is not registered

        Args:
            name: metric name

        Raises:
            ValueError: if the metric is not found.
        """
        if not self.is_registered_metric(name):
            raise ValueError(f"{name} metric not found")

    def metric(self, name: str) -> MetricBase:
        """Get a metric from its name.

        Args:
            name: metric name

        Returns:
            Metric instance

        Raises:
            ValueError: if the metric is not found.
        """
        self.assert_is_registered_metric(name)
        return self._metrics[name]

    @property
    def metric_names(self) -> typing.List[str]:
        """Get a list of the names of the registered metrics."""
        return list(self._metrics.keys())

    def label(self, name: str):
        """Get a metric label from its name.

        Args:
            name: metric name

        Returns:
            Label of the metric

        Raises:
            ValueError: if the metric is not found.
        """
        metric = self.metric(name)
        if isinstance(metric, MetricBase):
            return name if metric.label is None else metric.label
        else:
            raise TypeError("Type of the metric is not recognised.")

    def description(self, name: str):
        """Get a metric description from its name.

        Args:
            name: metric name

        Returns:
            Description of the metric

        Raises:
            ValueError: if the metric is not found.
        """
        metric = self.metric(name)
        if isinstance(metric, MetricBase):
            return metric.description
        else:
            raise TypeError("Type of the metric is not recognised.")

    def value_to_str(self, value: int | float, metric_name: str) -> str:
        """Get a string representation of a metric value given its format specifier.

        Args:
            value: metric value. Must be a number.
            name: metric name

        Returns:
            Formatted metric value

        Raises:
            ValueError: if the metric is not found or if the format specifier
                is not valid.
        """
        metric = self.metric(metric_name)
        if isinstance(metric, MetricBase):
            format_specifier = metric.format_specifier
        else:
            raise TypeError("Type of the metric is not recognised.")

        if format_specifier is None:
            if isinstance(value, (float, np.floating)):
                return "{value:.2f}"
            elif isinstance(value, (int, np.integer)):
                return "{value:,}"
            else:
                return str(value)
        else:
            try:
                formatted_value = f"{value:{format_specifier}}"
            except ValueError:
                raise ValueError(
                    f"Invalid format specifier `{format_specifier}` for value `{value}`"
                )
            return formatted_value

    def add_metric(self, metric: MetricBase, replace: bool = False):
        """Add a metric.

        Args:
            metric: a metric instance
            replace: whether to replace a metric if its name is already
                in the metric library
        """
        utils.formatting._assert_type(metric, MetricBase, "metric")
        name = metric.name
        if not replace and self.is_registered_metric(name):
            raise ValueError(f"A metric with same name `{name}` already exists.")
        else:
            metric.config = self.config
            self._metrics[name] = metric

    def add_metrics(self, metrics: typing.List[MetricBase], replace: bool = False):
        """Add a list of metrics.

        Args:
            metrics: a list of metric instances
            replace: whether to replace a metric if its name is already
                in the metric library
        """
        for metric in metrics:
            self.add_metric(metric)

    def remove_metric(self, metric: MetricBase | str):
        """Remove a metric.

        Args:
            metric: metric name or metric instance
        """
        if isinstance(metric, str):
            name = metric
        else:
            utils.formatting._assert_type(metric, MetricBase, "metric")
            name = metric.name

        if name not in self._metrics:
            raise ValueError(f"{name} metric not found")
        else:
            _ = self._metrics.pop(name)

    def remove_metrics(self, metrics: typing.List[MetricBase | str]):
        """Remove a list of metrics.

        Args:
            metrics: list of metric names or metric instances
        """
        for metric in metrics:
            self.remove_metric(metric)

    def add_count_distinct_metric(
        self,
        name: str,
        dfname: defs.DFName,
        columns: typing.List[str] | str,
        description: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = ",",
    ):
        """Adds a :py:class:`.custom.CountDistinctMetric`
        object to the library with the given parameters.

        Args:
            name: The name of the metric.
            dfname: A string indicating the name of the dataframe to compute
                the metric on. Must be one of ``candidates``, ``tracks``,
                or ``particles``.
            columns: Either a string or a list of strings indicating
                the columns to compute the count distinct of.
            description: An optional description for the metric.
            label: An optional label for the metric.
            format_specifier: An optional format specifier for the metric value.
        """
        self.add_metric(
            CountDistinctMetric(
                name=name,
                dfname=dfname,
                columns=columns,
                description=description,
                label=label,
                format_specifier=format_specifier,
                config=self.config,
            )
        )

    def add_sum_count_metric(
        self,
        name: str,
        dfname: defs.DFName,
        count_column: str,
        description: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = ",",
    ):
        """Adds a :py:class:`.custom.SumCountMetric`
        object to the library with the given parameters.

        Args:
            name: The name of the metric.
            dfname: A string indicating the name of the dataframe to compute
                the metric on. Must be one of ``candidates``, ``tracks``,
                or ``particles``.
            count_column: A string indicating the column to compute the sum of counts
                for.
            description: An optional description for the metric.
            label: An optional label for the metric.
            format_specifier: An optional format specifier for the metric value.
        """
        self.add_metric(
            SumCountMetric(
                name=name,
                dfname=dfname,
                count_column=count_column,
                description=description,
                label=label,
                format_specifier=format_specifier,
                config=self.config,
            )
        )

    def add_combined_metric(
        self,
        name: str,
        input1: str,
        input2: str,
        operator: typing.Literal["+", "-", "*", "/", "//", "%", "**"],
        merging: typing.Optional[typing.Literal["union", "intersection"]] = None,
        description: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = None,
    ):
        """Adds a :py:class:`.combine.CombinedMetric`
        object to the library with the given parameters.

        Args:
            name: The name of the metric.
            input1: The name of the first input metric.
            input2: The name of the second input metric.
            operator: A string indicating the arithmetic operator to use.
                Must be one of "+", "-", "*", "/", "//", "%", or "**".
            description: An optional description for the metric.
            label: An optional label for the metric.
            format_specifier: An optional format specifier for the metric value.
        """
        self.add_metric(
            CombinedMetric(
                input1=self.metric(input1),
                input2=self.metric(input2),
                operator=operator,
                name=name,
                description=description,
                label=label,
                format_specifier=format_specifier,
                merging=merging,
            )
        )

    def add_ratio_metric(
        self,
        name: str,
        input1: str,
        input2: str,
        merging: typing.Optional[typing.Literal["union", "intersection"]] = None,
        description: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = None,
    ):
        self.add_metric(
            RatioMetric(
                input1=self.metric(input1),
                input2=self.metric(input2),
                name=name,
                description=description,
                label=label,
                format_specifier=format_specifier,
                merging=merging,
            )
        )

    def add_averaged_metric(
        self,
        name: str,
        metric: str,
        average: typing.List[str] | str,
        description: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = None,
    ):
        """Adds a :py:class:`.aggregated.AverageMetric`
        object to the library with the given parameters.

        Args:
            name: The name of the metric.
            metric: The name of the metric to compute the average of.
            average: Either a string or a list of strings indicating the columns
                to average over.
            description: An optional description for the metric.
            label: An optional label for the metric.
            format_specifier: An optional format specifier for the metric value.
        """
        return self.add_metric(
            AverageMetric(
                metric=self.metric(metric),
                name=name,
                average=average,
                description=description,
                label=label,
                format_specifier=format_specifier,
                config=self.config,
            )
        )

    def compute(
        self,
        name: str,
        dataframes: typing.Dict[str, pd.DataFrame],
        groupby: typing.Optional[typing.List[str]] = None,
        average: typing.Optional[typing.List[str]] = None,
        err: str | None = None,
    ) -> MetricOutput:
        """Compute a metric value for the filtered dataframes.

        Args:
            name: The name of the metric to be computed.
            dataframes: A dictionary of dataframes to compute the metric from.
            groupby: Optional. A list of column names to group the metric values by.
            average: Optional. A list of column names to average the metric values by.

        Returns:
            The computed metric value or series.
        """
        metric = self.metric(name=name)
        if isinstance(metric, MetricBase):
            return metric.compute(dataframes, groupby=groupby, average=average, err=err)
        else:
            raise TypeError(f"Type of `{metric}` is not recognised")

    def produce_table(
        self,
        mode: TableMode = "string",
        show_hidden: bool = True,
        **kwargs,
    ):
        """Print a table that shows all the metrics stored inside
        the :py:class:`MetricsLibrary` instance.

        Args:
            mode: The format. Valid options are
                "markdown", "string", "latex", "html", "csv", and "pandas".
                Default is "string".
            show_hidden: whether to show metric labels that starts with a `_`
        """
        metric_rows = []

        for metric_name, metric in self._metrics.items():
            if show_hidden or not metric_name.startswith("_"):
                metric_rows.append(
                    {
                        "Name": metric_name,
                        "Label": metric.label,
                        "Formula": metric.formula,
                    }
                )

        df_table_metrics = pd.DataFrame(metric_rows).set_index(["Name"])
        return print(
            get_table_representation(table=df_table_metrics, mode=mode, **kwargs)
        )


#: A :py:class:`MetricsLibrary` instance that contains pre-defined metrics
metricsLibrary = MetricsLibrary()
metricsLibrary.add_count_distinct_metric(
    name="n_tracks",
    description="Number of tracks found by the tracking algorithm",
    label="# tracks",
    dfname="tracks",
    columns=defs.TRACK_INDICES,
    format_specifier=",",
)
metricsLibrary.add_count_distinct_metric(
    name="n_matched_tracks",
    description="Number of tracks that are matched to at least one particle",
    label="# matched tracks",
    dfname="candidates",
    columns=defs.TRACK_INDICES,
    format_specifier=",",
)
metricsLibrary.add_count_distinct_metric(
    name="n_particles",
    description="Number of particles",
    label="# particles",
    dfname="particles",
    columns=defs.PARTICLE_INDICES,
    format_specifier=",",
)
metricsLibrary.add_count_distinct_metric(
    name="n_matched_particles",
    description="Number of matched particles",
    label="# matched particles",
    dfname="candidates",
    columns=defs.PARTICLE_INDICES,
    format_specifier=",",
)
metricsLibrary.add_count_distinct_metric(
    name="n_candidates",
    description="Number of tracks - MC particles matches",
    label="# candidates",
    dfname="candidates",
    columns=defs.CANDIDATE_INDICES,
    format_specifier=",",
)
metricsLibrary.add_sum_count_metric(
    name="n_matched_hits_particle_track",
    description="Number of matched hits",
    label="# matched hits",
    dfname="candidates",
    count_column="n_matched_hits_particle_track",
    format_specifier=",",
)
metricsLibrary.add_sum_count_metric(
    name="n_hits_particle",
    description="Number of hits on particle",
    label="# particle hits",
    dfname="particles",
    count_column="n_hits_particle",
    format_specifier=",",
)
metricsLibrary.add_sum_count_metric(
    name="n_hits_track",
    description="Number of hits on track",
    label="# track hits",
    dfname="tracks",
    count_column="n_hits_track",
    format_specifier=",",
)
metricsLibrary.add_sum_count_metric(
    name="n_hits_matched_track",
    description="Number of hits on a matched track",
    label="# track hits",
    dfname="candidates",
    count_column="n_hits_track",
    format_specifier=",",
)
metricsLibrary.add_combined_metric(
    input1="n_tracks",
    input2="n_matched_tracks",
    operator="-",
    name="n_ghosts",
    description="Number of unmatched tracks",
    label="# ghosts",
    merging="union",
    format_specifier=",",
)
metricsLibrary.add_ratio_metric(
    input1="n_ghosts",
    input2="n_tracks",
    name="ghost_rate",
    description="Proportion of unmatched tracks",
    label=r"Ghost rate",
    format_specifier=".2%",
)
metricsLibrary.add_combined_metric(
    input1="n_candidates",
    input2="n_matched_particles",
    operator="-",
    name="n_clones",
    description="Number of redundant track-particle matches",
    label="# clones",
    format_specifier=",",
)
metricsLibrary.add_ratio_metric(
    input1="n_clones",
    input2="n_candidates",
    name="clone_rate",
    description="Proportion of redundant track-particle matches",
    label="Clone rate",
    format_specifier=".2%",
)
metricsLibrary.add_ratio_metric(
    input1="n_matched_particles",
    input2="n_particles",
    name="efficiency",
    description="Proportion of matched particles",
    label="Efficiency",
    merging="union",
    format_specifier=".2%",
)
metricsLibrary.add_combined_metric(
    input1="n_matched_hits_particle_track",
    input2="n_hits_matched_track",
    operator="/",
    name="_hit_purity",
    label="Hit purity",
    merging="intersection",
    format_specifier=".2%",
)
metricsLibrary.add_combined_metric(
    input1="n_matched_hits_particle_track",
    input2="n_hits_particle",
    operator="/",
    name="_hit_efficiency",
    label="Hit efficiency",
    merging="intersection",
    format_specifier=".2%",
)
metricsLibrary.add_averaged_metric(
    name="hit_efficiency_per_candidate",
    metric="_hit_efficiency",
    average=defs.CANDIDATE_INDICES,
    description="Average proportion of matched hits on matched particles",
    label="Average hit efficiency",
    format_specifier=".2%",
)
metricsLibrary.add_averaged_metric(
    name="hit_purity_per_candidate",
    metric="_hit_purity",
    average=defs.CANDIDATE_INDICES,
    description="Average proportion of matched hits on matched tracks",
    label="Average hit purity",
    format_specifier=".2%",
)
metricsLibrary.add_averaged_metric(
    name="efficiency_per_event",
    metric="efficiency",
    average=["event_id"],
    description="Average proportion of matched particles over events",
    label="Average efficiency",
    format_specifier=".2%",
)
