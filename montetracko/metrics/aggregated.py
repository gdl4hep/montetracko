"""This module defines a metric that is defined as the aggregation of another one.
This means that this metric computes another metric using a ``groupby`` operation,
and then apply an aggregation operation over the columns that were grouped by.
"""
from __future__ import annotations
import typing
import pandas as pd
from .base import MetricBase
from ..config import ConfigParams
from .. import utils


class AggregatedMetricBase(MetricBase):
    def __init__(
        self,
        metric: MetricBase,
        name: str,
        groupby: typing.Optional[typing.List[str] | str],
        description: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = None,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        """A base class for aggregated metrics that computes a metric value
        for a fixed set of groups.

        Args:
            metric: A metric to be aggregated.
            name: An optional name for the metric.
            groupby: An optional list of grouping columns.
            description: An optional description for the metric.
            label: An optional label for the metric.
            format_specifier: An optional format specifier for the metric value.
            config: configuration parameters.

        Raises:
            TypeError: If metric is not a MetricBase instance,
                or groupby is not a list of str.

        Attributes:
            groupby: A list of grouping columns.
            metric: The metric being aggregated.
        """
        utils.formatting._assert_type(metric, MetricBase, "metric")

        if groupby is None:
            groupby = []
        elif isinstance(groupby, str):
            groupby = [groupby]
        else:
            groupby = list(groupby)
        utils.formatting._assert_list(groupby, str, "groupby")

        if config is None:
            config = metric.config

        super().__init__(
            name=name,
            description=description,
            label=label,
            format_specifier=format_specifier,
            config=config,
        )
        self.groupby = groupby
        self.metric = metric

    def _compute(
        self,
        metric_value: int | float | pd.Series,
        groupby: typing.List[str],
        err: str | None = None,
        **kwargs,
    ):
        """Computes the aggregated metric value for a given set of groups.

        This method must be overriden.

        Args:
            metric_value: The metric value computed for the overall group.
            groupby: A list of grouping columns.

        Raises:
            NotImplementedError: If the method is not implemented in a subclass.
        """
        raise NotImplementedError

    def compute(
        self,
        *args,
        groupby: typing.Optional[typing.List[str] | str] = None,
        average: typing.Optional[str | typing.List[str]] = None,
        err: str | None = None,
        **kwargs,
    ):
        """Computes the aggregated metric value.

        Args:
            *args: passed to the ``metric``'s ``compute`` method.
            groupby: Optional. Either a single string or a list of strings. The names
                of the columns to group the results by. Default is None.
            average: Optional. Either a single string or a list of strings. The names
                of the columns to average over. Default is None.
            err: Error to compute. In the base class, only ``sym`` is defined,
                and the standard error is computed. If ``err`` is set to ``auto``,
                the error is set to ``sym``. The ``auto`` value may have a different
                behaviour in other metrics. If ``None``, no error is computed.
            **kwargs: passed to the ``metric``to metric's ``compute`` method.

        Returns:
            A computed result for the aggregated metric.

        Raises:
            TypeError: If groupby or average is not a list of str or a str.
        """
        if err == "auto":
            err = "sym"
        groupby, average = self._format_groupby_average(groupby, average)
        overall_groupby = groupby + self.groupby
        metric_value = self.metric.compute(
            *args, groupby=overall_groupby, average=average
        )
        assert not isinstance(metric_value, (dict, pd.DataFrame))
        result = self._compute(
            metric_value=metric_value, groupby=groupby, err=err, **kwargs
        )

        return self._check_computed_result(
            result=result,
            groupby=groupby,
            average=average,
        )


class AverageMetric(AggregatedMetricBase):
    def __init__(
        self,
        metric: MetricBase,
        name: str,
        average: typing.List[str] | str,
        description: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = None,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        """A metric that computes the average of a given metric over a set of groups.

        Args:
            metric: The metric to be averaged.
            name: An optional name for the metric.
            average: An optional list of columns to compute the average over.
            description: An optional description for the metric.
            label: An optional label for the metric.
            format_specifier: An optional format specifier for the metric value.
            config: An optional configuration for the metric.

        Attributes:
            groupby: A list of columns to compute the average over.
            metric: The metric being averaged.
        """
        super().__init__(
            metric=metric,
            name=name,
            groupby=average,
            description=description,
            label=label,
            format_specifier=format_specifier,
            config=config,
        )

    def _compute(
        self,
        metric_value: pd.Series,
        groupby: typing.List[str],
        err: str | None = None,
    ):
        """Private method that computes the average of the metric value
        for a given set of groups.

        Args:
            metric_value: A Pandas series containing the metric value
                computed for each group.
            groupby: A list of grouping columns.
            err: Error to compute. In the base class, only ``sym`` is defined,
                and the standard error is computed. If ``None``, no error is computed.

        Returns:
            The computed average value of the metric for the given set of groups.
        """
        return self._average(series=metric_value, groupby=groupby, err=err)

    @property
    def formula(self):
        original_label = (
            self.metric.label if self.metric.label is not None else self.metric.name
        )
        return f"Average of <{original_label}> over {self.groupby}"
