"""Module that implements the base metric, which any metric inherits from.
"""
from __future__ import annotations
import typing
import numpy as np
import pandas as pd
from ..config import ConfigurableClass, ConfigParams
from .. import utils


MetricOutput = typing.Union[
    pd.Series, pd.DataFrame, float, int, typing.Dict[str, typing.Union[float, int]]
]


class MetricBase(ConfigurableClass):
    """Base class for all metrics.

    A metric has a name, and optionally has a description and a label.
    It is computed from the ``tracks``, ``candidates`` or ``particles`` dataframes.

    Args:
        name: The name of the metric.
        description: Optional. A description of the metric.
        label: Optional. A label for the metric.
        format_specifier: Optional. A string format specifier for the metric value.
        config: Optional. A dictionary containing configuration parameters
            for the metric. If not provided, default values are used.

    Attributes:
        name: The name of the metric.
        description: A description of the metric.
        label: A label for the metric.
        format_specifier: A string format specifier for the metric value.
    """

    def __init__(
        self,
        name: str,
        description: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = None,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        super().__init__(config=config)

        # Check strings
        utils.formatting._assert_type(name, str, "name", False)
        utils.formatting._assert_type(description, str, "description", True)
        utils.formatting._assert_type(label, str, "label", True)
        utils.formatting._assert_type(format_specifier, str, "format_specifier", True)
        # Assignment
        self.name = name
        self.description = description
        self.label = label
        self.format_specifier = format_specifier

    def _compute(self, *args, groupby: typing.List[str], **kwargs):
        """A function that implements the computation of the metric and which must be
        overloaded.
        """
        raise NotImplementedError

    def _format_groupby_average(
        self,
        groupby: str | typing.List[str] | None,
        average: str | typing.List[str] | None,
    ) -> typing.Tuple[typing.List[str], typing.List[str]]:
        """Format and validate the list of columns to group by and average.

        Args:
            groupby: A string or a list of strings indicating the column names
                to group by.
            average: A string or a list of strings indicating the column names
                to average.

        Returns:
            : A tuple of two lists of strings

            * The formatted and validated list of columns to group by.
            * The formatted and validated list of columns to average.

        Raises:
            ValueError: If any column appears in both ``groupby`` and ``average``.
        """
        groupby = utils.formatting._format_list_str(
            groupby, "groupby", no_duplicates=True
        )
        average = utils.formatting._format_list_str(
            average, "average", no_duplicates=True
        )
        groupby = self.to_aliases(groupby)
        average = self.to_aliases(average)

        if average and groupby:
            for avg_column in average:
                if avg_column in groupby:
                    raise ValueError(
                        f"{avg_column} is in both `average` and `groupby`. "
                        "You cannot groupby a column you averaging over. "
                    )
        return groupby, average

    def compute(
        self,
        *args,
        groupby: typing.Optional[str | typing.List[str]] = None,
        average: typing.Optional[str | typing.List[str]] = None,
        err: str | None = None,
        **kwargs,
    ) -> MetricOutput:
        """Compute the value of the metric.

        Format the ``groupby`` and ``average`` lists before calling the
        :py:func:`_compute` method.
        Validates the output of the :py:func:`_compute` method.
        As the :py:func:`_compute` method only applies the group by operations,
        This function also also handles the ``average`` if provided.


        Args:
            *args: positional arguments passed to the underlying
                :py:func:`_compute` method.
            groupby: Optional. Either a single string or a list of strings. The names
                of the columns to group the results by. Default is None.
            average: Optional. Either a single string or a list of strings. The names
                of the columns to average over. Default is None.
            err: Error to compute. In the base class, only ``sym`` is defined,
                and the standard error is computed. If ``err`` is set to ``auto``,
                the error is set to ``sym``. This argument may have a different
                behaviour in child classes. If ``None``, no error is computed.
            **kwargs: keyword arguments passed to the underlying ``_compute`` method.

        Returns:
            Metric value.
        """
        groupby, average = self._format_groupby_average(groupby, average)
        overall_groupby = average + groupby
        result = self._compute(*args, groupby=overall_groupby, **kwargs)

        return self._check_computed_result(
            result=result,
            groupby=groupby,
            average=average,
            err=err,
        )

    def _check_computed_result(
        self,
        result: int | float | pd.Series[int | float],
        groupby: typing.List[str],
        average: typing.List[str],
        err: str | None = None,
    ) -> MetricOutput:
        """Checks if the computed result has the correct output format based on the
        ``groupby`` and ``average`` arguments provided. Apply the ``average`` if
        provided.
        Also compute the standard error of the average in the case where ``err``
        is set to ``sym``.

        Args:
            result: The computed result of the metric.
            groupby: A list of columns to group the computed result by.
            average: A list of columns to average the computed result by.
            err: ``sym`` to compute the error on the average, ``None`` otherwise.

        Returns:
            The computed result, formatted as a number or a Pandas Series depending on
            the ``groupby`` and ``average`` arguments provided.
        """
        if err == "auto":
            err = "sym"

        overall_groupby = groupby + average
        if overall_groupby:
            if not isinstance(result, (pd.Series, pd.DataFrame)):
                type_str = str(type(result))
                raise TypeError(
                    "The output of the compute method should be a Pandas Series "
                    "or a Pandas DataFrame "
                    f"but is {type_str}"
                )
            elif not result.index.names == overall_groupby:
                raise ValueError(
                    f"Index of outputted pandas Series is {result.index.names} "
                    f"but should be {overall_groupby}"
                )
            result.name = self.name
        else:
            if not isinstance(result, (int, np.integer, float, np.floating, dict)):
                type_str = str(type(result))
                raise TypeError(
                    "The output of the compute method should be a number but is "
                    + type_str
                )
        if average:
            assert isinstance(result, pd.Series)
            return self._average(result, groupby=groupby, err=err)
        else:
            return result

    def _average(
        self,
        series: pd.Series,
        groupby: typing.List[str],
        err: str | None = None,
    ) -> MetricOutput:
        """Compute the average value of a Pandas Series either globally or grouped
        by one or more columns.

        Args:
            series: The Pandas Series to compute the average of.
            groupby: A list of column names to group the Series by. If empty,
                the average is computed globally.
            err: ``sym`` to compute the error on the average, ``None`` otherwise.

        Returns:
            If ``groupby`` is not empty, returns a Pandas Series where the index
            consists of the unique combinations of values in the columns
            specified in ``groupby``, and the values are the average of the
            corresponding elements of ``series``. If ``groupby`` is empty,
            returns a single float value representing the global average of
            ``series``.
        """
        series_ = series.groupby(groupby) if groupby else series
        mean = series_.mean()
        if err is None:
            output = mean
        elif err == "sym":
            n_groups = series_.ngroups if groupby else series_.shape[0]  # type: ignore
            std = series_.std(ddof=1) / np.sqrt(n_groups)
            output = {"mean": mean, "err": std}
        else:
            raise ValueError(f"`err` should either be `None`, `sym` but is {err}")

        if groupby and err is not None:
            assert isinstance(output, dict)
            return pd.DataFrame(output)
        else:
            return output

    def __hash__(self):
        """Has of the name of the metric."""
        return hash(self.name)

    def __str__(self):
        """String representation of the metric."""
        return self.name

    def __repr__(self):
        """Representation of the metric."""
        return f"{self.__class__.__name__}[{self.name}]"

    @property
    def formula(self):
        """A string that indicates how the metric was computed. This property
        must be overloaded.
        """
        raise NotImplementedError
