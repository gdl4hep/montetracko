"""Implement the :py:class:`CustomMetric` class.
A Custom Metric is a metric that is computed from a Pandas DataFrame.

For instance, :py:class:`SumCountMetric` is a metric defined by summing a column
in a Pandas DataFrame.
The :py:class:`CountDistinctMetric` is a metric defined by counting the number
of unique columns in a Pandas DataFrame.
"""
from __future__ import annotations
import typing
import pandas as pd
from .base import MetricBase
from ..config import ConfigParams
from ..defs import POSSIBLE_DFNAMES
from .. import utils
from .. import array_utils


class CustomMetric(MetricBase):
    def __init__(
        self,
        name: str,
        dfname: typing.Optional[str],
        description: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = None,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        """A custom metric that is computed on a single pandas DataFrame.

        Args:
            name: A name for the metric.
            dfname: A name of the pandas DataFrame on which the metric will be computed.
            description: A description for the metric.
            label : A label for the metric.
            format_specifier: A format specifier to format the metric.
            config: configuration parameters

        Raises:
            TypeError: If ``dfname`` is not of type str.
            ValueError: If ``dfname`` is not a valid DataFrame name.

        Attributes:
            name: A name for the metric.
            description: A description for the metric.
            label: A label for the metric.
            format_specifier: A format specifier to format the metric.
            config: Configuration parameters for the metric.
            dfname: Name of the pandas DataFrame on which the metric will be computed.
        """
        super().__init__(
            name=name,
            description=description,
            label=label,
            format_specifier=format_specifier,
            config=config,
        )
        # Check `dfname`
        utils.formatting._assert_type(dfname, str, "dfname")
        if dfname not in POSSIBLE_DFNAMES:
            raise ValueError(
                "Specified `dfname` is " + str(dfname) + " which is unknown. "
                "Possible values are: " + ", ".join(POSSIBLE_DFNAMES)
            )

        self.dfname = dfname

    def compute(
        self,
        dataframes: pd.DataFrame | typing.Dict[str, pd.DataFrame],
        groupby: typing.Optional[typing.List[str] | str] = None,
        average: typing.Optional[typing.List[str] | str] = None,
        err: str | None = None,
        **kwargs,
    ):
        """Computes the metric over a Pandas DataFrame.

        Args:
            dataframes: A Pandas DataFrame or a dictionary of Pandas DataFrames.
                If it is a dictionary, the metric will be computed over the DataFrame
                with key ``dfname``.
            groupby: Optional. Either a single string or a list of strings. The names
                of the columns to group the results by. Default is None.
            average: Optional. Either a single string or a list of strings. The names
                of the columns to average over. Default is None.
            err: Error to compute. In the base class, only ``sym`` is defined,
                and the standard error is computed. If ``err`` is set to ``auto``,
                the error is set to ``sym``. This argument may have a different
                behaviour in child classes. If ``None``, no error is computed.
            **kwargs: keyword arguments passed to the underlying ``_compute`` method.

        Returns:
            The computed metric, as either a number or a Pandas Series.

        Raises:
            ValueError: If a dictionary of DataFrames is provided but the key
                ``dfname`` attribute is not in it.
            TypeError: If ``dataframes`` is not a Pandas DataFrame or
                a dictionary of Pandas DataFrames.
        """
        if isinstance(dataframes, dict):
            if self.dfname in dataframes:
                dataframe = dataframes[self.dfname]
            else:
                raise ValueError(
                    "The provided dictionnary of dataframes do not contain "
                    f"the dataframe named `{self.dfname}`"
                )
        else:
            dataframe = dataframes

        utils.formatting._assert_type(dataframe, pd.DataFrame, "dataframe")

        return super().compute(
            dataframe, groupby=groupby, average=average, err=err, **kwargs
        )

    def _compute(
        self,
        dataframe: pd.DataFrame,
        groupby: typing.List[str],
        **kwargs,
    ):
        """Compute the custom metric using the provided DataFrame.

        This method is not implemented and must be overridden.

        Args:
            dataframe: A pandas DataFrame object containing the data used to compute
                the custom metric.
            groupby: A list of strings representing the columns used to group the
                data in `dataframe` before computing the metric.
        """
        raise NotImplementedError

    def __repr__(self):
        """Representation of a :py:class:`CustomMetric`."""
        return super().__repr__() + f"({self.dfname})"


class CountDistinctMetric(CustomMetric):
    """Counts the number of distinct values in a column(s) in a specified dataframe.

    Args:
        name: The name of the metric.
        dfname: The name of the dataframe in which to count distinct values.
        columns: The column(s) to count the number of distinct values.
        description: A description of the metric. Defaults to None.
        label: A label for the metric. Defaults to None.
        format_specifier: A format specifier for the metric value. Defaults to ",".
        config: Configuration parameters for the metric. Defaults to None.

    Attributes:
        columns: The column(s) to count the number of distinct values.
    """

    def __init__(
        self,
        name: str,
        dfname: str,
        columns: typing.List[str] | str,
        description: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = ",",
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        super().__init__(
            name=name,
            dfname=dfname,
            description=description,
            label=label,
            format_specifier=format_specifier,
            config=config,
        )
        utils.formatting._assert_list(columns, str, "columns")
        self.columns = [columns] if isinstance(columns, str) else list(columns)

    def _compute(
        self,
        dataframe: pd.DataFrame,
        groupby: typing.Optional[typing.List[str]],
    ) -> int | pd.Series:
        """Compute the number of distinct values in the specified columns
        of the given dataframe.

        Args:
            dataframe: The dataframe in which to count distinct values.
            groupby : A list of column names to group by. Defaults to None.

        Returns:
            The number of distinct values in the specified columns
            of the given dataframe.
        """
        return array_utils.dataframes.count_distinct_groups(
            dataframe=dataframe,
            columns=self.to_aliases(self.columns),
            groupby=groupby,
        )

    @property
    def formula(self):
        """String representation of the metric formula."""
        return f"Number of unique {self.columns} in `{self.dfname}` dataframe"


class SumCountMetric(CustomMetric):
    """Metric that computes the sum of a given column.

    Args:
        name: The name of the metric.
        dfname: The name of the dataframe in which to count distinct values.
        count_column: the column ot sum up
        description: A description of the metric. Defaults to None.
        label: A label for the metric. Defaults to None.
        format_specifier: A format specifier for the metric value. Defaults to ",".
        config: Configuration parameters for the metric. Defaults to None.

    Attributes:
        count_column: The column to sum up
    """

    def __init__(
        self,
        name: str,
        dfname: str,
        count_column: str,
        description: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = ",",
        label: typing.Optional[str] = None,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        super().__init__(
            name=name,
            dfname=dfname,
            description=description,
            label=label,
            format_specifier=format_specifier,
            config=config,
        )
        utils.formatting._assert_type(count_column, str, "count_column")
        self.count_column = count_column

    def _compute(
        self,
        dataframe: pd.DataFrame,
        groupby: typing.List[str],
    ) -> int | pd.Series[int]:
        """Compute the sum of the given column,
        as well as the count of rows for each group.

        Args:
            dataframe: The input dataframe.
            groupby: A list of column names to group by.

        Returns:
            The sum of the given column
        """
        return array_utils.dataframes.sum_column_count(
            dataframe=dataframe,
            count_column=self.to_alias(self.count_column),
            groupby=groupby,
        )

    @property
    def formula(self):
        """String representation of the metric formula."""
        return f"Sum of {self.count_column} in `{self.dfname}` dataframe"
