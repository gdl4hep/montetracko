#pragma once

#include <vector>
#include "TEfficiency.h"
#include "TH1D.h"

TH1D *fill_histogram(const char *name, std::vector<double> vector_values)
{
    TH1D *hist = new TH1D(name, name, vector_values.size(), 0, 1);
    for (int i = 0; i < vector_values.size(); i++)
    {
        hist->SetBinContent(i + 1, vector_values[i]);
    }
    return hist;
}

void compute_efficiency_uncertainty(
    const std::vector<double> &vector_pass,
    const std::vector<double> &vector_tot,
    std::vector<double> &efficiency_avg,
    std::vector<double> &efficiency_err_low,
    std::vector<double> &efficiency_err_up)
{
    assert(vector_pass.size() == vector_tot.size());

    TH1D *hist_pass = fill_histogram("pass", vector_pass);
    TH1D *hist_tot = fill_histogram("tot", vector_tot);

    TEfficiency *efficiency_map = new TEfficiency(*hist_pass, *hist_tot);
    // uniform prior
    efficiency_map->SetStatisticOption(TEfficiency::EStatOption::kBUniform);
    efficiency_map->SetPosteriorMode(true);

    for (int i = 0; i < vector_pass.size(); i++)
    {
        efficiency_avg[i] = efficiency_map->GetEfficiency(i + 1);
        efficiency_err_low[i] = efficiency_map->GetEfficiencyErrorLow(i + 1);
        efficiency_err_up[i] = efficiency_map->GetEfficiencyErrorUp(i + 1);
    }
    delete efficiency_map;
    delete hist_pass;
    delete hist_tot;
}
