"""Module that implements the :py:class`CompositeMetric` class.
A composite metric is a metric that is expressed as a function of other metrics,
and only uses the dataframes through these metrics.
"""
from __future__ import annotations
import typing
import pandas as pd
from .base import MetricBase
from .custom import CustomMetric
from ..config import ConfigParams
from .. import utils
from .. import array_utils


def _assert_type_for_composite_metric(metric: int | float | MetricBase):
    """Assert that a metric has a valid type for a composite metric.

    Args:
        metric: The metric to check. It can be an integer, a float,
            or a :py:class:`MetricBase` object.

    Raises:
        ValueError: If the metric is not an integer, a float, or a MetricBase object.
    """
    if not isinstance(metric, (int, float, MetricBase)):
        raise ValueError(
            "Metric does not have the correct type. "
            "It must either be an integer or float number, or a metric"
        )


class CompositeMetric(MetricBase):
    """A metric that is calculated from one or more other metrics.

    Args:
        inputs: A list of one or more inputs to the composite metric.
            Each input must be either an integer or float, or
            a :py:class:`.base.MetricBase` instance.
        name: The name of the composite metric.
        merging: A string indicating how to combine pandas series representing
            the metric values.
            Must be either ``union``, ``intersection`` or ``None``. None means
            that the index of the pandas Series of two different metrics must
            be the exact same. Defaults to None.
        description: A string describing the composite metric.
        label: A string label for the composite metric.
        format_specifier: A string specifying how to format the composite metric.
        config: configuration parameters

    Attributes:
        inputs: A list of one or more inputs to the composite metric.
            Each input must be either an integer or float, or
            a :py:class:`.base.MetricBase` instance.
        input_metrics: A list of inputs that are :py:class:`.base.MetricBase` instances.
        merging: A string indicating how to combine pandas series representing
            the metric values.
            Must be either ``union``, ``intersection`` or ``None``. None means
            that the index of the pandas Series of two different metrics must
            be the exact same. Defaults to None.

    Raises:
        ValueError: If ``merging`` is not one of `union``, ``intersection`` or ``None``.
        ValueError: If no input is a :py:class:`.base.MetricBase` instance
        ValueError: If some input metrics have different configurations
    """

    def __init__(
        self,
        inputs: typing.List[int | float | MetricBase],
        name: str,
        merging: typing.Optional[typing.Literal["union", "intersection"]] = None,
        description: typing.Optional[str] = None,
        label: typing.Optional[str] = None,
        format_specifier: typing.Optional[str] = None,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        utils.formatting._assert_var_in_list(
            merging, ["union", "intersection", None], "merging"
        )

        # Check the input
        are_metrics = []
        for input in inputs:
            _assert_type_for_composite_metric(input)
            are_metrics.append(isinstance(input, MetricBase))

        # Require at least one metric
        if not any(are_metrics):
            raise ValueError("Either `metric1` or `metric2` must be a metric")

        metrics = [input for input in inputs if isinstance(input, MetricBase)]

        # Configuration `config`
        if config is None:  # deduce it from the input metrics
            config = metrics[0].config
            for metric in metrics[1:]:
                if config != metric.config:
                    raise ValueError("Metrics have different configurations.")

        super().__init__(
            name=name,
            description=description,
            label=label,
            format_specifier=format_specifier,
            config=config,
        )

        self.inputs = inputs
        self.input_metrics = metrics
        self.merging: typing.Optional[typing.Literal["union", "intersection"]] = merging

    def _compute(self, **kwargs):
        """Computes the value of the metric given the input metric values.

        This method should be implemented by a subclass.
        """
        raise NotImplementedError

    def compute(
        self,
        dataframes: typing.Optional[typing.Dict[str, pd.DataFrame]] = None,
        groupby: typing.Optional[typing.List[str] | str] = None,
        average: typing.Optional[str | typing.List[str]] = None,
        err: str | None = None,
        **kwargs,
    ):
        """Computes the composite metric value via the :py:func:`_compute` method.

        Args:
            dataframes: A dictionary of pandas  dataframes containing the data
                needed to compute the metric. Defaults to ``None``.
            groupby: Optional. Either a single string or a list of strings. The names
                of the columns to group the results by. Default is None.
            average: Optional. Either a single string or a list of strings. The names
                of the columns to average over. Default is None.
            err: Error to compute. In the base class, only ``sym`` is defined,
                and the standard error is computed. If ``err`` is set to ``auto``,
                the error is set to ``sym``. This argument may have a different
                behaviour in child classes. If ``None``, no error is computed.
            **kwargs: keyword arguments passed to the underlying ``_compute`` method.

        Raises:
            ValueError: If the specified columns for grouping or averaging are not
                in the provided dataframes.
            TypeError: If the input metric is not recognized.

        Returns:
            The computed metric value.
        """
        if dataframes is None:
            dataframes = {}
        groupby, average = self._format_groupby_average(groupby, average)
        overall_groupby = groupby + average
        overall_groupby_in_at_least_one_dataframe = False

        # Compute metrics
        input_metric_values = {}
        for metric in self.input_metrics:
            if isinstance(metric, CustomMetric):
                dfname = metric.dfname

                if dfname not in dataframes:
                    raise ValueError(
                        f"Dataframe {dfname} is not in the provided "
                        f"dictionnary of dataframes, whose keys are {dataframes.keys()}"
                    )
                dataframe = dataframes[dfname]
                groupby_in_dataframe = [
                    groupby_column
                    for groupby_column in overall_groupby
                    if groupby_column in dataframe
                ]
                if groupby_in_dataframe == overall_groupby:
                    overall_groupby_in_at_least_one_dataframe = True
                input_metric_values[metric.name] = metric.compute(
                    dataframe,
                    groupby=groupby_in_dataframe,
                )
            elif isinstance(metric, CompositeMetric):
                input_metric_values[metric.name] = metric.compute(
                    dataframes,
                    groupby=overall_groupby,
                )
                if overall_groupby:
                    if set(input_metric_values[metric.name].index.names) == set(
                        overall_groupby
                    ):
                        overall_groupby_in_at_least_one_dataframe = True
            else:
                raise TypeError(f"Type of `{metric}` is not recognised")

        if overall_groupby and not overall_groupby_in_at_least_one_dataframe:
            raise ValueError(
                f"It was required to groupby over: {overall_groupby}. "
                "However, none of the input dataframes contains all of these "
                "columns."
            )

        # The type (number or Pandas Series) of the variables should already have been
        # checked by the ``compute`` method of the respective metrics
        # Need to treat the case where we need to combine indexes
        if overall_groupby and len(input_metric_values) >= 2:
            # First broadcast indices
            name_to_index_levels = {
                name: series.index.names if isinstance(series, pd.Series) else []
                for name, series in input_metric_values.items()
            }
            # Find the Series with most levels
            name_to_nlevels = {
                name: len(index_levels)
                for name, index_levels in name_to_index_levels.items()
            }
            largest_index_levels_name = max(name_to_nlevels, key=name_to_nlevels.get)  # type: ignore
            largest_series = input_metric_values[largest_index_levels_name]
            # Broadcast other series so that they match the largest
            for name, sub_metric_value in input_metric_values.items():
                if name != largest_index_levels_name:
                    input_metric_values[
                        name
                    ] = array_utils.series.broadcast_index_series(
                        subseries=sub_metric_value,
                        series=largest_series,
                    )

            # Reindexing
            input_metric_values = dict(
                zip(
                    input_metric_values.keys(),
                    array_utils.series.merge_series_indices(
                        input_metric_values.values(), merging=self.merging
                    ),
                )
            )

        result = self._compute(**input_metric_values, **kwargs)
        return self._check_computed_result(
            result, groupby=groupby, average=average, err=err
        )
