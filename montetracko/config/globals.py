"""Some global configuration variables.
For the moment, they are hard-coded in this file.
"""

#: Configuration
NUMBA_CONFIG = {
    "nopython": True,
    "cache": True,
}

#: List of column names used in the DataFrames that can be customized by users
#: in the configuration
SPECIAL_COLUMNS = ["event_id", "track_id", "particle_id", "hit_id"]
