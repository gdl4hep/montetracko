"""Class that has a configuration."""
from __future__ import annotations
import typing
from .configparams import ConfigParams


class ConfigurableClass:
    """A class that holds a py:class:`.ConfigParams` instance."""

    def __init__(self, config: typing.Optional[ConfigParams | dict] = None):
        if config is None:
            config = ConfigParams()
        elif isinstance(config, dict):
            config = ConfigParams(**config)
        elif not isinstance(config, ConfigParams):
            raise ValueError("`config` input has wrong type.")
        self._config = config

    @property
    def config(self) -> ConfigParams:
        """Configuration parameters."""
        return self._config

    @config.setter
    def config(self, config: ConfigParams | dict | None):
        self.set_config(config)

    def set_config(self, config: ConfigParams | dict | None):
        """Sets the configuration parameters for the object.

        Args:
            config: A dictionary, a ``ConfigParams`` object, or ``None``. If a
                dictionary is provided, it will be used to create a new
                ``ConfigParams`` object. If a ``ConfigParams`` object is provided, it
                will be used directly. If ``None`` is provided, the default
                ``ConfigParams`` object will be used.

        Raises:
            TypeError: If `config` is not a `ConfigParams` object, a dictionary,
                or `None`.
    """
        if isinstance(config, ConfigParams):
            self._config = config
        elif isinstance(config, dict):
            self._config = ConfigParams(**config)
        elif config is None:
            self._config = ConfigParams()
        else:
            raise TypeError(
                "`config` can either be a `ConfigParams`, a `dict` or `None`"
            )

    @property
    def aliases(self) -> typing.Dict[str, str]:
        """Aliases for special columns used in track matching.

        A dictionary mapping special column names to their corresponding aliases.
        """
        return self.config["aliases"]

    def to_alias(self, column: str) -> str:
        """Return the alias for a special column, or the column name if no alias
        is defined.

        Args:
            column: The name of the special column to retrieve an alias for.

        Returns:
            The alias for the special column
        """
        return self.aliases.get(column, column)

    def to_aliases(self, columns: typing.List[str]) -> typing.List[str]:
        """Return the aliases for special colums, or the column names if no alias
        is defined.

        Args:
            columns: list of column names

        Returns:
            The list of column names, where the column names with aliases have been
            replaced by their aliases.
        """
        return [self.to_alias(column) for column in columns]

    @property
    def fake_particle_id(self):
        """Value of the ``particle_id`` in the case where the ``hit_id`` is not
        associated with any MC particle.
        """
        return self.config["fake_particle_id"]
