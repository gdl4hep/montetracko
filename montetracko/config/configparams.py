"""Default the ``ConfigParams`` class that handles the configuration.
"""

from .configdef import param_to_constraints, param_to_default
import pprint
import copy


class ConfigParams(dict):
    """A dict-like key-value store for config parameters, including validation,
    inspired from `RcParams` in the matplotlib library.

    The class ensures that parameter values are validated upon creation and that new
    key-value pairs conform to expected constraints.
    """

    # validate values on the way in
    def __init__(self, **kwargs):
        """Initialise a new instance of :py:class:`ConfigParams`

        Args:
            **kwargs: named configuration parameters
        """
        self.update(**param_to_default)
        self.update(**kwargs)

    @staticmethod
    def validate(param: str, value) -> bool:
        """Validate a new key-value pair.

        Args:
            param: the name of the parameter
            value: the value of the parameter

        Raises:
            TypeError: if param is not a string
            ValueError: if param is not recognised
            ValueError: if value has wrong type

        Returns:
            True if validation succeeds

        """
        if not isinstance(param, str):
            raise TypeError("Parameter " + str(param) + " should be a string.")
        elif param not in param_to_constraints:
            raise ValueError("Parameter " + str(param) + " is not recognised.")
        else:
            constraint = param_to_constraints[param]
            if isinstance(constraint, type):
                if not isinstance(value, constraint):
                    print("constraint:", str(constraint))
                    raise ValueError(
                        "The value of parameter " + str(param) + " has wrong type"
                    )
                else:
                    return True
            else:
                assert callable(constraint)
                valid = constraint(value)
                assert isinstance(valid, bool)
                return valid

    def _set(self, key: str, value):
        """Private method to set a new key-value pair

        Args:
            key: the name of the parameter
            value: the value of the parameter
        """
        dict.__setitem__(self, key, value)

    def __setitem__(self, key: str, value):
        """Update a key-value pair in the configuration.

        Args:
            key: the name of the parameter
            value: the value of the parameter

        Raises:
            ValueError: if key-value pair is not valid
        """
        valid = self.validate(key, value)
        if valid:
            self._set(key, value)
        else:
            raise ValueError(
                "Key " + str(key) + " with value " + str(value) + " is not valid"
            )

    def _get(self, key: str):
        """Private method to retrieve a value.

        Args:
            key: the name of the parameter

        Returns:
            the value of the parameter
        """
        return dict.__getitem__(self, key)

    def __getitem__(self, key: str):
        """Get the value of a parameter.

        Args:
            key: the name of the parameter

        Returns:
            the value of the parameter
        """
        return self._get(key)

    def __str__(self) -> str:
        """String configuration of the configuration."""
        return "\n".join(map("{0[0]}: {0[1]}".format, sorted(self.items())))

    def __repr__(self) -> str:
        """Representation of the configuration."""
        class_name = self.__class__.__name__
        indent = len(class_name) + 1
        repr_split = pprint.pformat(dict(self), indent=1, width=80 - indent).split("\n")
        repr_indented = ("\n" + " " * indent).join(repr_split)
        return f"{class_name}({repr_indented})"

    def __iter__(self):
        """Yield sorted list of keys."""
        yield from sorted(dict.__iter__(self))

    def __len__(self):
        return dict.__len__(self)

    def copy(self):
        """Copy this :py:class:`ConfigParam` instance."""
        config_copy = ConfigParams()
        for k in self:
            config_copy._set(k, copy.deepcopy(self._get(k)))
        return config_copy

    def update(self, **kwargs):
        """Update dictionnary"""
        for key, value in kwargs.items():
            self[key] = value
