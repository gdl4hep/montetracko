"""Definition of the default values and formats of the parameteters of the
configuration.
"""
import typing
import multiprocessing


def _check_dict_str(dict_: typing.Any) -> bool:
    """Check whether a variable is a dictionary with string keys and values."""
    if not isinstance(dict_, dict):
        return False
    for key, value in dict_.items():
        if not isinstance(key, str):
            return False
        if not isinstance(value, str):
            return False
    return True


#: Associates a parameter name with a type, or a function that returns `True`
#: if the value provided in input has correct formatting
param_to_constraints = {
    "aliases": _check_dict_str,
    "ncores": int,
    "fake_particle_id": int,
}

#: Associates a parameter name with its default value in the configuration
param_to_default = {
    "aliases": {
        "event_id": "event_id",
        "track_id": "track_id",
        "hit_id": "hit_id",
        "particle_id": "particle_id",
        "n_hits_particle": "n_hits_particle",
    },
    "ncores": multiprocessing.cpu_count(),
    "fake_particle_id": -1,
}
