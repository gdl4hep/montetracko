"""This package provides classes for track matching and evaluation. It defines
the :py:class:`matching.TrackMatcher` and :py:class:`trackevaluator.TrackEvaluator`
classes.
"""
from . import matching, trackevaluator
from .matching import TrackMatcher
from .trackevaluator import TrackEvaluator

__all__ = [
    "matching",
    "trackevaluator",
    "TrackMatcher",
    "TrackEvaluator",
]
