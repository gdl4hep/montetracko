"""Module that defines base classes used in the the
:py:class:`.trackevaluator.TrackEvaluator` class.
"""
from __future__ import annotations
import typing
import pandas as pd
from .. import defs
from ..config import ConfigurableClass, ConfigParams
from .. import utils
from ..requirement import Category


class DataFramesDict(dict, ConfigurableClass):
    """A dictionnary of dataframes.

    This class corresponds to a dictionnary, that contains 3 dataframes:
    * ``particles``: the dataframe of particles. The columns ``event_id`` \
    and ``particle_id`` must not have duplicates.
    * ``tracks``: the dataframe of tracks. The columns ``event_id``  \
    and ``track_id`` must not have duplicates.
    * ``candidates``: the dataframe of particle-track matching candidates.  \
    The columns ``event_id``, ``track_id`` must not have duplicates.

    Attributes:
        dataframes: a dictionary of dataframes.
        check_validity: whether or not to check the validity of the dataframes every
            time a dataframe is set.
        config: configuration parameters.
    """

    def __init__(
        self,
        dataframes: typing.Optional[typing.Dict[defs.DFName, pd.DataFrame]] = None,
        check_validity: bool = False,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        """Constructs a ``DataFramesDict`` instance."""
        self.check_validity = check_validity
        dict.__init__(self)
        ConfigurableClass.__init__(self, config=config)
        if dataframes is not None:
            self.update(dataframes)

    def _set(self, key: str, value: pd.DataFrame):
        """Sets the value of the key to a Pandas DataFrame."""
        return dict.__setitem__(self, key, value)

    def _get(self, key: defs.DFName):
        """Gets the value of the key from the DataFramesDict."""
        return dict.__getitem__(self, key)

    def _update(self, *args, **kwargs):
        """Updates the DataFramesDict instance."""
        return super().update(*args, **kwargs)

    def _assert_key_validity(self, key: str):
        """Asserts that the key is valid."""
        utils.formatting._assert_var_in_list(key, defs.POSSIBLE_DFNAMES, "key")

    def _assert_registered_key(self, key: defs.DFName):
        """Asserts that the key is registered."""
        if key not in self:
            raise KeyError(f"Dataframe `{key} is not registered.")

    def _assert_key_value_validity(self, key: str, dataframe: pd.DataFrame):
        """Asserts that the value associated with the key is a Pandas DataFrame.
        If the ``check_validity`` attribute is set to ``True``, also check
        that the relevant columns do not have duplicates.
        """
        self._assert_key_validity(key)
        if not isinstance(dataframe, pd.DataFrame):
            raise TypeError(
                f"Value associated with key `{key}` should have type pd.DataFrame"
            )
        if self.check_validity:  # Check the validity of the dataframes
            unique_columns = self.to_aliases(defs.DFNAME_TO_INDICES[key])
            for unique_column in unique_columns:
                if unique_column not in dataframe:
                    raise ValueError(
                        f"{unique_column} column is not in the dataframe {key}"
                    )
            if dataframe[unique_columns].duplicated().any():
                raise ValueError(
                    f"The following columns of the `{key}` dataframe "
                    f"should be unique: " + ", ".join(unique_columns)
                )

    def __setitem__(self, key: str, value: pd.DataFrame) -> None:
        """Sets an item in the dataframe.

        Args:
            key: the dataframe name.
            value: the dataframe.

        Raises:
            KeyError: if the key is not a valid dataframe name.
            TypeError: if the dataframe is not a pandas dataframe.
            ValueError: if the dataframe columns are invalid or duplicated for the
                considered dataframe.
        """
        self._assert_key_value_validity(key, value)
        return self._set(key, value)

    def __getitem__(self, key: str) -> pd.DataFrame:
        """Gets a dataframe given its name.

        Args:
            key: the dataframe name.

        Raises:
            ValueError: if the dataframe is not registered.

        Returns:
            The pandas dataframe.
        """
        utils.formatting._assert_var_in_list(key, defs.POSSIBLE_DFNAMES, "key")
        if key not in self:
            raise ValueError(
                f"DataFrame associated with the `{key}` was not registered."
            )
        return super().__getitem__(key)

    def update(
        self, *args: typing.Dict[defs.DFName, pd.DataFrame], **kwargs: pd.DataFrame
    ):
        """Updates the dictionnary with the specified key-value pairs.

        Args:
            *args: list of ``dict``, represents the key-value pairs
                to update in the dictionnary.
            **kwargs: key-value pairs to update in the dictionnary
        """
        # Enforce constraints on all key-value pairs
        for arg in args:
            if isinstance(arg, dict):
                for key, value in arg.items():
                    self.__setitem__(key, value)
        for key, value in kwargs.items():
            self.__setitem__(key, value)
        # Call the superclass method to update the dictionary
        return super().update(*args, **kwargs)


class FilteredDataFramesCollection(ConfigurableClass):
    """A class representing a collection of dataframes that can be filtered by
    a category. This class caches the filtered dataframes and reset it only if
    the ``category`` attribute is changed to a different category.

    Attributes:
        check_validity: Whether to check the validity of the dataframes when updating.
    """

    def __init__(
        self,
        dataframes: typing.Optional[typing.Dict[defs.DFName, pd.DataFrame]] = None,
        category: typing.Optional[Category] = None,
        config: typing.Optional[ConfigParams | dict] = None,
        check_validity: bool = True,
    ):
        super().__init__(config=config)
        self.check_validity = check_validity
        self._dataframes = DataFramesDict(dataframes=dataframes, config=self.config)
        self._category = category
        self._filtered_dataframes = DataFramesDict(
            check_validity=False, config=self.config
        )

        self.reset_filtered_dataframes()

    def set_config(self, config):
        """Set the configuration for the :py:class:`FilteredDataFramesCollection`
        object and propagate it to the :py:class:`DataFramesDict` objects that hold
        the dataframes.
        """
        super(FilteredDataFramesCollection, self).set_config(config)
        self._dataframes.config = self.config
        self._filtered_dataframes.config = self.config

    @property
    def category(self) -> Category | None:
        """Getter for the category."""
        return self._category

    @category.setter
    def category(self, category: typing.Optional[Category] = None):
        """Setter for the category. If the ``category`` is new, reset the filtered
        dataframes.

        Args:
            category: An optional ``Category`` instance to be set as the category.

        Raises:
            TypeError: If ``category`` is not an instance of
                :py:class:`..requirement.category.Category`.
        """
        if self._category != category:
            utils.formatting._assert_type(
                category, Category, "category", can_be_none=True
            )
            # print(f"Category has changed from {self._category} to {category}")
            self._category = category
            self.reset_filtered_dataframes()

    def reset_filtered_dataframes(self):
        """Resets the filtered dataframes and sets it to an empty
        :py:class:`DataFramesDict`.
        """
        self._filtered_dataframes = DataFramesDict(
            check_validity=False, config=self.config
        )

    @property
    def has_category(self) -> bool:
        """Check if the instance has a category set."""
        return self.category is not None

    def _filter_dataframe(self, name: defs.DFName):
        """Filter a dataframe given its name and store the filtered dataframe
        in the ``_filtered_dataframes`` attribute.

        Args:
            name: Name of the dataframe.
        """
        if name in defs.FILTERED_DFNAMES:
            assert self.category is not None, "No category was set."
            self._filtered_dataframes[name] = self.category.filter(
                self._dataframes[name]
            )
        else:
            self._filtered_dataframes[name] = self._dataframes[name]

    @property
    def filtered_dataframes(self) -> DataFramesDict:
        """Returns a dictionary of dataframes filtered by the category, if it exists.

        If no category has been set, this property returns the original ``dataframes``
        dictionary.

        Returns:
            DataFramesDict: The filtered dataframes dictionary.
        """
        if self.has_category:
            for dfname in self.dataframes:
                if dfname not in self._filtered_dataframes:
                    self._filter_dataframe(dfname)
            return self._filtered_dataframes
        else:
            return self.dataframes

    @property
    def dataframes(self) -> DataFramesDict:
        return self._dataframes

    @dataframes.setter
    def dataframes(
        self,
        dataframes: typing.Optional[
            typing.Dict[defs.DFName, pd.DataFrame] | DataFramesDict
        ] = None,
    ):
        """Sets the dataframes of the instance. This also resets the filtered
        dataframes.

        Args:
            dataframes: The dictionary of dataframes to set as the instance's
                dataframes. Defaults to None.
        """
        self._dataframes = DataFramesDict(
            dataframes=dataframes,
            check_validity=self.check_validity,
            config=self.config,
        )
        self.reset_filtered_dataframes()
