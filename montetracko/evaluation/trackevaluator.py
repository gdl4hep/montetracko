"""This module provides the :py:class:`TrackEvaluator` class,
which enables the evaluation of tracking algorithms.
"""
from __future__ import annotations
import typing
import copy
import numpy as np
import numpy.typing as npt
import pandas as pd
from matplotlib.axes import Axes
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from .collections import FilteredDataFramesCollection
from .. import defs
from ..config import ConfigParams
from ..metrics.base import MetricOutput
from ..metrics import metricsLibrary
from ..requirement import Category
from ..report import ReporterBase
from .. import utils


class TrackEvaluator(FilteredDataFramesCollection):
    """A class for evaluating track reconstruction quality.

    This class brings together metrics, categories and reporters to compute
    metrics in various categories and present them in a readable format.

    Attributes:
        dataframes: a dictionary of pandas dataframes.
        category: the category of the dataframes.
        config: configuration parameters
    """

    def __init__(
        self,
        dataframes: typing.Dict[defs.DFName, pd.DataFrame] | None = None,
        category: Category | None = None,
        config: ConfigParams | dict | None = None,
    ):
        """Initializes the TrackEvaluator object."""
        FilteredDataFramesCollection.__init__(
            self,
            dataframes=dataframes,
            category=category,
            config=config,
        )
        self.update_metric_library()

    def update_metric_library(self):
        """This method updates the internal :py:data:`..metrics.metricsLibrary`
        attribute with a deep copy of the latest metrics library.
        It also updates the configuration of the
        ``metricsLibrary`` with the latest configuration of the instance.
        """
        self.metricsLibrary = copy.deepcopy(metricsLibrary)
        self.metricsLibrary.config = self.config

    def set_config(self, config: ConfigParams | dict | None):
        """Set the configuration of the evaluator and propagate the configuration
        to the ``metricLibrary`` and dataframes collections.
        """
        super(TrackEvaluator, self).set_config(config)
        self.metricsLibrary.config = self.config
        self.dataframes.config = self.config

    @typing.overload
    def _compute_metric(
        self,
        metric_name: str,
        dataframes: typing.Dict[str, pd.DataFrame],
        groupby: None = ...,
        average: typing.List[str] | None = ...,
        err: str = ...,
    ) -> typing.Dict[str, int | float]:
        ...

    @typing.overload
    def _compute_metric(
        self,
        metric_name: str,
        dataframes: typing.Dict[str, pd.DataFrame],
        groupby: typing.List[str],
        average: typing.List[str] | None = ...,
        err: str = ...,
    ) -> pd.DataFrame:
        ...

    @typing.overload
    def _compute_metric(
        self,
        metric_name: str,
        dataframes: typing.Dict[str, pd.DataFrame],
        groupby: None = ...,
        average: typing.List[str] | None = ...,
        err: None = None,
    ) -> int | float:
        ...

    @typing.overload
    def _compute_metric(
        self,
        metric_name: str,
        dataframes: typing.Dict[str, pd.DataFrame],
        groupby: typing.List[str],
        average: typing.List[str] | None = ...,
        err: None = None,
    ) -> pd.Series:
        ...

    def _compute_metric(
        self,
        metric_name: str,
        dataframes: typing.Dict[str, pd.DataFrame],
        groupby: typing.List[str] | None = None,
        average: typing.List[str] | None = None,
        err: str | None = None,
    ) -> MetricOutput:
        """Compute a metric value for the filtered dataframes.

        Args:
            metric_name: The name of the metric to be computed.
            dataframes: A dictionary of dataframes to compute the metric from.
            groupby: Optional. A list of column names to group the metric values by.
            average: Optional. A list of column names to average the metric values by.
            category: Optional. The category which the dataframes are filtered with.
                If not set, the original dataframes are used.

        Returns:
            The computed metric value or series.
        """
        return self.metricsLibrary.compute(
            name=metric_name,
            groupby=groupby,
            average=average,
            dataframes=dataframes,
            err=err,
        )

    @typing.overload
    def compute_metric(
        self,
        metric_name: str,
        groupby: None = None,
        average: typing.List[str] | None = None,
        err: None = None,
        category: Category | None = None,
    ) -> int | float:
        ...

    @typing.overload
    def compute_metric(
        self,
        metric_name: str,
        groupby: typing.List[str],
        average: typing.List[str] | None = None,
        err: None = None,
        category: Category | None = None,
    ) -> pd.Series:
        ...

    @typing.overload
    def compute_metric(
        self,
        metric_name: str,
        groupby: None = None,
        average: typing.List[str] | None = None,
        err: str = ...,
        category: Category | None = None,
    ) -> typing.Dict[str, int | float]:
        ...

    @typing.overload
    def compute_metric(
        self,
        metric_name: str,
        groupby: typing.List[str],
        average: typing.List[str] | None = None,
        err: str = ...,
        category: Category | None = None,
    ) -> pd.DataFrame:
        ...

    def compute_metric(
        self,
        metric_name: str,
        groupby: typing.List[str] | None = None,
        average: typing.List[str] | None = None,
        err: str | None = None,
        category: Category | None = None,
    ) -> MetricOutput:
        """Compute a metric value for the filtered dataframes.

        Args:
            metric_name: The name of the metric to be computed.
            groupby: Optional. A list of column names to group the metric values by.
            average: Optional. A list of column names to average the metric values by.
            category: Optional. The category which the dataframes are filtered with.
                If not set, the original dataframes are used.

        Returns:
            The computed metric value or series.
        """
        self.category = category
        return self._compute_metric(
            metric_name=metric_name,
            groupby=groupby,
            average=average,
            dataframes=self.filtered_dataframes,
            err=err,
        )

    @typing.overload
    def compute_metrics(
        self,
        metric_names: typing.List[str],
        groupby: None = None,
        average: typing.List[str] | None = None,
        err: None = None,
        category: Category | None = None,
    ) -> typing.Dict[str, int | float]:
        ...

    @typing.overload
    def compute_metrics(
        self,
        metric_names: typing.List[str],
        groupby: typing.List[str],
        average: typing.List[str] | None = None,
        err: None = None,
        category: Category | None = None,
    ) -> typing.Dict[str, pd.Series]:
        ...

    @typing.overload
    def compute_metrics(
        self,
        metric_names: typing.List[str],
        groupby: None = None,
        average: typing.List[str] | None = None,
        err: str = ...,
        category: Category | None = None,
    ) -> typing.Dict[str, typing.Dict[str, int | float]]:
        ...

    @typing.overload
    def compute_metrics(
        self,
        metric_names: typing.List[str],
        groupby: typing.List[str],
        average: typing.List[str] | None = None,
        err: str = ...,
        category: Category | None = None,
    ) -> typing.Dict[str, pd.DataFrame]:
        ...

    def compute_metrics(
        self,
        metric_names: typing.List[str],
        groupby: typing.List[str] | None = None,
        average: typing.List[str] | None = None,
        err: str | None = None,
        category: Category | None = None,
    ) -> (
        typing.Dict[str, int | float]
        | typing.Dict[str, pd.Series]
        | typing.Dict[str, typing.Dict[str, int | float]]
        | typing.Dict[str, pd.DataFrame]
    ):
        """Compute metric values for the filtered dataframes.

        Args:
            metric_names: A list of metric names to be computed.
            groupby: Optional. A list of column names to group the metric values by.
            average: Optional. A list of column names to average the metric values by.
            category: Optional. The category which the dataframes are filtered with.
                If not set, the original dataframes are used.

        Returns:
            A dictionary containing the metric name as key and the computed metric
            value as value.
        """
        self.category = category
        metric_values = {}
        for metric_name in metric_names:
            metric_values[metric_name] = self._compute_metric(
                metric_name,
                groupby=groupby,
                average=average,
                err=err,
                dataframes=self.filtered_dataframes,
            )
        return metric_values

    def compute_metrics_categories(
        self,
        metric_names: typing.List[str],
        categories: typing.List[Category | None] | typing.List[Category] | None = None,
    ) -> typing.Dict[str | None, typing.Dict[str, int | float]]:
        """Compute a dictionary of metric values for the filtered dataframes across
        different categories.

        Args:
            metric_names: A list of the names of the metrics to be computed.
            categories: Optional. A list of categories to compute the metrics for.
                In this list, ``None`` is understood as using the original dataframes
                with no selection applied.

        Returns:
            A dictionary of nested metrics,
            where the top-level keys are category names,
            and the values are dictionaries of metric names
            and their associated values.
        """
        if categories is None:
            categories = [None]
        nested_metric_values: typing.Dict[
            str | None, typing.Dict[str, int | float]
        ] = {}
        for category in categories:
            category_name = None if category is None else category.name
            nested_metric_values[category_name] = self.compute_metrics(
                metric_names=metric_names, category=category
            )
        return nested_metric_values

    def report(
        self,
        reporter: ReporterBase,
        categories: typing.List[Category | None] | typing.List[Category] | None = None,
    ) -> str:
        """Compute a report for the filtered dataframes across different categories,
        based on the given reporter.

        Args:
            reporter: An instance of a reporter class that implements the
                ``report`` method.
            categories: Optional. A list of categories to compute the report for.
                In this list, ``None`` is understood as using the original dataframes
                with no selection applied. If not specified, ``[None]`` is used.

        Returns:
            A string containing the computed report.
        """
        utils.formatting._assert_type(reporter, ReporterBase, "reporter")
        if categories is None:
            categories = [None]

        category_names = [
            None if category is None else category.name for category in categories
        ]
        if len(set(category_names)) != len(category_names):
            raise ValueError("Some of the categories have the same names")

        metric_names = reporter.metric_names
        global_metric_names = reporter.global_metric_names

        # Compute global metric values
        global_metric_values: typing.Dict[str, int | float] = self.compute_metrics(
            metric_names=global_metric_names
        )

        # Compute category-based metric values, that could be computed globally
        # for the all dataframe
        category_based_metric_values = self.compute_metrics_categories(
            metric_names=metric_names,
            categories=categories,
        )

        # Add the global metric values to the category-based metric values
        category_based_metric_values[None] = {
            **category_based_metric_values.get(None, {}),
            **global_metric_values,
        }
        return reporter.report(
            categories=categories,
            nested_metric_values=category_based_metric_values,  # type: ignore
        )

    def print_report(self, *args, **kwargs):
        """Print a report for the filtered dataframes across different categories,
        based on the given reporter.

        Args:
            *args: passed to :py:func:`trackEvaluator.report`
            *kwargs: passed to :py:func:`trackEvaluator.report`
        """
        print(self.report(*args, **kwargs))

    @typing.overload
    def compute_histogram(
        self,
        column: str,
        metric_name: str,
        bins: npt.ArrayLike | typing.SupportsIndex = 50,
        range: typing.Tuple[float, float] | None = None,
        average: typing.List[str] | None = None,
        err: None = ...,
        category: Category | None = None,
    ) -> typing.Tuple[np.ndarray, np.ndarray, np.ndarray]:
        ...

    @typing.overload
    def compute_histogram(
        self,
        column: str,
        metric_name: str,
        bins: npt.ArrayLike | typing.SupportsIndex = 50,
        range: typing.Tuple[float, float] | None = None,
        average: typing.List[str] | None = None,
        err: str = ...,
        category: Category | None = None,
    ) -> typing.Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        ...

    def compute_histogram(
        self,
        column: str,
        metric_name: str,
        bins: npt.ArrayLike | typing.SupportsIndex = 50,
        range: typing.Tuple[float, float] | None = None,
        average: typing.List[str] | None = None,
        err: str | None = None,
        category: Category | None = None,
    ) -> (
        typing.Tuple[np.ndarray, np.ndarray, np.ndarray]
        | typing.Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]
    ):
        """Compute a histogram of a column of the filtered dataframes and a metric
        value associated with each bin.

        Args:
            column: The column name to compute the histogram on.
            metric_name: The name of the metric to compute for each bin.
            bins: Optional. Specification of the bins. Default is 50.
                This is passed to :py:func:`numpy.histogram`.
            range: Optional. The lower and upper range of the bins.
                This is passed to :py:func:`numpy.histogram`.
            average: Optional. A list of column names to average the metric values by.
            category: Optional. The category which the dataframes are filtered with.
                If not set, the original dataframes are used.

        Returns:
           :  A tuple containing

           * the histogram of the column in the ``particles`` dataframe
           * the metric values for each bin
           * the bin edges
        """
        # Get dataframes in the category that is considered
        self.category = category

        # Filter dataframes based on the low and high values of bin edges
        filtered_dataframes = self.filtered_dataframes

        # Get bins
        particle_histogram, edges = np.histogram(
            filtered_dataframes["particles"][column],
            bins=bins,
            range=range,
        )
        bin_column_name = f"_{column}_bin"
        filtered_dataframes_in_bin = {}
        # Create the bin column
        for dataframe_name in defs.FILTERED_DFNAMES:
            filtered_dataframes_in_bin[dataframe_name] = filtered_dataframes[
                dataframe_name
            ].copy(deep=True)
            assert bin_column_name not in filtered_dataframes[dataframe_name]
            filtered_dataframes_in_bin[dataframe_name][bin_column_name] = pd.cut(
                filtered_dataframes_in_bin[dataframe_name][column],
                bins=pd.Series(edges),
                labels=False,
                include_lowest=True,
            )
            # Remove the elements that are not in any of the bins
            filtered_dataframes_in_bin[dataframe_name] = filtered_dataframes_in_bin[
                dataframe_name
            ][filtered_dataframes_in_bin[dataframe_name][bin_column_name].notna()]

        # Get metric values grouped by bins.
        try:
            df_or_series_metric_values: pd.Series | pd.DataFrame = self._compute_metric(
                metric_name=metric_name,
                dataframes=filtered_dataframes_in_bin,
                groupby=[bin_column_name],
                average=average,
                err=err,
            )
        except ValueError as message:
            if str(message) == (
                "Dataframe tracks is not in the provided "
                + "dictionnary of dataframes, whose keys are "
                + f"{filtered_dataframes_in_bin.keys()}"
            ):
                raise ValueError(
                    "You are trying to compute a metric that is computed from "
                    "the dataframe of tracks, that cannot be binned on "
                    "a truth-based particle-related variable."
                )
            else:
                raise message

        df_or_series_metric_values = df_or_series_metric_values.reindex(
            np.arange(len(edges) - 1)
        )

        series_metric_values = (
            df_or_series_metric_values
            if err is None
            else df_or_series_metric_values["mean"]
        )
        # Reindex to take into account empty bins

        if err is None:
            array_metric_values = series_metric_values.to_numpy()
            return particle_histogram, array_metric_values, edges
        else:
            assert isinstance(df_or_series_metric_values, pd.DataFrame)
            array_metric_values = df_or_series_metric_values["mean"].to_numpy()
            if (
                "err_neg" in df_or_series_metric_values
                and "err_pos" in df_or_series_metric_values
            ):
                bin_errs = (
                    df_or_series_metric_values[["err_neg", "err_pos"]].to_numpy().T
                )
            elif "err" in df_or_series_metric_values:
                bin_errs = df_or_series_metric_values["err"].to_numpy()
            else:
                raise ValueError(
                    "neither `err_neg` and `err_pos`, not `err` could be found in "
                    "the dataframe. The dataframe as the following column: "
                    f"{df_or_series_metric_values.columns}"
                )
            return particle_histogram, array_metric_values, edges, bin_errs

    def plot_histogram(
        self,
        column: str,
        metric_name: str,
        column_label: str | None = None,
        bins: npt.ArrayLike | typing.SupportsIndex = 50,
        range: typing.Tuple[float, float] | None = None,
        average: typing.List[str] | None = None,
        err: str | None = None,
        category: Category | None = None,
        ax: Axes | None = None,
        histogram_color: str | None = None,
        show_histogram: bool = True,
        **kwargs,
    ) -> typing.Tuple[
        typing.Tuple[Figure, Axes, Axes] | typing.Tuple[Figure, Axes] | Axes | None,
        npt.NDArray,
        npt.NDArray,
        npt.NDArray,
        npt.NDArray | None,
    ]:
        """Plot a histogram with metric values for each bin.

        Args:
            column: The column name to compute the histogram on.
            metric_name: The name of the metric to compute for each bin.
            bins: Optional. Specification of the bins. Default is 50.
                This is passed to :py:func:`numpy.histogram`.
            range: Optional. The lower and upper range of the bins.
                This is passed to :py:func:`numpy.histogram`.
            average: Optional. A list of column names to average the metric values by.
            category: Optional. The category which the dataframes are filtered with.
                If not set, the original dataframes are used.

        Returns:
            If ``ax`` is not ``None``, only returns the twin second axes object
            for the histogram if it was passed.
            If ``ax`` is None, returns a tuple containing the matplotlib figure object,
            the matplotlib axes object andthe twin second axes object
            for the histogram if it was passed.
        """
        output = self.compute_histogram(
            column=column,
            metric_name=metric_name,
            bins=bins,
            range=range,
            average=average,
            err=err,  # type: ignore
            category=category,
        )
        if err is None:
            particle_histogram, array_metric_values, edges = output
            array_metric_errs = None
        else:
            particle_histogram, array_metric_values, edges, array_metric_errs = output

        return (
            self._plot_histogram(
                column=column,
                metric_name=metric_name,
                array_metric_values=array_metric_values,
                array_metric_errs=array_metric_errs,
                edges=edges,
                column_label=column_label,
                histogram=particle_histogram if show_histogram else None,
                ax=ax,
                histogram_color=histogram_color,
                **kwargs,
            ),
            particle_histogram,
            array_metric_values,
            edges,
            array_metric_errs,
        )

    @typing.overload
    def _plot_histogram(
        self,
        column: str,
        metric_name: str,
        array_metric_values: np.ndarray,
        edges: npt.ArrayLike,
        array_metric_errs: np.ndarray | None = None,
        column_label: str | None = None,
        histogram: np.ndarray = ...,
        ax: None = None,
        color: str | None = "k",
        histogram_color: str | None = None,
        **kwargs,
    ) -> typing.Tuple[Figure, Axes, Axes]:
        ...

    @typing.overload
    def _plot_histogram(
        self,
        column: str,
        metric_name: str,
        array_metric_values: np.ndarray,
        edges: npt.ArrayLike,
        array_metric_errs: np.ndarray | None = None,
        column_label: str | None = None,
        histogram: None = None,
        ax: None = None,
        color: str | None = "k",
        histogram_color: str | None = None,
        **kwargs,
    ) -> typing.Tuple[Figure, Axes]:
        ...

    @typing.overload
    def _plot_histogram(
        self,
        column: str,
        metric_name: str,
        array_metric_values: np.ndarray,
        edges: npt.ArrayLike,
        array_metric_errs: np.ndarray | None = None,
        column_label: str | None = None,
        histogram: None = ...,
        ax: Axes = ...,
        color: str | None = "k",
        histogram_color: str | None = None,
        **kwargs,
    ) -> None:
        ...

    @typing.overload
    def _plot_histogram(
        self,
        column: str,
        metric_name: str,
        array_metric_values: np.ndarray,
        edges: npt.ArrayLike,
        array_metric_errs: np.ndarray | None = None,
        column_label: str | None = None,
        histogram: np.ndarray = ...,
        ax: Axes = ...,
        color: str | None = "k",
        histogram_color: str | None = None,
        **kwargs,
    ) -> Axes:
        ...

    def _plot_histogram(
        self,
        column: str,
        metric_name: str,
        array_metric_values: np.ndarray,
        edges: npt.ArrayLike,
        array_metric_errs: np.ndarray | None = None,
        column_label: str | None = None,
        histogram: np.ndarray | None = None,
        ax: Axes | None = None,
        color: str | None = "k",
        histogram_color: str | None = None,
        alpha: float = 1.0,
        **kwargs,
    ) -> typing.Tuple[Figure, Axes, Axes] | typing.Tuple[Figure, Axes] | Axes | None:
        """Plot a histogram with metric values for each bin.

        Args:
            column: The column name to compute the histogram on.
            metric_name: The name of the metric to compute for each bin.
            array_metric_values: The metric values for each bin.
            edges: The bin edges.
            column_label: Optional. The label for the column on the x-axis.
                Default is the column name.
            histogram: Optional. The histogram values to be overlaid on the plot.
            ax: Optional. The matplotlib axes object to be used for the plot.

        Returns:
            If ``ax`` is not ``None``, only returns the twin second axes object
            for the histogram if it was passed.
            If ``ax`` is None, returns a tuple containing the matplotlib figure object,
            the matplotlib axes object andthe twin second axes object
            for the histogram if it was passed.
        """
        if ax is None:
            fig, ax_ = plt.subplots(figsize=(8, 6))
        else:
            fig = None
            ax_ = ax

        metric_label = self.metricsLibrary.label(metric_name)
        column_label = column if column_label is None else column_label

        edges = np.asarray(edges)
        centers = (edges[:-1] + edges[1:]) / 2
        bin_widths = edges[1:] - edges[:-1]

        ax_.errorbar(
            x=centers,
            y=array_metric_values,
            xerr=bin_widths / 2,
            yerr=array_metric_errs,  # type: ignore
            linestyle="",
            markersize=5,
            marker=".",
            color=color,
            alpha=alpha,
            **kwargs,
        )
        ax_.axhline(y=1, color="grey", alpha=0.5)
        ax_.set_xlim(edges[0], edges[-1])
        # Label
        ax_.set_xlabel(column_label)
        ax_.set_ylabel(metric_label.replace("%", r"%"))
        # Grid
        ax_.grid(color="grey", alpha=0.5, which="both")

        if histogram is not None:
            if histogram_color is None:
                histogram_color = "purple"
            ax_histogram = ax_.twinx()
            ax_.set_zorder(ax_histogram.get_zorder() + 1)
            ax_.patch.set_visible(False)  # type: ignore
            ax_histogram.patch.set_visible(True)  # type: ignore
            ax_histogram.bar(
                x=centers,
                height=histogram,
                width=edges[1] - edges[0],
                color=histogram_color,
                alpha=0.7,
                **kwargs,
            )
            ax_histogram.set_ylabel("Distribution count", color=histogram_color)
            ax_histogram.tick_params(axis="y", colors=histogram_color)
        else:
            ax_histogram = None

        # ax_histogram.grid(color=histogram_color, alpha=0.2, which="both")
        if ax is None:
            assert fig is not None
            if histogram is not None:
                assert ax_histogram is not None
                return fig, ax_, ax_histogram
            else:
                assert fig is not None
                return fig, ax_
        else:
            if histogram is not None:
                return ax_histogram
