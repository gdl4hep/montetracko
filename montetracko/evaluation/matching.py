"""This module contains the implementation of the :py:class:`TrackMatcher` class
that can be used to match tracks to particles.
"""
from __future__ import annotations
import typing
import pandas as pd
from ..config import ConfigurableClass
from ..matchcond.base import MatchingConditionBase
from .trackevaluator import TrackEvaluator
from .. import defs


class TrackMatcher(ConfigurableClass):
    def __init__(self, config):
        super().__init__(config=config)

    def compute_number_hits_per_particle(
        self,
        df_events_particles: pd.DataFrame,
        df_events_hits_particles: pd.DataFrame,
    ) -> pd.DataFrame:
        """Computes the number of hits per particle and returns a new
        ``df_events_particles`` dataframe with the new column.

        Args:
            df_events_particles: The dataframe containing the particle information.
            df_events_hits_particles: The dataframe containing the hits-particles
                association.

        Returns:
            A new dataframe with the same columns as ``df_events_particles``
                and an additional column ``n_hits_particle``
                (or its alias as defined in the configuration)
                indicating the number of hits per particle.
        """
        # Number of hits / particle
        n_hits = (
            df_events_particles[self.to_aliases(defs.PARTICLE_INDICES)]
            .merge(
                df_events_hits_particles[
                    self.to_aliases(["event_id", "particle_id", "hit_id"])
                ],
                how="left",
                on=self.to_aliases(defs.PARTICLE_INDICES),
            )  # -> dataframe [event_id, particle_id, hit_id]
            .groupby(self.to_aliases(defs.PARTICLE_INDICES))
            .count()  # -> count the number of `hit_id` per `particle_id`
            .rename(columns={self.to_alias("hit_id"): "n_hits_particle"})
            .astype(int)
        )
        # Add the new column to the particle dataframe and return
        return df_events_particles.merge(
            n_hits,
            how="left",
            on=self.to_aliases(defs.PARTICLE_INDICES),
        )

    def compute_number_hits_per_track(
        self,
        df_events_tracks_hits: pd.DataFrame,
    ) -> pd.DataFrame:
        """Computes the number of hits per track in a given DataFrame.

        Args:
            df_events_tracks_hits: A DataFrame with the event ID, track ID and hit ID
                columns, containing information about the hits and tracks.

        Returns:
            A DataFrame with the event ID and track ID columns and
            the ``n_hits_track`` column, the number of hits associated
            with each track in each event.
        """
        return (
            df_events_tracks_hits.groupby(self.to_aliases(["event_id", "track_id"]))[
                self.to_alias("hit_id")
            ]
            .count()  # -> number of hits / track / event
            .reset_index()  # -> dataframe
            .rename(columns={self.to_alias("hit_id"): "n_hits_track"})
        )

    def compute_number_fake_hits_per_track(
        self,
        df_events_tracks_hits_particles: pd.DataFrame,
    ):
        """Computes the number of fake (non-matched) hits per track in a given
        DataFrame.

        Args:
            df_events_tracks_hits: A DataFrame with the event ID, track ID, hit ID
                and particle ID columns.

        Returns:
            A DataFrame with the event ID and track ID columns and
            the ``n_hits_track`` column, the number of hits that are not matched
            to any particle
            (i.e., for which the particle ID column is equal to
            the ``fake_particle_id`` in the configuration.)
        """
        # Create a boolean mask for fake hits
        df_events_tracks_hits_particles["fake"] = (
            df_events_tracks_hits_particles[self.to_alias("particle_id")]
            == self.fake_particle_id
        )
        # Count the number of fake hits / track
        df_n_fake_hits_track = (
            df_events_tracks_hits_particles.groupby(
                self.to_aliases(["event_id", "track_id"])
            )["fake"]
            .sum()  # -> number of fake hits / track / event
            .reset_index()  # -> dataframe
            .rename(columns={"fake": "n_fake_hits_track"})
        )
        df_events_tracks_hits_particles.drop("fake", axis=1, inplace=True)
        return df_n_fake_hits_track

    def compute_number_matched_hits(
        self,
        df_events_tracks_hits_particles: pd.DataFrame,
    ):
        """Computes the number of matched hits per track-particle match in a given
        DataFrame.

        Args:
            df_events_tracks_hits: A DataFrame with the event ID, track ID, hit ID
                and particle ID columns.

        Returns:
            A DataFrame with the event ID, track ID columns and particle ID columns and
            the ``n_matched_hits_particle_track`` column, the number of hits
            on the track that are matched to the particle considered.
        """
        return (
            df_events_tracks_hits_particles.groupby(
                self.to_aliases(["event_id", "track_id", "particle_id"])
            )
            .count()
            .reset_index()
            .rename(columns={self.to_alias("hit_id"): "n_matched_hits_particle_track"})
        )

    def matching_without_filtering(
        self,
        df_events_tracks_hits: pd.DataFrame,
        df_events_hits_particles: pd.DataFrame,
    ) -> typing.Tuple[pd.DataFrame, pd.DataFrame]:
        """Match tracks to MC particles and returns the dataframe of matched candidates
        and the dataframes of tracks with information about the hits.

        Args:
            df_tracks_events_hits_particles: A Pandas DataFrame with 3 columns:
                the event IDs, the track IDs and the hit IDs. This dataframe defines
                the tracks to match to particles.
            df_events_hits_particles: A Pandas DataFrame with 3 columns:
                the event IDs, the hit IDs and the corresponding particle IDs.
                This dataframe provides the mapping from hit ID to particle ID,
                which is necessary for the matching.

        Returns:
            : 2 Pandas DataFrames are returned.

                * DataFrame of the tracks-particles matching candidates, with 6 \
                columns. The event ID, the track ID and the particle ID columns allow \
                to identify a matching candidate. \
                ``n_hits_track`` is the number of hits on the track. \
                ``n_fake_hits_track`` is the number of hits not matched to any \
                particles. ``n_matched_hits_particle_track`` is the number \
                of hits matched to the given particle. These 3 columns can be used \
                to filter the matching candidates.
                * DataFrames of the tracks (even those which are not matched), with 5 \
                columns. The event ID and track ID allow to identify a track. \
                `n_hits_track`` is the number of hits on the track. \
                ``n_fake_hits_track`` is the number of hits not matched to any \
                particles.

        Notes:
            The function uses three helper methods to compute the supplementary
            columns of the DataFrames:
            :py:func:`TrackMatcher.compute_number_hits_per_track`,
            :py:func:`TrackMachter.compute_number_fake_hits_per_track`,
            and :py:func`compute_number_matched_hits`.
        """
        # Only keep relevant columns
        df_events_hits_particles = df_events_hits_particles[
            self.to_aliases(["event_id", "hit_id", "particle_id"])
        ]
        df_events_tracks_hits = df_events_tracks_hits[
            self.to_aliases(["event_id", "track_id", "hit_id"])
        ]

        #: Dataframe with 4 columns:
        #: the event ID, the track ID, the hit ID and the particle ID
        #: (where particle ID might indicate that the hit is a fake one
        #: it is kept to compute the proportion of fake hits in a track)
        df_events_tracks_hits_particles = df_events_tracks_hits.merge(
            df_events_hits_particles,
            on=self.to_aliases(["event_id", "hit_id"]),
            how="left",
        )

        # We only need the hit ID for the matching but it is fairly useless
        # for tracking evaluation
        df_events_tracks_particles = df_events_tracks_hits_particles[
            self.to_aliases(["event_id", "track_id", "particle_id"])
        ].drop_duplicates()

        # Now we compute the 3 supplementary columns
        # 1. The number of hits / track
        df_n_hits_track = self.compute_number_hits_per_track(df_events_tracks_hits)
        # 2. The number of fake hits / track
        df_n_fake_hits_track = self.compute_number_fake_hits_per_track(
            df_events_tracks_hits_particles
        )
        # 3. The number of matched hits / track / matched MC particle
        df_n_matched_hits = self.compute_number_matched_hits(
            df_events_tracks_hits_particles
        )

        # The 3 new columns are merged to the dataframe
        # First remove fake hits
        df_matching_candidates = df_events_tracks_particles[
            df_events_tracks_particles[self.to_alias("particle_id")]
            != self.fake_particle_id
        ]
        for df_to_merge, on_columns in (
            (df_n_hits_track, ["event_id", "track_id"]),
            (df_n_fake_hits_track, ["event_id", "track_id"]),
            (df_n_matched_hits, ["event_id", "track_id", "particle_id"]),
        ):
            df_matching_candidates = df_matching_candidates.merge(
                df_to_merge,
                on=self.to_aliases(on_columns),
                how="left",
            )

        # Create the DataFrame of tracks
        df_tracks_info = df_n_hits_track.merge(
            df_n_fake_hits_track,
            on=self.to_aliases(["event_id", "track_id"]),
            how="left",
        )

        return df_matching_candidates, df_tracks_info

    def filter(
        self,
        df_matching_candidates: pd.DataFrame,
        condition: MatchingConditionBase | str | typing.Callable,
    ) -> pd.DataFrame:
        """Filter the track-particle matches according to a condition.

        Args:
            df_matching_candidates: Dataframe that  represents the
                matching candidates between tracks and MC particles.

            condition: The condition to filter the tracks with. The condition
                can be one of the following:

                * a string: In this case, a simple `query` is used to filter the
                    dataframe.
                * a callable: A function that takes the dataframe as input and
                    returns a boolean mask that indicates which track-MC particle
                    associations to keep.
                * a :py:class:`MatchingConditionBase` subclass intance,
                    that has a `filter` method that takes the
                    dataframe as input and returns a boolean mask that indicates which
                    track-MC particle associations to keep.

        Returns:
            A new Pandas DataFrame of track-particle matches containing only the
            matching candidates that meet the filtering condition.

        Raises:
            ValueError: If the condition provided is not a string, callable, or a
            subclass of `MatchingConditionBase`.
        """
        if isinstance(condition, str):
            return df_matching_candidates.query(condition)
        elif isinstance(condition, MatchingConditionBase):
            condition.config = self.config
            return condition.filter(df_matching_candidates)
        elif callable(condition):
            return df_matching_candidates[condition(df_matching_candidates)]
        else:
            raise ValueError(
                "The condition given as input is not understood. "
                "It should either be a MatchinCondition, a `str` or a `callable`."
            )

    def matching(
        self,
        df_events_tracks_hits: pd.DataFrame,
        df_events_hits_particles: pd.DataFrame,
        matching_condition: typing.Optional[
            MatchingConditionBase | str | typing.Callable
        ] = None,
        track_condition: typing.Optional[
            MatchingConditionBase | str | typing.Callable
        ] = None,
    ) -> typing.Tuple[pd.DataFrame, pd.DataFrame]:
        """Match tracks to MC particles and filter them based on a given condition.

        Args:
            Args:
            df_tracks_events_hits_particles: A Pandas DataFrame with 3 columns:
                the event IDs, the track IDs and the hit IDs. This dataframe defines
                the tracks to match to particles.
            df_events_hits_particles: A Pandas DataFrame with 3 columns:
                the event IDs, the hit IDs and the corresponding particle IDs.
                This dataframe provides the mapping from hit ID to particle ID,
                which is necessary for the matching.
            matching_condition: The condition to filter the matching candidates with.
                The condition can be one of the following:

                * a string: In this case, a simple `query` is used to filter the
                    dataframe.
                * a callable: A function that takes the dataframe as input and
                    returns a boolean mask that indicates which track-MC particle
                    associations to keep.
                * a :py:class:`MatchingConditionBase` subclass intance,
                    that has a `filter` method that takes the
                    dataframe as input and returns a boolean mask that indicates which
                    track-MC particle associations to keep.
                * ``None`` of all the candidates are kept
            track_condition: The condition to filter the tracks with.
                The condition can be either a ``string``, a ``callable``,
                :py:class:`MatchingConditionBase` subclass intance, or ``None``,
                like for a matching condition. It is only provided for convenience
                in order to filter the tracks.

        Returns:
            : A tuple of two Pandas DataFrames

            * A DataFrame of the matched tracks and their corresponding MC particles, \
            filtered according to the given condition (if any). This DataFrame has six \
            columns: the event ID, the track ID, the particle ID, the number of hits \
            in the track (``n_hits_track``), the number of fake hits in the track \
            (``n_hits_fake_track``), and the number of hits matched  \
            to the given MC particle (``n_matched_hits_particle_track``)
            * A DataFrame of all the tracks (event ID and track ID columns), \
            matched or not, with the number of hits in the track and the number of \
            fake hits in the track.
        """
        df_matching_candidates, df_tracks_info = self.matching_without_filtering(
            df_events_tracks_hits=df_events_tracks_hits,
            df_events_hits_particles=df_events_hits_particles,
        )
        if track_condition is not None:
            df_matching_candidates = self.filter(
                df_matching_candidates,
                condition=track_condition,
            )
            df_tracks_info = self.filter(
                df_tracks_info,
                condition=track_condition,
            )
        if matching_condition is not None:
            df_matching_candidates = self.filter(
                df_matching_candidates,
                condition=matching_condition,
            )
        return df_matching_candidates, df_tracks_info

    def full_matching(
        self,
        df_events_tracks_hits: pd.DataFrame,
        df_events_hits_particles: pd.DataFrame,
        df_events_particles: pd.DataFrame,
        matching_condition: typing.Optional[
            MatchingConditionBase | str | typing.Callable
        ],
        track_condition: typing.Optional[
            MatchingConditionBase | str | typing.Callable
        ] = None,
    ) -> TrackEvaluator:
        """Match tracks to MC particles and filter them based on a given condition.

        Also computes the number of hits in each particle add it to the
        the ``particles`` dataframes, and merge the particle properties in
        ``df_events_particles`` to the dataframe of candidates.

        Args:
            df_tracks_events_hits_particles: A Pandas DataFrame with 3 columns:
                the event IDs, the track IDs and the hit IDs. This dataframe defines
                the tracks to match to particles.
            df_events_hits_particles: A Pandas DataFrame with 3 columns:
                the event IDs, the hit IDs and the corresponding particle IDs.
                This dataframe provides the mapping from hit ID to particle ID,
                which is necessary for the matching.
            matching_condition: The condition to filter the matching candidates with.
                The condition can be one of the following:

                * a string: In this case, a simple `query` is used to filter the
                    dataframe.
                * a callable: A function that takes the dataframe as input and
                    returns a boolean mask that indicates which track-MC particle
                    associations to keep.
                * a :py:class:`MatchingConditionBase` subclass intance,
                    that has a `filter` method that takes the
                    dataframe as input and returns a boolean mask that indicates which
                    track-MC particle associations to keep.
                * ``None`` of all the candidates are kept
            track_condition: The condition to filter the tracks with.
                The condition can be either a ``string``, a ``callable``,
                :py:class:`MatchingConditionBase` subclass intance, or ``None``,
                like for a matching condition. It is only provided for convenience
                in order to filter the tracks.

        Returns:
            A :py:class:`TrackEvaluator` instance that contains the relevant
            ``dataframes`` attribute.
        """
        df_matching_candidates, df_tracks_info = self.matching(
            df_events_tracks_hits=df_events_tracks_hits,
            df_events_hits_particles=df_events_hits_particles,
            matching_condition=matching_condition,
            track_condition=track_condition,
        )
        # Compute the number of hits per particle if not already in the `particle`
        # dataframe
        nhits_column_name = self.to_alias("n_hits_particle")
        if nhits_column_name not in df_events_particles:
            df_events_particles = self.compute_number_hits_per_particle(
                df_events_particles=df_events_particles,
                df_events_hits_particles=df_events_hits_particles,
            )
        # Marge particle information to the `candidates` dataframe
        df_matching_candidates = df_matching_candidates.merge(
            df_events_particles,
            on=self.to_aliases(["event_id", "particle_id"]),
            how="left",
        )

        return TrackEvaluator(
            dataframes={
                "candidates": df_matching_candidates,
                "tracks": df_tracks_info,
                "particles": df_events_particles,
            },
            config=self.config,
        )
