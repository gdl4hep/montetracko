"""This module contains functions to check if a library is installed.
"""


def pyarrow_installed() -> bool:
    try:
        import pyarrow  # noqa: F401

        return True
    except ModuleNotFoundError:
        return False
