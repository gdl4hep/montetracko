"""This module provides utility functions for type checking and string representation
of classes.
"""
from __future__ import annotations
import typing
import pprint


def _get_parameterised_repr(
    class_instance, params: typing.Optional[typing.List[str] | dict] = None
):
    """Return a string representation of a parameterised class

    Args:
        class_instance: An instance of a class.
        params: A list of parameter names or a dictionary of parameter names and values
                for the class instance. If None, only the class name is returned.

    Returns:
        A string representation of the class instance,
        including its class name and parameter values.

    Example:
        >>> obj = MyClass(x=1, y=2)
        >>> _get_parameterised_repr(obj)
        'MyClass()'
        >>> _get_parameterised_repr(obj, params=['1'])
        'MyClass(1)'

        >>> _get_parameterised_repr(obj, params={'x':1, 'y': 2})
        'MyClass(x=1, y=2)'
    """
    class_name = class_instance.__class__.__name__
    indent = len(class_name) + 1
    if params is not None:
        repr_split = pprint.pformat(params, indent=1, width=80 - indent).split("\n")
        repr_indented = ("\n" + " " * indent).join(repr_split)
    else:
        repr_indented = ""
    return f"{class_name}({repr_indented})"


def _assert_list(list_: typing.Any, elementType: type, varname: str):
    """Raise an error if the elements of a list are not of a given type

    Args:
        list_: The list to check.
        elementType: The type all elements of the list should have.
        varname: The name of the variable being checked.

    Raises:
        TypeError: If ``list_`` is not a list, or if one of its elements is not of type
            ``elementType``.
    """
    if not isinstance(list_, list):
        raise TypeError(f"`{varname}` should be a list")
    for element in list_:
        if not isinstance(element, elementType):
            raise TypeError(
                f"One of the element of `{varname}` "
                f"is not of type {elementType.__name__}"
            )


def _assert_type(
    var: typing.Any,
    varType: type,
    varname: str,
    can_be_none: bool = False,
):
    """Raise an error if a variable does not have a specified type.

    This function checks if the given variable has the specified type. It also
    raises an error if the variable is ``None`` and ``can_be_none`` is False.

    Args:
        var: The variable to check the type of.
        varType: The expected type of the variable.
        varname: The name of the variable being checked.
        can_be_none: Whether the variable is allowed to be None. Defaults to False.

    Raises:
        ValueError: If ``var`` is None and ``can_be_none`` is False.
        TypeError: If ``var`` is not an instance of the specified type.

    Examples:
        >>> _assert_type(1, int, "my_var")
        >>> _assert_type("hello", str, "my_var")
        >>> _assert_type(None, str, "my_var", can_be_none=True)
    """
    if var is None:
        if not can_be_none:
            raise ValueError(f"`{varname}` cannot be None")
    elif not isinstance(var, varType):
        raise TypeError(
            f"`{varname}`should have type {varType.__name__} "
            f"but has type `{type(varname).__name__}`"
        )


def _assert_no_duplicates(
    list_: typing.List[str],
    varname: str,
):
    """Raise an error if a list has duplicates

    Args:
        list_: the list that should not have duplicates
        varname: name of ``list_``

    Raises:
        ValueError: if the list ``list_`` has duplicates
    """
    if not len(set(list_)) == len(list_):
        raise ValueError(f"`{varname}` has duplicated elements")


def _format_list_str(
    list_: typing.List[str] | str | None,
    varname: str,
    no_duplicates: bool = False,
) -> typing.List[str]:
    """Convert a variable into a list of string if possible.

    Args:
        var: variable to convert to a list of string
        varname: variable name
        no_duplicates: whether the list of variables is entitled to have duplicates

    Returns:
        if ``var`` is ``None``, returns an empty list. If ``var`` is a ``str``,
        returns ``[var]``. If ``var`` is already a list of string, returns,
        ``var``.

    Raises:
        TypeError: if ``var`` is neither ``None``, ``str``, nor a list of strings
    """
    if list_ is None:
        return []
    elif isinstance(list_, str):
        return [list_]
    else:
        _assert_list(list_, str, varname)
        if no_duplicates:
            _assert_no_duplicates(list_, varname=varname)
        return list_


def _assert_var_in_list(var, list_, varname):
    """Raise an error if a variable is not in a list.

    Args:
        var: variable to test
        list_: list
        varname: name of ``var``

    Raises:
        ValueError: if ``var`` is not in ``list_``
    """
    if var not in list_:
        raise ValueError(
            f"`{varname}` is `{var}` but is required to be one of these values: "
            + ", ".join(list_)
        )
