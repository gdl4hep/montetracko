"""Module that implements basic matching conditions. The latter can be combined
with boolean operators (``&``, ``|`` and ``~``).
"""
from __future__ import annotations
import typing
import pandas as pd
from .base import MatchingConditionBase
from ..config import ConfigParams


class MinMatchingFraction(MatchingConditionBase):
    """Implement the matching condition based on the matching fraction
    (the ratio between the number of matched hits and the number of hits) being
    higher or equal than a certain threshold (0.7 at LHCb).

    Args:
        min_matching_fraction: A floating-point number representing the minimal matching
            fraction for a particle - track candidate to be considered as a match.
        config: configuration parameters

    Raises:
        ValueError: if ``min_matching_fraction`` is not a floating number between 0
            and 1
    """

    def __init__(
        self,
        min_matching_fraction: float,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        """Initialize a new instance of the :py:class:`MinMatchingFraction` class."""
        super().__init__(
            params={"min_matching_fraction": min_matching_fraction},
            config=config,
        )

    def _assert_params_validity(self, params: typing.Dict[str, typing.Any]):
        """Assert that ``min_matching_fraction`` is a floating number between 0 and 1"""
        min_matching_fraction = params["min_matching_fraction"]
        if not isinstance(min_matching_fraction, float):
            raise ValueError("Min matching fraction should be a `float`")
        elif min_matching_fraction < 0:
            raise ValueError("Min matching fraction should be >= 0")
        elif min_matching_fraction > 1:
            raise ValueError("Min matching fraction should be <= 1")

    def _mask(self, dataframe: pd.DataFrame) -> pd.Series:
        """Obtain the mask that only retains the track-particle candidates whose
        matching fractions are higher than the ``matching_fraction_min`` attribute.

        Args:
            dataframe: a Pandas DataFrame containing the track-particle candidates

        Returns:
            A boolean mask that only keeps the candidates whose matching
            fraction is larger than or equal to the ``matching_fraction_min`` attribute
        """
        return (
            dataframe["n_matched_hits_particle_track"] / dataframe["n_hits_track"]
            >= self.params["min_matching_fraction"]
        )

    def __str__(self):
        """string representation of the matching condition."""
        return (
            "n_matched_hits_particle_track / n_hits_track "
            + f">= {self.params['min_matching_fraction']}"
        )

    def _not_str(self):
        """String representation of the inverse of the matching condition."""
        return (
            "n_matched_hits_particle_track / n_hits_track "
            + f"< {self.params['matching_fraction']}"
        )


class BestMatchingParticles(MatchingConditionBase):
    """Class that implements a track to be matched only to the best matching particles.

    Args:
        dataframe: a Pandas DataFrame containing the track-particle candidates

    Returns:
        A boolean mask that only keeps the candidates for which the number matched
        hits is equal to the maximum number of matched hits for the track.
    """

    def _mask(self, dataframe: pd.DataFrame) -> pd.Series:
        """Obtain the mask that only retains the track-particle candidates whose
        matching fractions are higher than the ``matching_fraction_min`` attribute.

        Args:
            dataframe: a Pandas DataFrame containing the track-particle candidates

        Returns:
            A boolean mask that only keeps the candidates whose matching
            fraction is larger than or equal to the ``matching_fraction_min`` attribute
        """
        dataframe = (
            dataframe.reset_index()
            .merge(
                dataframe.groupby(self.to_aliases(["event_id", "track_id"]))[
                    "n_matched_hits_particle_track"
                ]
                .max()
                .rename("max_n_matched_hits_particle_track"),
                how="left",
                on=self.to_aliases(["event_id", "track_id"]),
            )
            .set_index("index")
        )

        return (
            dataframe["n_matched_hits_particle_track"]
            == dataframe["max_n_matched_hits_particle_track"]
        )

    def __str__(self):
        """string representation of the matching condition."""
        return "BestMatchingParticle"


class MinLengthTrack(MatchingConditionBase):
    """Class that implements the minimum length matching condition.

    It retains only the track-particle candidates whose track has a length
    greater than or equal to a given threshold.

    Args:
        min_length: Minimum length of a track for a candidate to be considered
            as a match
        config: Optional configuration parameters

    Attributes:
        min_length: Minimum length of a track for a candidate to be considered
            as a match
        params: Dictionary that associates parameter names with their values, here
            ``"min_length"`` with its value
        config: Configuration parameters
    """

    def __init__(
        self,
        min_length: int,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        super().__init__({"min_length": min_length}, config=config)
        self.min_length = min_length

    def _assert_params_validity(self, params: typing.Dict[str, typing.Any]):
        """Assert that the given minimum length is a positive integer"""
        if not isinstance(params["min_length"], int):
            raise ValueError("Min length of a track should be an `int`")
        elif params["min_length"] <= 0:
            raise ValueError("Min length of a trach should be > 0")

    def _mask(self, dataframe: pd.DataFrame) -> pd.Series:
        """Return a mask that retains only the track - particle candidates whose
        track has a length greater than or equal to the minimum length"""
        return dataframe["n_hits_track"] >= self.params["min_length"]

    def __str__(self):
        """string representation of the minimum length matching condition"""
        return f"n_hits_track >= {self.params['min_length']}"

    def _not_str(self):
        """string representation of the negation of the minimum length
        matching condition"""
        return f"n_hits_track < {self.params['min_length']}"


class MinLengthMatching(MatchingConditionBase):
    """Implement the matching condition based on the minimum length of the
    matched hits in a track.

    Args:
        min_length: Minimum length of matched hits in a track
        config: Configuration parameters

    Raises:
        ValueError: If ``min_length`` is not a positive integer
    """

    def __init__(
        self,
        min_length: int,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        super().__init__({"min_length": min_length}, config=config)
        self.min_length = min_length

    def _assert_params_validity(self, params: typing.Dict[str, typing.Any]):
        if not isinstance(params["min_length"], int):
            raise ValueError("Min length of matched hits in a track should be an `int`")
        elif params["min_length"] <= 0:
            raise ValueError("Min length of matched hits in a track should be > 0")

    def _mask(self, dataframe: pd.DataFrame) -> pd.Series:
        """Obtain the mask that only retains the track-particle candidates whose
        number of matched hits is higher than or equal to the ``min_length`` attribute.
        """
        return dataframe["n_matched_hits_particle_track"] >= self.params["min_length"]

    def __str__(self):
        """String representation of a :py:class:`MinLengthMatching` instance"""
        return f"n_matched_hits_particle_track >= {self.params['min_length']}"

    def _not_str(self):
        """String representation of the inversion of a :py:class:`MinLengthMatching`
        instance"""
        return f"n_matched_hits_particle_track < {self.params['min_length']}"
