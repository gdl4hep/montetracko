"""Module that implements The base class of a matching condition.
"""
from __future__ import annotations
import typing
from .. import requirement
from ..config import ConfigParams
from .. import utils


class MatchingConditionBase(requirement.RequirementBase):
    """Base class for a matching condition.

    A matching condition filters the DataFrame of the track-particle matching
    candidates.
    This DataFrame has at least the following columns:

    * event ID
    * track ID
    * particle ID
    * ``n_matched_hits_particle_track``: the number of matched hits for the given \
    particle and the given track
    * ``n_hits_track``: the number of hits on the track

    Attributes:
        params: a dictionnary that associates a parameter name of the matching condition
            with its value
        config: configuration parameters
    """

    def __init__(
        self,
        params: typing.Optional[typing.Dict[str, typing.Any]] = None,
        config: typing.Optional[ConfigParams | dict] = None,
    ):
        """Initializes a MatchingConditionBase instance.

        Args:
            params: A dictionary that associates a parameter name of the matching
                condition with its value.
            config: Configuration parameters.
        """
        super().__init__(config=config)
        params_ = {} if params is None else params
        self._assert_params_validity(params)
        self.params = params_

    def _assert_params_validity(self, params: dict | None):
        """An abstract method that raises an error if the ``params`` attribute
        is not correct.
        By default, the method does not do anything and it must be overriden.

        Args:
            params: a dictionnary that associates a parameter name of the matching
            condition with its value
        """
        pass

    def __repr__(self):
        """Representation of a py:class:`MatchingConditionBase` instance"""
        return utils.formatting._get_parameterised_repr(self, self.params)

    def __str__(self):
        """String representation of a py:class:`MatchingConditionBase` instance"""
        return repr(self)

    def __and__(self, other: "MatchingConditionBase"):
        """Define the ``&`` (and) operator between 2 :py:class:`MatchingConditionBase`
        instances.
        """
        return CombinedMatchingConditionBase(
            matchingCondition1=self,
            matchingCondition2=other,
            operator="and",
        )

    def __or__(self, other: "MatchingConditionBase"):
        """Define the ``|`` (or) operator between 2 :py:class:`MatchingConditionBase`
        instances.
        """
        return CombinedMatchingConditionBase(
            matchingCondition1=self,
            matchingCondition2=other,
            operator="or",
        )

    def __invert__(self):
        """Define the ``~`` (not) operator of a :py:class:`MatchingConditionBase`
        instance.
        """
        return NotMatchingConditionBase(matchingCondition=self)


class CombinedMatchingConditionBase(
    requirement.CombinedRequirementBase, MatchingConditionBase
):
    """Class that implements the combination of 2 :py:class:`MatchingConditionBase`
    instances with a boolean operator (``and`` or ``or``).

    Attributes:
        requirement1: first :py:class:`MatchingConditionBase` instance
        requirement2: second :py:class:`MatchingConditionBase` instance
        operator: The boolean operator to combine the two matching conditions
            ("and" or "or").
    """

    def __init__(
        self,
        matchingCondition1: MatchingConditionBase,
        matchingCondition2: MatchingConditionBase,
        operator: typing.Literal["and", "or"],
    ):
        requirement.CombinedRequirementBase.__init__(
            self,
            requirement1=matchingCondition1,
            requirement2=matchingCondition2,
            operator=operator,
        )
        MatchingConditionBase.__init__(self, config=self.config)


class NotMatchingConditionBase(requirement.NotRequirementBase, MatchingConditionBase):
    """Class that implements the "not" (or inversion) operator of
    a :py:class:`MatchingConditionBase` instance.

    Attributes:
        requirement: the :py:class:`MatchingConditionBase` instance to invert
        config: configuration parameters
    """

    def __init__(self, matchingCondition: MatchingConditionBase):
        requirement.NotRequirementBase.__init__(self, requirement=matchingCondition)
        MatchingConditionBase.__init__(self, config=self.config)
