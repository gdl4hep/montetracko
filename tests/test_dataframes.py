"""Tests for the :py:mod:`montetracko.array_utils.dataframes` module.
"""
import pytest
from montetracko.array_utils import dataframes as dfutils
import pandas as pd


@pytest.fixture
def dataframe():
    return pd.DataFrame(
        {
            "A": ["a", "b", "c", "d", "a", "b", "c", "d"],
            "B": [1, 2, 1, 2, 1, 2, 1, 2],
            "C": ["x", "y", "x", "y", "x", "y", "x", "y"],
        }
    )


def test_count_distinct_groups_without_groupby(dataframe):
    expected = 4
    result = dfutils.count_distinct_groups(dataframe, ["A", "B"])
    assert expected == result


def test_count_distinct_groups_with_groupby(dataframe):
    expected = pd.Series([2, 2], index=["x", "y"], name="counts")
    result = dfutils.count_distinct_groups(dataframe, ["A", "B"], ["C"])
    assert expected.equals(result)


@pytest.fixture
def dataframe2():
    return pd.DataFrame({
        'A': ['a', 'b', 'c', 'a', 'b', 'c'],
        'B': [1, 2, 1, 1, 2, 1],
        'C': [10, 20, 30, 40, 50, 60],
    })


def test_sum_column_count_with_groupby(dataframe2):
    expected = pd.Series([50, 70, 90], index=['a', 'b', 'c'], name='C')
    result = dfutils.sum_column_count(dataframe2, 'C', ['A'])
    assert expected.equals(result)


def test_sum_column_count_without_groupby(dataframe2):
    expected = 210
    result = dfutils.sum_column_count(dataframe2, 'C')
    assert expected == result
