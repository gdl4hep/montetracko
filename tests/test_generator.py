"""Tests of the functions in the :py:mod:`montetracko.generator` module.
"""
import numpy as np
import pandas as pd
from montetracko.tracks import generator


def test_get_list_perfect_tracks_events():
    hits_velo = pd.DataFrame(
        {
            "event_id": [1, 1, 1, 2, 2, 2],
            "particle_id": [1, 1, 2, 2, 2, 3],
            "hit_id": [1, 2, 3, 4, 5, 6],
        }
    )

    expected_perfect_tracks = [
        np.array([[1, 1, 2], [1, 3, 0]]),
        np.array([[2, 4, 5], [2, 6, 0]]),
    ]
    perfect_tracks = generator.get_list_perfect_tracks_events(
        hits_velo,
        nhits_max=2,
        condition=generator.should_save_track_passthrough,
        with_event_col=True,
    )
    assert len(perfect_tracks) == len(expected_perfect_tracks)
    for track, expected_track in zip(perfect_tracks, expected_perfect_tracks):
        assert np.array_equal(track, expected_track)
