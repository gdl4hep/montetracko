"""Test for the :py:mod:`montetracko.arrays_utils.series` module.
"""
import pytest
import pandas as pd
from montetracko.array_utils.series import (
    broadcast_index_series,
    merge_series_indices,
)


@pytest.fixture
def subseries():
    return pd.Series(
        [879, 1237, 185, 3317, 1477],
        index=pd.Index([148345, 148346, 148347, 148348, 148349], name="event"),
        name="n_hits_particle",
    )


@pytest.fixture
def series():
    return pd.Series(
        [2, 2, 2, 4, 4, 0, 6, 0, 0, 0],
        index=pd.MultiIndex.from_tuples(
            [
                (148345, 240),
                (148345, 241),
                (148345, 246),
                (148345, 251),
                (148345, 253),
                (148349, 3321),
                (148349, 3328),
                (148349, 3329),
                (148349, 3361),
                (148349, 3364),
            ],
            names=["event", "mcid"],
        ),
        name="n_hits_particle",
    )


def test_broadcast_index_series_with_same_indices(subseries):
    pd.testing.assert_series_equal(
        broadcast_index_series(subseries, subseries),
        subseries,
        check_dtype=False,
    )


def test_broadcast_index_series_with_different_indices(subseries, series):
    expected = pd.Series(
        [879, 879, 879, 879, 879, 1477, 1477, 1477, 1477, 1477],
        index=pd.MultiIndex.from_tuples(
            [
                (148345, 240),
                (148345, 241),
                (148345, 246),
                (148345, 251),
                (148345, 253),
                (148349, 3321),
                (148349, 3328),
                (148349, 3329),
                (148349, 3361),
                (148349, 3364),
            ],
            names=["event", "mcid"],
        ),
        name="n_hits_particle",
    )
    pd.testing.assert_series_equal(
        broadcast_index_series(subseries, series),
        expected,
        check_dtype=False,
    )


def test_broadcast_index_series_with_incompatible_indices(subseries):
    incompatible_series = pd.Series(
        [879, 1237, 185, 3317, 1477],
        index=pd.Index([148345, 148346, 148347, 148348, 148349], name="foo"),
        name="n_hits_particle",
    )
    with pytest.raises(ValueError):
        broadcast_index_series(subseries, incompatible_series)


@pytest.fixture
def series1():
    return pd.Series([1, 2, 3], index=["a", "b", "c"])


@pytest.fixture
def series2():
    return pd.Series([4, 5, 6], index=["a", "c", "d"])


def test_merge_series_indices_with_none_merging(series1, series2):
    with pytest.raises(ValueError):
        merge_series_indices([series1, series2], merging=None)


def test_merge_series_indices_with_union_merging(series1, series2):
    expected = [
        pd.Series([1, 2, 3, 0], index=["a", "b", "c", "d"]),
        pd.Series([4, 0, 5, 6], index=["a", "b", "c", "d"]),
    ]
    result = merge_series_indices([series1, series2], merging="union")
    assert len(expected) == len(result)
    for i in range(len(expected)):
        assert expected[i].equals(result[i])


def test_merge_series_indices_with_intersection_merging(series1, series2):
    expected = [
        pd.Series([1, 3], index=["a", "c"]),
        pd.Series([4, 5], index=["a", "c"]),
    ]
    result = merge_series_indices([series1, series2], merging="intersection")
    assert len(expected) == len(result)
    for i in range(len(expected)):
        assert expected[i].equals(result[i])


def test_merge_series_indices_with_invalid_merging(series1, series2):
    with pytest.raises(ValueError):
        merge_series_indices([series1, series2], merging="invalid_merging")
