"""Tests of the functions in the :py:mod:`montetracko.array_utils.operations` module.
"""
import pytest
import numpy as np
from montetracko.array_utils import operations


@pytest.mark.parametrize(
    "arrays1, arrays2, expected",
    [
        (
            [np.array([1, 2, 3]), np.array([4, 5, 6]), np.array([7, 8, 9])],
            [np.array([1, 2, 3]), np.array([4, 5, 6]), np.array([7, 8, 9])],
            True,
        ),
        (
            [np.array([1, 2, 3]), np.array([4, 5, 6]), np.array([7, 8, 9])],
            [np.array([1, 2, 3]), np.array([4, 5, 6]), np.array([7, 8, 10])],
            False,
        ),
        (
            [np.array([1, 2, 3]), np.array([4, 5, 6]), np.array([7, 8, 9])],
            [np.array([1, 2, 3]), np.array([4, 5, 6])],
            False,
        ),
        ([], [], True),
        ([np.array([1, 2, 3])], [np.array([1, 2, 3, 4])], False),
    ],
)
def test_check_equality_list_arrays(arrays1, arrays2, expected):
    assert operations.check_equality_list_arrays(arrays1, arrays2) == expected


@pytest.mark.parametrize(
    "array, value, expected",
    [
        (np.array([1, 2, 3, 4, 5]), 3, 2),
        (np.array([1, 2, 3, 4, 5]), 6, None),
        (np.array([1.0, 2.0, 3.0, 4.0, 5.0]), 2.0, 1),
        (np.array([1, 2, 3, 4, 5]), 5, 4),
        (np.array([1, 2, 3, 4, 5]), 1, 0),
        (np.array([1, 2, 3, 4, 5]), 0, None),
    ],
)
def test_where_first(array, value, expected):
    assert operations.where_first(array, value=value) == expected


@pytest.mark.parametrize(
    "array, expected",
    [
        (np.array([1, 2, 3, 4]), True),
        (np.array([4, 3, 2, 1]), False),
        (np.array([1, 3, 2, 4]), False),
        (np.array([1]), True),
        (np.array([]), True),
    ],
)
def test_is_sorted(array, expected):
    assert operations.is_sorted(array) == expected


@pytest.mark.parametrize(
    "array, value, expected_output",
    [
        (np.array([1, 2, 3]), 1, 0),
        (np.array([1, 2, 3]), 2, 1),
        (np.array([1, 2, 3]), 3, 2),
        (np.array([1, 2, 3]), 0, -1),
        (np.array([1, 2, 3]), 4, -1),
        (np.array([]), 1, -1),
    ],
)
def test_find_sorted_array(array, value, expected_output):
    assert operations.find_sorted_array(array, value) == expected_output
