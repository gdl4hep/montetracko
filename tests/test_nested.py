import pytest
import numpy as np
from montetracko.array_utils import nested


@pytest.mark.parametrize(
    "arrays, ncols, fill_value, expected_output",
    [
        (
            [[1, 2, 3], [4, 5], [6]],
            None,
            0,
            np.array([[1, 2, 3], [4, 5, 0], [6, 0, 0]]),
        ),
        (
            [[1.1, 2.2, 3.3], [4.4, 5.5], [6.6]],
            None,
            0,
            np.array([[1.1, 2.2, 3.3], [4.4, 5.5, 0], [6.6, 0, 0]]),
        ),
        ([[1, 2, 3], [4, 5], [6]], 2, 0, np.array([[1, 2], [4, 5], [6, 0]])),
        (
            [[1, 2, 3], [4, 5], [6]],
            4,
            -1,
            np.array([[1, 2, 3, -1], [4, 5, -1, -1], [6, -1, -1, -1]], dtype=np.int64),
        ),
    ],
)
def test_nested_list_to_padded_array(arrays, ncols, fill_value, expected_output):
    result = nested.nested_list_to_padded_array(
        arrays, ncols=ncols, fill_value=fill_value
    )
    assert np.array_equal(result, expected_output)


@pytest.mark.parametrize(
    "array, fill_value, expected",
    [
        (
            np.array([[1, 2, 3], [4, 5, 0], [6, 0, 0]]),
            0,
            [np.array([1, 2, 3]), np.array([4, 5]), np.array([6])],
        ),
        (
            np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]),
            -1,
            [np.array([1, 2, 3]), np.array([4, 5, 6]), np.array([7, 8, 9])],
        ),
        (
            np.array([[1, 2], [3, 4], [5, 6]]),
            0,
            [np.array([1, 2]), np.array([3, 4]), np.array([5, 6])],
        ),
    ],
)
def test_padded_array_to_nested_list(array, fill_value, expected):
    result = nested.padded_array_to_nested_list(array, fill_value)
    assert len(result) == len(expected)
    for res, exp in zip(result, expected):
        np.testing.assert_array_equal(res, exp)
