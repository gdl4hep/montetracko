"""Test for :py:mod:`montetracko.array_utils.transform` module.
"""
from __future__ import annotations
import pytest
import numpy as np
import numpy.typing as npt
from montetracko.array_utils import transform


@pytest.mark.parametrize(
    "array,length_max,fill_value,expected_output",
    [
        ([1, 2, 3], 5, 0, np.array([1, 2, 3, 0, 0])),
        ([0.5, 1.5, 2.5, 3.5], 3, -1, np.array([0.5, 1.5, 2.5])),
        (np.array([1, 2, 3]), 2, -1, np.array([1, 2])),
        (np.array([1.5, 2.5, 3.5]), 4, -1, np.array([1.5, 2.5, 3.5, -1])),
    ],
)
def test_get_padded_array(
    array: npt.ArrayLike,
    length_max: int,
    fill_value: int | float,
    expected_output: np.ndarray,
):
    """Test the functionality of the :py:func:`get_padded_array` function"""
    assert np.allclose(
        transform.get_padded_array(array, length_max, fill_value), expected_output
    )


@pytest.mark.parametrize(
    "array, values_to_replace, replacement_values, expected_result",
    [
        (
            np.array([1, 2, 3, 4, 5]),
            np.array([2, 4]),
            np.array([20, 40]),
            np.array([1, 20, 3, 40, 5]),
        ),
        (
            np.array([1, 2, 3, 4, 5]),
            np.array([2, 4]),
            np.array([40, 60]),
            np.array([1, 40, 3, 60, 5]),
        ),
        (
            np.array([1, 2, 3, 4, 5]),
            np.array([]),
            np.array([]),
            np.array([1, 2, 3, 4, 5]),
        ),
        (np.array([]), np.array([2, 4]), np.array([20, 40]), np.array([])),
        (
            np.array([1, 2, 3, 4, 5]),
            np.array([2, 6]),
            np.array([20, 40]),
            np.array([1, 20, 3, 4, 5]),
        ),
    ],
)
def test_replace(array, values_to_replace, replacement_values, expected_result):
    assert np.array_equal(
        transform.replace(array, values_to_replace, replacement_values), expected_result
    )
    assert np.array_equal(
        transform.replace(array, values_to_replace, replacement_values, inplace=True),
        expected_result,
    )
    new_array = np.copy(array)
    transform.replace(new_array, values_to_replace, replacement_values, inplace=True)
    assert np.array_equal(new_array, expected_result)
