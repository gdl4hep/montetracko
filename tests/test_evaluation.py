"""Test that the Allen report coincides with the results obtained using Allen.
"""
import pytest
import pandas as pd
import montetracko as mt
import montetracko.lhcb as mtb


@pytest.mark.parametrize("detector", ("velo", "scifi"))
def test_report(detector: str):
    config_params = {
        "aliases": {
            "event_id": "event",
            "track_id": "track",
            "hit_id": "lhcbid",
            "particle_id": "mcid",
        },
    }
    # Load dataframes
    df_hits_particles = pd.read_parquet(
        f"guide/data/xdigi2csv/hits_{detector}.parquet.lz4",
        columns=["event", "lhcbid", "mcid"],
    )
    df_particles = pd.read_parquet("guide/data/xdigi2csv/mc_particles.parquet.lz4")
    df_tracks_hits = mt.TrackHandler.from_padded_csv(
        f"guide/data/{detector}_tracks/*.csv", config=config_params
    ).dataframe

    df_tracks_hits["event"] = df_tracks_hits["event"] % 10**9

    # Match tracks to particles
    trackEvaluator = mt.TrackMatcher(config=config_params).full_matching(
        df_events_tracks_hits=df_tracks_hits,
        df_events_hits_particles=df_hits_particles,
        df_events_particles=df_particles,
        matching_condition=mt.matchcond.MinMatchingFraction(0.7),
    )
    # Report
    allen_report = trackEvaluator.report(
        mt.AllenReporter(auto_numbering=detector == "velo"),
        categories=mtb.category.detector_to_categories[detector],
    )
    with open(f"guide/data/allen_{detector}_validation.log", "r") as allen_result_file:
        expected_allen_report = allen_result_file.read()

    print(allen_report)
    print(expected_allen_report)
    assert allen_report == expected_allen_report
