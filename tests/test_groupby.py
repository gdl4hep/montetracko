"""Tests of the :py:mod:``
"""
import pytest
import numpy as np
from montetracko.array_utils import groupby


@pytest.mark.parametrize(
    "array, expected_lengths, expected_values",
    [
        (
            np.array([[1, 2], [2, 3], [2, 4]]),
            [1, 2],
            [1, 2],
        ),
        (
            np.array([[1, 2], [1, 3], [1, 4]]),
            [3],
            [1],
        ),
        (
            np.array([[1, 2], [1, 3], [2, 4], [2, 5], [2, 6]]),
            [2, 3],
            [1, 2],
        ),
    ],
)
def test_group_lengths(array, expected_lengths, expected_values):
    lengths, values = groupby.group_lengths(array[:, 0])
    assert np.array_equal(lengths, expected_lengths)
    assert np.array_equal(values, expected_values)


@pytest.mark.parametrize(
    "array,column_idx,expected_groups,expected_group_values,sort",
    [
        (
            np.array([[1, 2], [2, 3], [2, 4], [3, 1], [3, 3]]),
            0,
            [
                np.array([[1, 2]]),
                np.array([[2, 3], [2, 4]]),
                np.array([[3, 1], [3, 3]]),
            ],
            np.array([1, 2, 3]),
            False,
        ),
        (
            np.array([[3, 1], [2, 3], [2, 4], [3, 3], [1, 2]]),
            0,
            [
                np.array([[1, 2]]),
                np.array([[2, 3], [2, 4]]),
                np.array([[3, 1], [3, 3]]),
            ],
            np.array([1, 2, 3]),
            True,
        ),
        (
            np.array([[1, 2], [1, 3], [2, 1], [2, 2]]),
            1,
            [np.array([[2, 1]]), np.array([[1, 2], [2, 2]]), np.array([[1, 3]])],
            np.array([1, 2, 3]),
            True,
        ),
    ],
)
def test_groupby_sorted_array(
    array,
    column_idx,
    expected_groups,
    expected_group_values,
    sort,
):
    groups, group_values = groupby.groupby_sorted_array(array, column_idx, sort=sort)
    assert len(groups) == len(expected_groups)
    for group, expected_group in zip(groups, expected_groups):
        assert np.array_equal(group, expected_group)
    assert np.array_equal(group_values, expected_group_values)
