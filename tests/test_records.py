"""Test for the :py:mod:`montetracko.array_utils.records` module.
"""
import pytest
import numpy as np
import pandas as pd
from montetracko.array_utils import records


def test_to_record():
    # Create a test DataFrame
    df = pd.DataFrame(
        {
            "event_id": [1, 2, 3],
            "hit_id": [101, 102, 103],
            "particle_id": [1001, 1002, 1003],
            "x": [0.1, 0.2, 0.3],
            "y": [1.0, 2.0, 3.0],
        }
    )

    # Test with default column mapping
    columns = ["event_id", "hit_id", "particle_id"]
    expected_result = np.array(
        [(1, 101, 1001), (2, 102, 1002), (3, 103, 1003)],
        dtype=[("event_id", np.int64), ("hit_id", np.int64), ("particle_id", np.int64)],
    )
    result = records.to_record(df, columns)
    np.testing.assert_array_equal(result, expected_result)

    # Test with a column not in `alias_to_column_names`
    columns = ["event_id", "hit_id", "particle_id", "y"]
    expected_result = np.array(
        [(1, 101, 1001, 1.0), (2, 102, 1002, 2.0), (3, 103, 1003, 3.0)],
        dtype=[
            ("event_id", np.int64),
            ("hit_id", np.int64),
            ("particle_id", np.int64),
            ("y", np.float64),
        ],
    )
    result = records.to_record(df, columns)
    np.testing.assert_array_equal(result, expected_result)

    # Test with custom column mapping
    custom_mapping = {"event": "event_id", "lhcbid": "hit_id", "mcid": "particle_id"}
    expected_result = np.array(
        [(1, 101, 1001), (2, 102, 1002), (3, 103, 1003)],
        dtype=[("event", np.int64), ("lhcbid", np.int64), ("mcid", np.int64)],
    )
    columns = ["event", "lhcbid", "mcid"]

    result = records.to_record(df, columns, alias_to_column_names=custom_mapping)
    np.testing.assert_array_equal(result, expected_result)

    # Test with non-existent column
    with pytest.raises(KeyError):
        columns = ["event_id", "nonexistent_column"]
        result = records.to_record(df, columns)
