# MonteTracko

MonteTracko is a Python library for evaluating the performance of track reconstruction
algorithms in particle physics experiments.
It provides tools to match simulated particle trajectories with reconstructed tracks,
compute performance metrics, and visualize the results in various ways.

The Montetracko library has been tested and validated to produce identical
results to the Allen Velo validation sequence.
Currently, the library only supports one detector.

Documentation can be found at https://montetracko.docs.cern.ch/master/index.html.
A user guide is available under `Guide/guide.ipynb`
or [here](https://montetracko.docs.cern.ch/master/guide.html).
